package core.data;

import core.util.io.LogConst;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static lombok.AccessLevel.PRIVATE;

@SuppressWarnings("SpellCheckingInspection")
public final class AppConst extends LogConst {

    /* PATHS */
    public final static String DICTIONARY_PATH = "idyota_core/src/main/resources/assets/";
    public final static String RULES_PATH = DICTIONARY_PATH + "rules/";
    public final static String EXAMPLE_PATH = DICTIONARY_PATH + "examples/";
    public final static String HTML_PATH = DICTIONARY_PATH + "html/";
    /* SIGNS */
    public final static String LINE_SEPARATOR = "#line#";
    /* DICTIONARY NAMES */
    private final static String MAIN_DICT = "huge.txt";
    /* DICTIONARY PATHS */
    public final static String MAIN_DICT_PATH = DICTIONARY_PATH + MAIN_DICT;
    private final static String FIX_DICT = "hints.txt";
    public final static String FIX_DICT_PATH = DICTIONARY_PATH + FIX_DICT;
    private final static String DIVIDE_DICT = "divide.txt";
    public final static String DIVIDE_DICT_PATH = DICTIONARY_PATH + DIVIDE_DICT;
    private final static String NAMES_DICT = "names.txt";
    public final static String NAMES_DICT_PATH = DICTIONARY_PATH + NAMES_DICT;
    private final static String UNWIND_DATA = "unwind.txt";
    public final static String UNWIND_PATH = DICTIONARY_PATH + UNWIND_DATA;
    private final static String TO_COMPRESS = "hints.txt";
    public final static String DICT_TO_COMPRESS = DICTIONARY_PATH + TO_COMPRESS;
    private final static String COMMON_DICT = "common.txt";
    public final static String COMMON_DICT_PATH = DICTIONARY_PATH + COMMON_DICT;
    private final static String DICTS = "dictionaries/";
    private final static String EM_SINGLE = "em-emi-single.txt";
    public final static String EM_SINGE_PATH = DICTIONARY_PATH + DICTS + EM_SINGLE;
    private final static String EM_DOUBLE = "em-emi-both.txt";
    public final static String EM_DOUBLE_PATH = DICTIONARY_PATH + DICTS + EM_DOUBLE;
    private final static String JOTA = "jota.txt";
    public final static String JOTA_PATH = DICTIONARY_PATH + DICTS + JOTA;
    private final static String TYPOS = "typos.txt";
    public final static String TYPO_PATH = DICTIONARY_PATH + DICTS + TYPOS;
    private static final String TO_OUTPUT = "compressed.txt";
    public final static String DICT_TO_OUTPUT = DICTIONARY_PATH + TO_OUTPUT;

    @Getter(value = PRIVATE)
    @AllArgsConstructor
    public enum TIME {
        NANOS("nanos"),
        MILLIS("millis"),
        SEC("seconds"),
        MINUTES("minutes");
        protected final String value;

        public String stringify() {
            return getValue();
        }
    }
}
