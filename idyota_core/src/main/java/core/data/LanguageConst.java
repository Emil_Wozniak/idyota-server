package core.data;

public final class LanguageConst {

    public static final String COLON = ":";

    public final static  String TILDE = "~";

    public final static  String PIPE = "|";

    public static final String[] TEST = {"A", "R", "C"};

    public static final String[] LETTERS = {
        "a", "ą", "b", "c", "ć", "d", "e", "ę", "f", "g", "h", "i", "j", "k", "l", "ł",
        "m", "n", "o", "ó", "p", "r", "s", "ś", "t", "u", "w", "y", "z", "ż", "ź"
    };

    public static final String PUNCTUATION = ". , ! ? ; : ) ( - : — « » … „ ” “ …";

    public final static String[] punctuationAfter = {",", "\\.", "!", "\\?", "\\)", "\\]", "\\;", "\\:", "„"};

    public final static String[] punctuationBefore = {",", "\\(", "\\[", "„"};

    public final static String[] TOKENIZER_PUNCTUATION = {",", "\\.", "!", "\\?", "\\)", "\\]", "\\;", "\\:", "\\(", "\\[", "«", "»", "…", "„", "”", "“", " "};

}
