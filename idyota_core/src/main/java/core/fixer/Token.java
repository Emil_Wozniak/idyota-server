package core.fixer;

import core.data.AppConst;
import lombok.Getter;

import java.util.StringJoiner;

@Getter
public class Token {

    private final String id;
    private final String fixedSpan = "<span style=\"color:#009B77;\" alt=\""; // note it isn't closed
    private final String unknownSpan = "<span style=\"color:#ff0000;\">";
    private String word;
    private String replacement;
    private String info;

    public Token(String word, String replacement, String info, String id) {
        this.word = word;
        this.replacement = replacement;
        this.info = info;
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String toPlainString() {

        if (word.equals(AppConst.LINE_SEPARATOR)) {
            return (System.lineSeparator());
        } else if (replacement.equals("")) {
            return word;
        } else {
            return replacement;
        }
    }

    public String toHTMLPart() {
        if (word.equals(AppConst.LINE_SEPARATOR)) {
            return ("<br>");
        } else if (replacement.equals("") || replacement.equals(word)) {
            return info.equals("unknown") ? unknownSpan + word + "</span>" : word;
        } else {
            return fixedSpan(info) + replacement + "</span>";
        }
    }

    public String toHTMLLog() {
        if (word.equals(replacement)) return "";
        else if (info.contains("unknown")) {
            return unknownSpan + word + "</span><br>";
        } else {
            return word + " > " + replacement + " " + info + "<br>";
        }
    }

    private String fixedSpan(String info) {
        info = info.replace(">", "");

        String idString = "id=\"" + id + "\";";

        String altString = "alt=\"" + replacement + "\"; ";

        String functionString = " onclick=\"changeWording(id)\")";

        if (info.contains("L2") || info.contains("L1U")) {
            swapElements();
            return "<span " + idString + altString + "style=\"color:#00779B\"; title=\"" + word + "\"" + functionString + ">";
        } else

            return "<span " + idString + altString + "style=\"color:#339B77\"; title=\"" + word + "\"" + functionString + ">";
    }

    private void swapElements() {
        String temp = word;
        word = replacement;
        replacement = temp;
    }


    @Override
    public String toString() {
        return new StringJoiner(", ", Token.class
            .getSimpleName() + "[", "]")
            .add("word='" + word + "'")
            .add("replacement='" + replacement + "'")
            .add("info='" + info + "'")
            .add("id='" + id + "'")
            .add("fixedSpan='" + fixedSpan + "'")
            .add("unknownSpan='" + unknownSpan + "'")
            .toString();
    }
}
