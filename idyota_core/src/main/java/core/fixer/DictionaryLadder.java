package core.fixer;

import core.dictionary.ReplacementDictionary;
import lombok.Value;

import static core.data.AppConst.*;

@Value
@SuppressWarnings("SpellCheckingInspection")
public class DictionaryLadder {

    ReplacementDictionary divide = new ReplacementDictionary(DIVIDE_DICT_PATH, "");
    ReplacementDictionary emSingle = new ReplacementDictionary(EM_SINGE_PATH, "");
    ReplacementDictionary emDouble = new ReplacementDictionary(EM_DOUBLE_PATH, "i");
    ReplacementDictionary jota = new ReplacementDictionary(JOTA_PATH);
    ReplacementDictionary typos = new ReplacementDictionary(TYPO_PATH);

    public String getReplacement(final String word) {
        var replacement = divide.replace(word);
        if (word.equals(replacement)) replacement = emSingle.replace(word);
        if (word.equals(replacement)) replacement = emDouble.replace(word);
        if (word.equals(replacement)) replacement = typos.replace(word);
        if (word.equals(replacement)) replacement = jota.replaceUnsorted(word);
        return replacement;
    }

}
