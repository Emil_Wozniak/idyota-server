package core.fixer;

import core.dictionary.FlatDictionary;
import core.levenstein.DistanceEditor;
import core.levenstein.IDistance;
import core.tokenizer.IdyotaTokenizer;
import core.tokenizer.NaiveTokenizer;
import core.util.factory.DictionaryFactory;
import core.util.io.IdyotaDictionary;
import core.util.io.IdyotaFileWriter;
import core.util.io.TxtFileWriter;
import core.util.logger.StatPrinter;
import io.vavr.collection.Stream;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static core.data.AppConst.FIX_DICT_PATH;
import static core.data.LanguageConst.LETTERS;
import static core.util.BooleanUtil.isNot;
import static core.util.string.StringCaseUtil.*;

/**
 * Fix words in text and returns Tokens.
 */
@Slf4j
@Value
@NonFinal
@NoArgsConstructor
@SuppressWarnings("SpellCheckingInspection")
public class NaiveFixer implements Fixer {

    IdyotaFileWriter writer = new TxtFileWriter();
    IdyotaTokenizer tokenizer = new NaiveTokenizer();
    IdyotaDictionary fixDict = new FlatDictionary(FIX_DICT_PATH, true, writer);
    DictionaryFactory identifier = new WordIdentifier();
    IDistance distanceEditor = new DistanceEditor(LETTERS);
    StatPrinter statistics = new FixerStats();
    BigramDictionary bigrams = new BigramDictionary("src/assets/2grams.txt");

    List<String> unknownWords = new ArrayList<>();
    List<String> unknownBigrams = new ArrayList<>();

    /**
     * Main method used to convert text into the list of {@link Token}.
     *
     * @param text polish text from file or any other type of source
     * @return list of Token instance
     * @see Token
     */
    @Override
    public List<Token> solve(final String text) {
        statistics.clear();
        final String[] previous = {""};
        final var stringTokens = tokenizer.getStringTokens(text);
        final var tokens = Arrays.asList(stringTokens);
        return Stream
                .ofAll(tokens)
                .zipWithIndex()
                .filter(word -> isNotEquals(word._1, ""))
                .distinctBy(word -> word._1)
                .map((candidate) -> {
                    var word = candidate._1;
                    var index = candidate._2;
                    var result = processWord(previous[0], word, index);
                    previous[0] = conditionReplace(previous[0], isNotEquals(word, " "), word);
                    return result;
                })
                .toJavaList();
    }

    private Token processWord(final String previous, final @NotNull String word, final Integer index) {
        return isNotEmptyAndNotSpaceAndNotSpecial(word)
                ? identifier.isKnown(word)
                ? processKnownWord(previous, word, index)
                : processUnknownWord(previous, word, index)
                : new Token(word, word, "[did nothing]", id(index));
    }

    @NotNull
    private String id(final Integer index) {
        return "id" + index;
    }

    /**
     * TODO: suggestions in case of words routinely misspell by OCR software.
     */
    @NotNull
    @Contract("_, _, _ -> new")
    private Token processKnownWord(final String previousWord, final String word, Integer index) {
        if (isIdentifierAndIsNotBigram(previousWord, word)) unknownBigrams.add(previousWord + " " + word + " 1");
        statistics.addKnown();
        return new Token(word, word, "", id(index));
    }

    /**
     * checks if word is not in upperCase it is that way on assumption that
     * word in lower case occurs more often than word in upper case. Therefore
     * to keep high performance of JVM skips second option on JIT as less occurs.
     *
     * @param word  examine text
     * @param index word position
     * @return instance of Token
     */
    @NotNull
    private Token processUnknownWord(final String previous, final String word, Integer index) {
        final var isUpper = isUpperCase(word);
        final var upper = conditionReplace(word, isUpper, word.toLowerCase());
        final var replacement = getForcedFix(upper, isUpper, fixDict);
        return isUnknown(word)
                ? new Token(word, word, "unknown", id(index))
                : isReplacementNotEqualsWord(previous, upper, isUpper, replacement, index);
    }

    @NotNull
    private Token isReplacementNotEqualsWord(String previousWord, String word, boolean isUpper, @NotNull String replacement, Integer index) {
        return isNot(replacement.equals(word))
                ? addForcedFix(word, replacement, isUpper, index)
                : isUnknown(previousWord, word, isUpper, index, getSuggestion(previousWord, word, isUpper, false, index));
    }

    @NotNull
    private Token isUnknown(String previousWord, String word, boolean upper, int index, @NotNull Token suggestion) {
        return suggestion.getInfo().equals("unknown") && isNot(upper)
                ? getSuggestion(previousWord, word, false, true, index)
                : isSuggestUnknown(word, suggestion);
    }

    @NotNull
    @Contract("_, _ -> param2")
    private Token isSuggestUnknown(String word, @NotNull Token suggestion) {
        if (suggestion.getInfo().equals("unknown")) unknownWords.add(word);
        return suggestion;
    }

    private String getForcedFix(final String word, final boolean isUpper, IdyotaDictionary dictionary) {

        var replacement = new DictionaryLadder().getReplacement(word);
        final var isReplacement = word.equals(replacement);
        final var nasalSuggestion = new Rule("on", "ą").getSuggestions(word, dictionary);
        final var rulesSuggestion = new RulesLadder().getSuggestion(word, dictionary, statistics);

        /* TODO CONDITION IS REDUNDANT AND RESULT OF FIRST IS OVERRIDING */
        replacement = conditionReplace(replacement, isReplacement, nasalSuggestion);
        replacement = conditionReplace(replacement, isReplacement, rulesSuggestion);

        replacement = isNonLatinE(replacement, word, isUpper, dictionary);
        return isNamesMisplacedApostrophe(word, replacement, isUpper);
    }

    /**
     * special case 1: treat "é" like "e".
     *
     * @param replacement word replacemnet
     * @param word        analyze word
     * @param isUpper     if true word is uppercase
     * @param dictionary  words source
     * @return if isReplacement and contains é, then if it's new word replaced é candidate
     * or forced fixed word. In other case original replace word.
     */
    private String isNonLatinE(@NonNull String replacement, @NotNull String word, boolean isUpper, IdyotaDictionary dictionary) {
        String candidate = word.replaceAll("é", "e");
        return word.equals(replacement) && word.contains("é")
                ? identifier.isKnown(candidate)
                ? candidate
                : getForcedFix(candidate, isUpper, dictionary)
                : replacement;
    }

    /**
     * special case 2: names with misplaced apostrophe.
     *
     * @param word        analyze word
     * @param replacement original replecement word
     * @param isUpper     if true word is uppercase
     * @return new candidate or null
     */
    private String isNamesMisplacedApostrophe(@NotNull final String word, @NonNull final String replacement, boolean isUpper) {
        return word.equals(replacement) && word.contains("'") && isUpper
                ? isIdentifierName(word, replacement)
                : replacement;
    }

    private String isIdentifierName(@NotNull String word, final String replacement) {
        String candidate = word.replaceAll("'", "");
        candidate = candidate.substring(0, 1).toUpperCase() + candidate.substring(1);
        if (identifier.isName(candidate)) return candidate;
        return replacement;
    }

    Token addForcedFix(final String word, final String replacement, final boolean isUpper, Integer index) {
        statistics.addFixed();
        var candidate = normalizeCase(replacement, isUpper);
        var normalize = normalizeCase(word, isUpper);
        return new Token(normalize, candidate, "> [" + candidate + "]", id(index));
    }

    Token getSuggestion(String previous, String word, boolean isUpper, boolean distant, int index) {
        return isSpecial(word)
                ? new Token(word, word, word, id(index))
                : getNotSpecial(previous, word, isUpper, distant, index);
    }

    @NotNull
    private Token getNotSpecial(String previous, String word, boolean isUpper, boolean distant, int index) {
        var possibleWords = distant
                ? distanceEditor.getDistantWords(word)
                : distanceEditor.getClosestWords(word, 0);

        var filterSuggestions = distanceEditor.filterSuggestions(possibleWords, fixDict);
        var best = distanceEditor.getBestSuggestion(previous, word, filterSuggestions, identifier, bigrams);

        return new Token(
                normalizeCase(word, isUpper),
                normalizeCase(best, isUpper),
                InfoUtil.joinSuggestions(filterSuggestions, getInfo(previous, distant, best)),
                id(index));
    }

    @NotNull
    private String getInfo(String previous, boolean getDistant, @NotNull String bestSuggestion) {
        return InfoUtil.assign(statistics, bigrams).createInfo(previous, bestSuggestion, getDistant);
    }

    private boolean isUnknown(final String word) {
        return unknownWords.contains(word);
    }

    private boolean isNotEmptyAndNotSpaceAndNotSpecial(@NotNull String word) {
        return isNotEquals(word, "") && isNotEquals(word, " ") && isNot(isSpecial(word));
    }

    private boolean isIdentifierAndIsNotBigram(String previousWord, String word) {
        return identifier.isKnown(previousWord) && isNot(bigrams.isKnown(previousWord + " " + word));
    }
}
