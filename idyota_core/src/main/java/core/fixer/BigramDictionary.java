package core.fixer;

import core.dictionary.FlatDictionary;
import core.util.io.AllLinesReader;
import core.util.io.IdyotaFileWriter;
import core.util.io.IdyotaReader;
import core.util.io.TxtFileWriter;
import io.vavr.control.Try;
import lombok.Getter;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static core.util.BooleanUtil.isNot;
import static java.lang.Integer.parseInt;
import static java.util.stream.Collectors.toList;

@Slf4j
@Value
@SuppressWarnings("SpellCheckingInspection")
public class BigramDictionary {

    FlatDictionary inputDictionary;
    FlatDictionary outputDictionary;
    IdyotaFileWriter writer = new TxtFileWriter();

    List<String> in = new ArrayList<>();
    List<String> out = new ArrayList<>();
    IdyotaReader reader = new AllLinesReader();

    @Getter
    @NonFinal
    int max = 0;

    public BigramDictionary(String fileName) {

        List<String> lines = reader.linesToList(fileName)
                .stream()
                .filter(line -> isNot(line.contains("#")) && isNot(line.equals("")))
                .collect(toList());

        Object[] sortedLines = lines.stream()
                .map(String::trim)
                .map(line -> line.split(" "))
                .map(parts -> parts[0] + " " + parts[1] + " " + parts[2])
                .sorted()
                .toArray();

        Arrays.stream(sortedLines)
                .map(line -> (String) line)
                .map(lineString -> lineString.split(" "))
                .forEach(parts -> {
                    in.add(parts[0] + " " + parts[1]);
                    out.add(parts[2]);
                    Try.run(() -> {
                        int currentValue = parseInt(parts[2]);
                        if (currentValue > max) max = currentValue;
                    }).onFailure(NumberFormatException.class, error -> log.error(error.getMessage()));
                });

        inputDictionary = new FlatDictionary(in.toArray(), writer);
        outputDictionary = new FlatDictionary(out.toArray(), writer);

        //listToFile(newLines, "C:/Users/Pawel/Documents/GitHub/idyota_java/src/assets/2grams.txt", true);
    }

    public void arrayToFile(@NotNull final Object[] array, final String fileName) {
        Try.withResources(() -> new BufferedWriter(new FileWriter(fileName)))
                .of(writer -> {
                    Stream.of(array).forEach(line -> Try
                            .run(() -> {
                                writer.write(line.toString());
                                writer.newLine();
                            }));
                    writer.close();
                    return writer;
                })
                .onFailure(error -> log.error("Error writing file "));
    }

    /**
     * TODO check implementation
     */
    public int getSlowValue(String bigram) {
        final var first = IntStream
                .range(0, inputDictionary.getSize())
                .boxed()
                .filter(i -> bigram.equals(inputDictionary.getWord(i)))
                .parallel()
                .map(i -> Try
                        .of(() -> parseInt(outputDictionary.getWord(i)))
                        .recover(NumberFormatException.class, error -> 0)
                        .get())
                .findAny();

        return first.orElse(0);
    }

    public boolean isKnown(String bigram) {
        return inputDictionary.isInDictionary(bigram);
    }

}
