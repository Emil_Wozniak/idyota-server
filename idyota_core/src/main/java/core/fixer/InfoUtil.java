package core.fixer;

import core.util.logger.StatPrinter;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import static core.util.BooleanUtil.isNot;
import static java.util.stream.Collectors.joining;

@FunctionalInterface
public interface InfoUtil {

    @NotNull
    @Contract(value = "_, _ -> new", pure = true)
    static InfoUtil assign(StatPrinter stats, BigramDictionary dictionary) {
        return new InfoBuilder(stats, dictionary);
    }

    String createInfo(String previous, String best, Boolean distant);

    class InfoBuilder implements InfoUtil {
        BigramDictionary dictionary;
        StatPrinter stats;
        String info = "[???]";

        public InfoBuilder(StatPrinter stats, BigramDictionary dictionary) {
            this.stats = stats;
            this.dictionary = dictionary;
        }

        @Override
        public String createInfo(String previous, @NotNull String best, Boolean distant) {
            info = best;
            if (best.equals("")) {
                stats.addUnfixed();
                info = "unknown";
            } else {
                stats.addFixed();
                info = " > [" + best + "] ";
                isDistant(previous, best, distant);
            }
            return info;
        }

        private void isDistant(final String previous, final String best, @NotNull Boolean distant) {
            String possiblyKnown = previous + " " + best;
            if (distant) info += " L2 ";
            else isKnown(possiblyKnown);
        }

        private void isKnown(final String possiblyKnown) {
            if (isNot(dictionary.isKnown(possiblyKnown))) info += " L1U ";
            else info += " L1K ";
        }
    }

    /**
     * Appends possible words to info.
     */
    @NotNull
    static String joinSuggestions(@NotNull final List<String> possibleWords, final String info) {
        return info + possibleWords
                .stream()
                .map(suggestion -> suggestion + " ")
                .collect(joining());
    }
}
