package core.fixer;

import core.util.io.IdyotaDictionary;

public interface IRule {
    String getSuggestions(String word, IdyotaDictionary dictionary);
}
