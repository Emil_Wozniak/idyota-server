package core.fixer;

import java.util.List;

public interface Fixer {

    List<Token> solve(final String txt);

}
