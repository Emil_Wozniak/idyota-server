package core.fixer;

import core.util.logger.StatPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("SpellCheckingInspection")
public class FixerStats implements StatPrinter {

    static Logger log = LoggerFactory.getLogger(Fixer.class.getName());

    private int knownWords;
    private int fixedWords;
    private int unfixedWords;
    private int emRule;
    private int jotaRule;
    private int ocrRule;

    @Override
    public void clear() {
        knownWords = 0;
        fixedWords = 0;
        unfixedWords = 0;
        emRule = 0;
        jotaRule = 0;
        ocrRule = 0;
    }

    @Override
    public void print() {
        log.info("Total words: " + (knownWords + fixedWords + unfixedWords));
        log.info("Words in dictionary: " + knownWords);
        log.info("Words not in dictionary: " + (fixedWords + unfixedWords));
        log.info("Fixed words: " + fixedWords);
        log.info("Unfixed words: " + unfixedWords);
        log.info("Fixed permille: " + (fixedWords * 1000) / (fixedWords + unfixedWords) + "/1000");
        log.info("Jota rule: " + jotaRule);
        log.info("Em rule: " + emRule);
        log.info("OCR rule: " + ocrRule);
    }

    @Override
    public void addKnown() {
        knownWords++;
    }

    @Override
    public void addFixed() {
        fixedWords++;
    }

    @Override
    public void addUnfixed() {
        unfixedWords++;
    }

    @Override
    public void addJota() {
        jotaRule++;
    }

    @Override
    public void addEm() {
        emRule++;
    }

    @Override
    public void addOcr() {
        ocrRule++;
    }
}
