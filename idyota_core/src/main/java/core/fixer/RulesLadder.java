package core.fixer;

import core.util.io.IdyotaDictionary;
import core.util.logger.StatPrinter;
import lombok.NoArgsConstructor;
import lombok.Value;

import static core.data.AppConst.RULES_PATH;
import static core.util.BooleanUtil.isNot;

@Value
@NoArgsConstructor
@SuppressWarnings("SpellCheckingInspection")
public class RulesLadder {

    RulesList ocrPart = new RulesList(RULES_PATH + "ocr.txt");
    RulesList jotaPart = new RulesList(RULES_PATH + "jota_part.txt");
    RulesList emPart = new RulesList(RULES_PATH + "em_part.txt");
    RulesList consonantsPart = new RulesList(RULES_PATH + "consonants.txt");
    RulesList numberPart = new RulesList(RULES_PATH + "numbers.txt");
    RulesList verbPart = new RulesList(RULES_PATH + "verbs.txt");

    public String getSuggestion(String word, IdyotaDictionary dictionary, StatPrinter statistics) {

        String replacement = emPart.getSuggestion(word, dictionary);
        if (isNot(word.equals(replacement))) statistics.addEm();
        else {
            replacement = jotaPart.getSuggestion(word, dictionary);
            if (isNot(word.equals(replacement))) statistics.addJota();
            else {
                replacement = ocrPart.getSuggestion(word, dictionary);
                if (isNot(word.equals(replacement))) statistics.addOcr();
                else replacement = consonantsPart.getSuggestion(word, dictionary);
            }
        }

        if (word.equals(replacement)) replacement = numberPart.getSuggestion(word, dictionary);

        if (word.equals(replacement)) replacement = verbPart.getSuggestion(word, dictionary);

        return replacement;
    }

}
