package core.fixer;

import core.util.io.IdyotaDictionary;
import org.jetbrains.annotations.NotNull;

public class Rule implements IRule {
    String wrongPart;
    String rightPart;

    public Rule(String wrong, String right) {
        this.wrongPart = wrong;
        this.rightPart = right;
    }

    public String getSuggestions(@NotNull String word, @NotNull IdyotaDictionary dictionary) {
        String replacement = word.replace(wrongPart, rightPart);
        return dictionary.isInDictionary(replacement) ? replacement : word;
    }
}
