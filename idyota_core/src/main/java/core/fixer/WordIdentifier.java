package core.fixer;

import core.dictionary.FlatDictionary;
import core.util.factory.DictionaryFactory;
import core.util.factory.FlatDictionariesFactory;
import core.util.factory.IdyotaDictionaryFactory;
import core.util.io.IdyotaDictionary;
import core.util.io.IdyotaFileWriter;
import core.util.io.TxtFileWriter;
import io.vavr.control.Try;
import lombok.NonNull;
import lombok.Value;

import static core.util.factory.FlatDictionariesFactory.DictNames.*;
import static core.util.string.StringCaseUtil.makeLowerCase;
import static java.lang.Double.parseDouble;

@Value
public class WordIdentifier implements DictionaryFactory {

    IdyotaDictionaryFactory<FlatDictionary> factory = new FlatDictionariesFactory();
    IdyotaFileWriter writer = new TxtFileWriter();
    IdyotaDictionary punctuation;
    IdyotaDictionary common;
    IdyotaDictionary main;
    IdyotaDictionary names;

    public WordIdentifier() {
        factory.create(writer);
        punctuation = factory.getLibrary(PUNCT);
        common = factory.getLibrary(COMMON);
        main = factory.getLibrary(MAIN);
        names = factory.getLibrary(NAMES);
    }

    public boolean isKnown(String word) {
        if (punctuation.isInDictionary(word)) return true;
        if (isCommon(word)) return true;
        if (names.isInDictionary(word)) return true;
        if (main.isInDictionary(word)) return true;
        if (isNumeric(word)) return true;
        final var lowerCase = makeLowerCase(word);
        return main.isInDictionary(lowerCase);
    }

    public boolean isCommon(String word) {
        return common.isInDictionary(word);
    }

    public boolean isName(String word) {
        return names.isInDictionary(word);
    }

    /**
     * @param text word to check
     * @return true if word is a number or false in other case
     */
    public boolean isNumeric(@NonNull final String text) {
        return Try
                .of(() -> parseDouble(text))
                .map(it -> true)
                .getOrElse(false);
    }
}
