package core.fixer;

import core.util.io.AllLinesReader;
import core.util.io.IdyotaDictionary;
import core.util.io.IdyotaReader;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static core.util.BooleanUtil.isNot;

@Slf4j
@Value
public class RulesList {

    List<IRule> ruleList = new ArrayList<>();
    IdyotaReader reader = new AllLinesReader();

    public RulesList(String fileName) {
        List<String> lines = reader.linesToList(fileName);
        lines.stream()
                .filter(line -> isNot(line.contains("#")) && isNot(line.equals("")))
                .map(line -> line.split(" > "))
                .forEach(parts -> ruleList.add(new Rule(parts[0], parts[1])));
    }

    public String getSuggestion(String word, IdyotaDictionary dictionary) {
        return ruleList
                .stream()
                .map(rule -> rule.getSuggestions(word, dictionary))
                .filter(replacement -> isNot(isSame(word, replacement) && isInDictionary(dictionary, replacement)))
                .findFirst()
                .orElse(word);
    }

    private boolean isInDictionary(@NotNull IdyotaDictionary dictionary, String replacement) {
        return dictionary.isInDictionary(replacement);
    }

    @Contract(value = "_, null -> false", pure = true)
    private boolean isSame(@NotNull String word, String replacement) {
        return word.equals(replacement);
    }
}
