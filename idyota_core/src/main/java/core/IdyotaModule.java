package core;

import core.data.AppConst;
import core.dictionary.Compressor;
import core.fixer.Fixer;
import core.fixer.NaiveFixer;
import core.fixer.Token;
import core.levenstein.DistanceEditor;
import core.util.model.CoreMode;
import io.vavr.collection.Stream;
import io.vavr.control.Try;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static core.data.AppConst.EXAMPLE_PATH;
import static core.data.AppConst.TIME.MILLIS;
import static core.data.AppConst.TIME.NANOS;
import static core.data.LanguageConst.LETTERS;
import static core.util.io.LogConst.*;
import static core.util.model.CoreMode.from;
import static java.lang.Math.abs;
import static java.lang.String.join;
import static java.time.Duration.between;
import static java.time.LocalTime.now;

/**
 * Core class on this module, provides all method required to use business logic.
 */
@Slf4j
@SuppressWarnings("SpellCheckingInspection")
public class IdyotaModule {

    private static final int SAFETY_LIMIT = 20;
    private static final Fixer fixer = new NaiveFixer();
    private static final DistanceEditor editor = new DistanceEditor(LETTERS);
    private static final List<String> examples = List.of("berent", "conrad", "domanska", "doyle", "poe");

    /**
     * @param version application version
     */
    public IdyotaModule(String version) {
        log.info("Init Idyota Module: {}", version);
    }

    private static List<Token> fixString(String txt) {
        return fixer.solve(txt);
    }

    /**
     * Resolves type of mode that will be performed.
     *
     * <p>
     * Default mode is {@link CoreMode#FIX}, and it has full functioning implementation,
     * as well as the proper REST responce.
     * </p>
     * <p>
     * {@link CoreMode#DIST} can be used in future to create and get new posibble replacements
     * for the particular word
     * </p>
     * <ul>
     *     Possible modes are in {@link CoreMode}
     *     <li>{@link CoreMode#DIST}</li>
     *     <li>{@link CoreMode#EXAMPLES}</li>
     *     <li>{@link CoreMode#COMPRESS}</li>
     *     <li>{@link CoreMode#FIX}</li>
     *     <li>{@link CoreMode#LEV}</li>
     * </ul>
     *
     * @param mode    string value of {@link CoreMode} type
     * @param examine provided text to analyze
     * @param lines   file lines
     * @return list of {@link Token} of empty list if text of file doesn't fit requirements
     * of the algorithms
     */
    @SuppressWarnings("SpellCheckingInspection")
    public List<Token> tokenize(final String mode, final String examine, List<String> lines) {
        switch (from(mode)) {
            case DIST:
                return distance(examine); // "przysnęła"
            case EXAMPLES:
                return examples(examples);
            case COMPRESS:
                return compress(examine);
            case FIX:
                return fix(lines);
            case LEV:
                return levenstein(examine);
            default:
                return List.of();
        }
    }

    /**
     * @param target checked word
     * @return suggestions for word
     */
    private List<Token> distance(String target) {
        return Stream
            .ofAll(editor.getClosestWords(target, 0))
            .zipWithIndex()
            .map(word -> new Token(target, word._1(), "", word._2().toString()))
            .toJavaList();
    }

    /**
     * <p>
     * If examine <b>is</b> presented it will use compress algorithms to analyze examine text and return Tokens.
     * <p>
     * If examine <b>is not</b> presented then analyze will be performed over sample files and only logs will be
     * performed.
     * </p>
     *
     * @param examine analyze text
     * @link idyota_core/src/main/resource/assets
     */
    @NotNull
    private List<Token> compress(String examine) {
        return examine != null
            ? Compressor.compress(examine)
            : Compressor.endingCompression();
    }

    /**
     * Return value is limited to {@link #SAFETY_LIMIT} elements to increase performance.
     *
     * @param target search word
     * @return list of tokens if fits {@link DistanceEditor#getDistantWords} requirements
     * or empty list
     */
    @SuppressWarnings({"SameParameterValue", "SpellCheckingInspection"})
    private List<Token> levenstein(@NonNull final String target) {
        final var info = "limited to " + SAFETY_LIMIT + " elements";
        return Stream
            .ofAll(editor.getDistantWords(target))
            .take(SAFETY_LIMIT)
            .zipWithIndex()
            .map(word -> new Token(target, word._1(), info, word._2().toString()))
            .toJavaList();
    }

    /**
     * @param examples filenames, without extension and path
     * @return matrix of token instances, each upper list element has a list of token of one file
     */
    @NotNull
    @Contract("_ -> new")
    @SuppressWarnings("SameParameterValue")
    private ArrayList<Token> examples(@NotNull final List<String> examples) {
        examples.forEach(fileName -> Try
            .of(() -> registerFile(fileName, "EXAMPLES"))
            .onFailure(error -> log.error("Error reading file")));
        return new ArrayList<>();
    }

    private List<Token> fixTxtFile(@NonNull final String fileName) {
        return Try
            .of(() -> {
                Path path = Paths.get(EXAMPLE_PATH + fileName + ".txt");
                byte[] allBytes = Files.readAllBytes(path);
                String testString = new String(allBytes);
                return fixString(testString);
            })
            .onFailure(error -> log.error(FILE_TOKENS_ERROR, fileName, error.getCause()))
            .onFailure(error -> log.warn(FILE_TOKENS_WARN, fileName))
            .getOrElse(LinkedList::new);
    }

    /**
     * Get difference of two nanoTime values.
     */
    @SuppressWarnings("SameParameterValue")
    private List<Token> fix(List<String> lines) {
        return Try
            .of(() -> lines == null
                ? registerFile("input", "FIX")
                : fixString(join("\n", lines)))
            .onFailure(error -> log.error("Error reading file"))
            .get();
    }

    /**
     * all files must be stored in path describe in constant
     * var {@link AppConst#EXAMPLE_PATH}, and has
     * extension '.txt'.
     *
     * @param fileName full path to the examine file with extension
     * @param mode     registered type of execution
     * @return list of token instance
     * @see AppConst#EXAMPLE_PATH
     */
    private List<Token> registerFile(final String fileName, final String mode) {
        LocalTime startTime = now();
        final var tokens = fixTxtFile(fileName);
        LocalTime endTime = now();
        Duration timeElapsed = between(startTime, endTime);
        log.info(EXEC_LOG, mode, fileName, abs(timeElapsed.toNanos()), NANOS.stringify());
        log.info(EXEC_LOG, mode, fileName, abs(timeElapsed.toMillis()), MILLIS.stringify());
        return tokens;
    }
}
