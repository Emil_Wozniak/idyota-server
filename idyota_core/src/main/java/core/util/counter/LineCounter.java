package core.util.counter;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.util.*;

import static core.util.io.LogConst.LOG_KEY_VALUE;
import static java.util.stream.Collectors.toMap;
import static org.slf4j.LoggerFactory.getLogger;

public interface LineCounter {

    Logger log = getLogger(LineCounter.class.getName());

    /**
     * key is value of array, value is counter.
     *
     * @param lines analyzed file lines
     */
    @NotNull
    static Map<String, Integer> linesToHashMap(@NotNull List<String> lines) {
        Map<String, Integer> map = new HashMap<>();
        lines.forEach(line -> {
            map.put(line, map.containsKey(line)
                ? map.get(line) + 1
                : 1); // increase counter if contains
        });
        return map;
    }

    /**
     * <ul>
     * <li style="list-style-type:decimal">Converts Map to List of Map</li>
     * <li style="list-style-type:decimal">Sorts list with Collections.sort(), provides a custom Comparator</li>
     * <li style="list-style-type:decimal">Tries switch the o1 o2 position for a different order</li>
     * <li style="list-style-type:decimal">Loop the sorted list and put it into a new insertion order Map LinkedHashMap</li>
     * </ul>
     */
    static Map<String, Integer> sortByValue(@NotNull Map<String, Integer> unsortedMap) {
        return new LinkedList<>(unsortedMap.entrySet())
            .stream()
            .sorted((o1, o2) -> (o2.getValue()).compareTo(o1.getValue()))
            .collect(toMap(
                Map.Entry::getKey,
                Map.Entry::getValue,
                (a, b) -> b, LinkedHashMap::new));
    }

    static <K, V> void printMap(@NotNull Map<K, V> map, boolean verbose) {
        if (verbose) map.forEach((key, value) -> log.info(LOG_KEY_VALUE, value, key));
    }
}
