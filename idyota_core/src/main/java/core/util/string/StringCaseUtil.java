package core.util.string;

import core.data.AppConst;
import org.jetbrains.annotations.NotNull;

import static core.data.AppConst.LINE_SEPARATOR;
import static core.util.BooleanUtil.isNot;

public interface StringCaseUtil {

    @NotNull
    static String makeUpperCase(@NotNull final String word) {
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }

    static String makeLowerCase(String word) {
        if (word == null || word.equals(""))
            return word;
        return word.substring(0, 1).toLowerCase() + word.substring(1);
    }

    static boolean isUpperCase(@NotNull String word) {
        // TODO: check that only first letter is uppercase
        return word.equals(makeUpperCase(word));
    }

    static String normalizeCase(@NotNull String word, boolean isUpperCase) {
        return word.length() < 1
            ? word
            : isUpperCase
            ? makeUpperCase(word)
            : word;
    }

    static String conditionReplace(String word, boolean isUpper, String replace) {
        if (isUpper) word = replace;
        return word;
    }

    static boolean isNotEquals(@NotNull String word, String wordLike) {
        return isNot(word.equals(wordLike));
    }

    /**
     * TODO: there will be other cases
     *
     * @param word checked word
     * @return true if word equals {@link AppConst#LINE_SEPARATOR}
     * or false in other case
     */
   static boolean isSpecial(@NotNull String word) {
        return word.equals(LINE_SEPARATOR);
    }
}
