package core.util.factory;


import core.dictionary.FlatDictionary;
import core.util.io.IdyotaFileWriter;
import core.util.string.StringCaseUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import static core.data.AppConst.*;
import static core.data.LanguageConst.PUNCTUATION;
import static core.util.factory.FlatDictionariesFactory.DictNames.*;
import static core.util.io.LogConst.DICTIONARY_CREATE;

@Slf4j
@Getter
@NoArgsConstructor
public final class FlatDictionariesFactory implements IdyotaDictionaryFactory<FlatDictionary> {

    private final Library<FlatDictionary> dictionaries = new FlatDictionaries();
    /**
     * Flat dictionaries.
     */
    private FlatDictionary main;
    private FlatDictionary fix;
    private FlatDictionary punctuation;
    private FlatDictionary names;
    private FlatDictionary common;

    @Override
    public void create(IdyotaFileWriter writer) {

        log.info(DICTIONARY_CREATE, MAIN.getName());
        main = new FlatDictionary(MAIN_DICT_PATH, true, writer);
        dictionaries.add(main, MAIN.getName());

        log.info(DICTIONARY_CREATE, FIX.getName());
        fix = new FlatDictionary(FIX_DICT_PATH, true, writer);
        dictionaries.add(fix, FIX.getName());

        log.info(DICTIONARY_CREATE, PUNCT.getName());
        punctuation = new FlatDictionary(PUNCTUATION, writer);
        dictionaries.add(punctuation, PUNCT.getName());

        log.info(DICTIONARY_CREATE, NAMES.getName());
        names = new FlatDictionary(NAMES_DICT_PATH, true, writer);
        dictionaries.add(names, NAMES.getName());

        log.info(DICTIONARY_CREATE, COMMON.getName());
        common = new FlatDictionary(COMMON_DICT_PATH, true, writer);
        dictionaries.add(common, COMMON.getName());
    }

    @Override
    public boolean isName(String candidate) {
        return names.isInDictionary(candidate);
    }

    @Override
    public FlatDictionary getLibrary(@NotNull NamesLibrary name) {
        return this.dictionaries.get(name.getName());
    }

    @Override
    public boolean isCommon(String word) {
        return common.isInDictionary(word);
    }

    @Override
    public boolean isKnown(String word) {

        String lowerCaseWord = StringCaseUtil.makeLowerCase(word);

        if (punctuation.isInDictionary(word))
            return true;

        if (names.isInDictionary(word))
            return true;

        if (main.isInDictionary(word))
            return true;

        return main.isInDictionary(lowerCaseWord);
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Getter
    @AllArgsConstructor
    public enum DictNames implements NamesLibrary {
        MAIN("main"),
        FIX("fix"),
        PUNCT("punctuation"),
        NAMES("names"),
        COMMON("common");
        private final String name;
    }

    public static class FlatDictionaries implements Library<FlatDictionary> {

        Map<String, FlatDictionary> books = new HashMap<>();

        @Override
        public void add(FlatDictionary dictionary, String name) {
            books.put(name, dictionary);
        }

        @Override
        public FlatDictionary get(String name) {
            return books.get(name);
        }
    }
}
