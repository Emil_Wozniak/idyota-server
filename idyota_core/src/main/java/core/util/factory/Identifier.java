package core.util.factory;

public interface Identifier {

    boolean isKnown(String word);
}
