package core.util.factory;

import core.util.io.DictionaryModel;
import core.util.io.IdyotaFileWriter;

public interface IdyotaDictionaryFactory<R extends DictionaryModel> extends DictionaryFactory {

    void create(IdyotaFileWriter writer);

    R getLibrary(NamesLibrary name);
}
