package core.util.factory;

import core.util.io.IdyotaDictionary;

public interface DictionaryFactory extends Identifier {

    boolean isCommon(String word);

    boolean isName(String candidate);

    interface Library<T extends IdyotaDictionary> {

        void add(T dictionary, String name);

        T get(String name);
    }

    interface NamesLibrary {
        String getName();
    }
}
