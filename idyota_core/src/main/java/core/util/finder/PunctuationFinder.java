package core.util.finder;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.IntStream;

import static java.lang.Character.isLetter;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

/**
 * provides methods to checks text
 * in presence of the special signs.
 */
public interface PunctuationFinder {

    /**
     * symbols that can be suppose to find before letter.
     */
    List<Character> punctuationAfter = createPunctuationList(" ,.!?];:„");

    /**
     * symbols that can be suppose to find after letter.
     */
    List<Character> punctuationBefore = createPunctuationList(" ,([„");

    /**
     * separate all symbols, then checks if
     * it is a special symbol. All any is
     * non special symbol it will be extracted.
     *
     * @param symbols special symbols
     * @return list of special characters
     */
    private static List<Character> createPunctuationList(@NotNull final String symbols) {
        return IntStream
                .range(0, symbols.length())
                .mapToObj(symbols::charAt)
                .collect(toList());
    }

    private static String isPunctuation(final Character symbol) {
        return punctuationBefore.contains(symbol)
                ? symbol + " "
                : punctuationAfter.contains(symbol)
                ? " " + symbol :
                String.valueOf(symbol);
    }

    private static String isPunctuation(
            final Character symbol,
            final String[] punctuation,
            final String separator) {
        /* Please no not change line below to Arrays.toString
        it will not work properly */
        String symbols = String.join("", punctuation);
        return createPunctuationList(symbols).contains(symbol)
                ? separator + symbol + separator
                : String.valueOf(symbol);
    }

    /**
     * gets character from text, checks if symbol is
     * special symbol, then adds whitespace using
     * {@link #isPunctuation(Character)} or converts
     * to String. Finally collects symbols.
     *
     * @param text        example to eliminate.
     * @param punctuation special symbols.
     * @return text with special symbols separated
     * from letters using whitespace.
     * @see #punctuationBefore
     * @see #punctuationAfter
     */
    static String addSpaces(@NotNull final String text, final String[] punctuation, final String separator) {
        return IntStream
                .range(0, text.length())
                .mapToObj(text::charAt)
                .map(symbol -> isPunctuation(symbol, punctuation, separator))
                .collect(joining());
    }

    /**
     * gets character from text, checks if symbol is
     * special symbol, then adds whitespace using
     * {@link #isPunctuation(Character)} or converts
     * to String. Finally collects symbols.
     *
     * @param text example to eliminate.
     * @return text with special symbols separated
     * from letters using whitespace.
     * @see #punctuationBefore
     * @see #punctuationAfter
     */
    static String addSpaces(@NotNull String text) {
        return IntStream
                .range(0, text.length())
                .mapToObj(text::charAt)
                .map(PunctuationFinder::isPunctuation)
                .collect(joining());
    }

    /**
     * provided to reduce number of checks.
     *
     * @param symbol check symbol
     * @return true if it is special symbol, otherwise false.
     */
    default boolean isNotSpecialSign(final Character symbol) {
        return isLetter(symbol);
    }

    /**
     *
     * @param symbol any symbol, letter or not
     * @param before sign added before symbol
     * @param after sign added after symbol
     * @return symbol with added signs
     */
    private String isPunctuation(final Character symbol, final String before, final String after) {
        return createPunctuationList(before).contains(symbol)
                ? " " + symbol
                : createPunctuationList(after).contains(symbol)
                ? symbol + " " :
                String.valueOf(symbol);
    }

    /**
     * gets character from text, checks if symbol is
     * special symbol, then adds whitespace using
     * {@link #isPunctuation(Character)} or converts
     * to String. Finally collects symbols.
     *
     * @param text          example to eliminate.
     * @param symbolsBefore special symbols before letters.
     * @param symbolsAfter  special symbols after letters.
     * @return text with special symbols separated
     * from letters using whitespace.
     * @see #punctuationBefore
     * @see #punctuationAfter
     */
    @Deprecated
    default String addSpaces(@NotNull final String text, final String symbolsBefore, final String symbolsAfter) {
        return IntStream
                .range(0, text.length())
                .mapToObj(text::charAt)
                .map(symbol -> isNotSpecialSign(symbol)
                        ? String.valueOf(symbol)
                        : isPunctuation(symbol, symbolsBefore, symbolsAfter))
                .collect(joining("", "", ""));
    }
}
