package core.util.model;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.NonFinal;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static core.util.BooleanUtil.isNot;

@Value
public class TextScanner {

    AtomicInteger compressions = new AtomicInteger(0);
    String oldEnding;
    String addedEnding;
    Boolean useColon;
    Integer endingLength;
    Integer addendumLength;
    Object[] dictionary;
    List<String> lines = new ArrayList<>();

    @NonFinal
    boolean blockWriting = false;
    @NonFinal
    String firstBeginning;
    @NonFinal
    String firstEnding;
    @NonFinal
    String secondBeginning;
    @NonFinal
    String secondEnding;

    @Builder(builderMethodName = "examine", buildMethodName = "check")
    public TextScanner(@NotNull String oldEnding,
                       @NotNull String addedEnding,
                       boolean useColon,
                       Object[] dictionary) {
        this.endingLength = oldEnding.length();
        this.addendumLength = addedEnding.length();
        this.oldEnding = oldEnding;
        this.addedEnding = addedEnding;
        this.useColon = useColon;
        this.dictionary = dictionary;
    }

    @Contract(pure = true)
    public static boolean isNotBlockWriting(@NotNull TextScanner checker) {
        return isNot(checker.blockWriting);
    }

    public void invoke(int index) {
        String firstWord = (String) dictionary[index];
        int firstLength = firstWord.length();
        String secondWord = (String) dictionary[index + 1];
        int secondLength = secondWord.length();

        /*
         * set auxiliary substrings, guarding against overflow.
         */
        if (isWordEnoughSize(firstLength, secondLength)) {
            firstBeginning = firstWord.substring(0, firstLength - endingLength);
            firstEnding = firstWord.substring(firstLength - endingLength, firstLength);
            secondBeginning = secondWord.substring(0, secondLength - addendumLength);
            secondEnding = secondWord.substring(secondLength - addendumLength, secondLength);
        }
        isNotBlockWriting(firstWord, firstLength, secondWord, secondLength);
    }

    private void isNotBlockWriting(
            final String firstWord,
            final int firstLength,
            final String secondWord,
            final int secondLength) {
        if (isNot(blockWriting)) {
            if (firstLength > endingLength &&
                    secondLength > addendumLength &&
                    !secondWord.contains(":") &&
                    !secondBeginning.contains(":") &&
                    firstEnding.equals(oldEnding) &&
                    secondEnding.equals(addedEnding) &&
                    firstBeginning.equals(secondBeginning)) {
                isUsingColon();
                blockWriting = true;
                compressions.getAndIncrement();
            } else {
                lines.add(firstWord);
            }
        } else {
            blockWriting = false;
        }
    }

    private boolean isWordEnoughSize(int firstLength, int secondLength) {
        return firstLength > endingLength && secondLength > addendumLength;
    }

    private void isUsingColon() {
        lines.add(useColon
                ? firstBeginning + ":" + oldEnding + "," + addedEnding
                : firstBeginning + oldEnding + "," + addedEnding);
    }
}

