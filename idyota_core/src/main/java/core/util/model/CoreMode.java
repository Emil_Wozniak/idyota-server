package core.util.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

@Getter
@RequiredArgsConstructor
public enum  CoreMode {
    FIX("fix"),
    COMPRESS("compress"),
    QUIT("quit"),
    DIST("dist"),
    LEV("lev"),
    EXAMPLES("examples");

    static public CoreMode from(@NotNull String mode) {
        return valueOf(mode.toUpperCase());
    }

    private final String mode;

    @NotNull
    public static Boolean isNotPossibleMode(String mode) {
        return Arrays
                .stream(values())
                .noneMatch(it -> it.name().equals(mode));
    }
}
