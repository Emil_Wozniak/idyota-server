package core.util.io;

import java.util.List;

public interface IdyotaFileWriter {

    /**
     * @param array     data
     * @param fileName  url path with filename
     * @param isSorting if true lines will be sort
     */
    void writeFile(final List<String> array, final String fileName, final boolean isSorting);
}
