package core.util.io;

import static core.util.io.Ansi.COLOR.*;
import static core.util.io.Ansi.TEXT.*;

/**
 * Module logs values.
 */
public class LogConst extends Ansi {

    /* ERRORS */
    public final static String WRITE_ERROR_MSG = RED + "Error writing file " + RESET;
    public final static String FILE_TOKENS_ERROR = RED + "Error reading file {}.txt, with cause: {}" + RESET;
    public final static String FILE_TOKENS_WARN = YELLOW + "Abort on reading file " + RESET + UNDERLINE + "{}.txt" + RESET;
    public final static String DICT_NOT_FOUND = RED + "Dictionary file {} not found" + RESET;
    public final static String FILE_ERROR = RED + "Rules file %s not found" + RESET;
    public final static String RULES_CREATED = CYAN + "Fetched: " + BLUE + "{}" + RESET + " rules from: " + ITALIC + "{}" + RESET;
    public final static String FILE_DELETE_FAIL = RED + "Failed to delete the file" + RESET;
    public final static String FILE_MOVE_FAIL = RED + "File moved failed: {} {}" + RESET;

    /* LOGS */
    public final static String VERSION_LOG = HIGH_INTENSITY + "This is: " + ITALIC + "{}" + RESET;
    public final static String EXEC_MODE = HIGH_INTENSITY + "Execute mode: " + RESET + BLUE + "{}" + RESET;
    public final static String EXEC_LOG = CYAN + "Mode: " + BLUE + "{}" + RESET + " file: " + ITALIC + "{}" + RESET + " exec in: " + BLUE + "{}" + RESET + " {}";
    public final static String TEST_PASSED = HIGH_INTENSITY + GREEN + "Both tests passed" + RESET;
    public final static String TEST_END = HIGH_INTENSITY + CYAN + "Test ended" + RESET;
    public final static String FILE_DELETE_OK = GREEN + "File deleted successfully" + RESET;
    public final static String FILE_MOVE_OK = GREEN + "File moved successfully" + RESET;
    public final static String COND_1_OK = GREEN + "condition 1 ok" + RESET;
    public final static String COND_2_OK = GREEN + "condition 2 ok" + RESET;
    public final static String COND_1_FAIL = GREEN + "condition 1 failed" + RESET;
    public final static String COND_2_FAIL = GREEN + "condition 2 failed" + RESET;
    public final static String LINES_READ = CYAN + "Read: " + BLUE + "{}" + RESET + " lines from " + ITALIC + "{}" + RESET;
    public final static String FILE_RESOLVED = CYAN + "File resolved: " + RESET + ITALIC + "{}" + RESET;
    public final static String FILE_CREATED = CYAN + "Created file: " + RESET + ITALIC + "{}" + RESET;
    public final static String FILE_WRITE_COMPLETE = CYAN + "Writing completed: " + RESET + ITALIC + "{}" + RESET;
    public final static String FILE_EXISTS = YELLOW + "File already exists: " + RESET + ITALIC + "{}" + RESET;
    public final static String DICTIONARY_SIZE = CYAN + "Found: " + BLUE + "{}" + RESET + " " + ITALIC + "{}" + RESET;
    public final static String DICTIONARY_COMPRESS = CYAN + "Made " + BLUE + "{}" + RESET + " compressions";
    public final static String DICTIONARY_CREATE = CYAN + "Created: " + RESET + UNDERLINE + "{}" + RESET + " dictionary";
    public final static String LOG_KEY_VALUE = CYAN + "Cases: " + BLUE + "{}" + RESET + " for " + PURPLE + ITALIC + "{} " + RESET;
    public final static String DICTIONARY_LOGGER = CYAN + "Log set: " + BLUE + "{}" + RESET + " for " + ITALIC + "{}" + RESET;
}

