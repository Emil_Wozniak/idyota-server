package core.util.io;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.Value;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static core.util.io.LogConst.DICT_NOT_FOUND;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 *
 */
@Value
@Log4j2
@ToString
@RequiredArgsConstructor
public class AllLinesReader implements IdyotaReader {

    /**
     * @param fileName relative path to the file
     * @return file content as string list
     */
    @Override
    public List<String> linesToList(String fileName) {
        return Try
            .of(() -> Files.readAllLines(Path.of(fileName), UTF_8))
            .onFailure(error -> log.error(DICT_NOT_FOUND, fileName))
            .getOrElse(List.of());
    }
}
