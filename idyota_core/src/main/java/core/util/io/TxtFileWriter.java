package core.util.io;

import io.vavr.control.Try;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import static core.util.io.LogConst.*;
import static java.nio.charset.StandardCharsets.UTF_16;
import static java.nio.file.Files.*;
import static java.nio.file.StandardOpenOption.WRITE;
import static java.util.stream.Collectors.toList;

@Slf4j
@ToString
@NoArgsConstructor
public class TxtFileWriter implements IdyotaFileWriter {

    /**
     * @param lines     data to write
     * @param fileName  url path with filename
     * @param isSorting if true lines will be sort
     */
    @Override
    public void writeFile(final List<String> lines, @NotNull final String fileName, boolean isSorting) {
        int index = fileName.lastIndexOf("/");
        if (index < 1) {
            log.warn("index: {} Skipped", index);
        } else {
            String filePath = fileName.substring(0, index);
            String name = fileName.substring(index + 1);
            Try.run(() -> {
                Path path = Path.of(filePath).resolve(name);
                doFileExist(path);
                tryWrite(doSort(lines, isSorting), path);
            })
                .onFailure(error -> log.error(error.getMessage()))
                .onSuccess(value -> log.info(FILE_WRITE_COMPLETE, fileName));
        }
    }

    private void doFileExist(Path path) throws IOException {
        if (exists(path)) {
            log.warn(FILE_EXISTS, path);
        } else {
            createFile(path);
            log.info(FILE_CREATED, path);
        }
    }

    private List<String> doSort(List<String> lines, boolean isSorting) {
        return isSorting
            ? lines
            .stream()
            .sorted()
            .collect(toList())
            : lines;
    }

    /**
     * @param lines data to write
     * @param file  complete path of the file
     */
    private void tryWrite(List<String> lines, Path file) {
        Try.run(() -> write(file, lines, UTF_16, WRITE))
            .onFailure(error -> log.error(error.getMessage()));
    }
}

