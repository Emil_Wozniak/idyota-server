package core.util.io;

import java.util.List;

public interface IdyotaReader {
    List<String> linesToList(String fileName);
}
