package core.util.io;


import lombok.AllArgsConstructor;

/**
 * Ansi console format properties with codes
 */
public class Ansi {
    public static final String RESET = AnsiCode.RESET.value;

    @AllArgsConstructor
    private enum AnsiCode {
        RESET("\u001B[0m"),
        BLACK("\u001B[30m"),
        RED("\u001B[31m"),
        GREEN("\u001B[32m"),
        YELLOW("\u001B[33m"),
        BLUE("\u001B[34m"),
        PURPLE("\u001B[35m"),
        CYAN("\u001B[36m"),
        WHITE("\u001B[37m"),
        BGD_BLACK("\u001B[40m"),
        BGD_RED("\u001B[41m"),
        BGD_GREEN("\u001B[42m"),
        BGD_YELLOW("\u001B[43m"),
        BGD_BLUE("\u001B[44m"),
        BGD_MAGENTA("\u001B[45m"),
        BGD_CYAN("\u001B[46m"),
        BGD_WHITE("\u001B[47m"),
        SANE("\u001B[0m"),
        HIGH_INTENSITY("\u001B[1m"),
        LOW_INTENSITY("\u001B[2m"),
        ITALIC("\u001B[3m"),
        UNDERLINE("\u001B[4m"),
        BLINK("\u001B[5m"),
        RAPID_BLINK("\u001B[6m"),
        REVERSE_VIDEO("\u001B[7m"),
        INVISIBLE_TEXT("\u001B[8m");

        private String value;
    }

    /**
     * Ansi text colors
     */
    public static class COLOR {
        // ANSI escape code
        public static final String BLACK = AnsiCode.BLACK.value;
        public static final String RED = AnsiCode.RED.value;
        public static final String GREEN = AnsiCode.GREEN.value;
        public static final String YELLOW = AnsiCode.YELLOW.value;
        public static final String BLUE = AnsiCode.BLUE.value;
        public static final String PURPLE = AnsiCode.PURPLE.value;
        public static final String CYAN = AnsiCode.CYAN.value;
        public static final String WHITE = AnsiCode.WHITE.value;
    }

    /**
     * Ansi background colors
     */
    public static class BACKGROUND {
        public static final String BLACK = AnsiCode.BGD_BLACK.value;
        public static final String RED = AnsiCode.BGD_RED.value;
        public static final String GREEN = AnsiCode.BGD_GREEN.value;
        public static final String YELLOW = AnsiCode.BGD_YELLOW.value;
        public static final String BLUE = AnsiCode.BGD_BLUE.value;
        public static final String MAGENTA = AnsiCode.BGD_MAGENTA.value;
        public static final String CYAN = AnsiCode.BGD_CYAN.value;
        public static final String WHITE = AnsiCode.BGD_WHITE.value;
    }

    /**
     * Ansi text format props
     */
    public static class TEXT {
        public static final String SANE = AnsiCode.SANE.value;
        public static final String HIGH_INTENSITY = AnsiCode.HIGH_INTENSITY.value;
        public static final String LOW_INTENSITY = AnsiCode.LOW_INTENSITY.value;
        public static final String ITALIC = AnsiCode.ITALIC.value;
        public static final String UNDERLINE = AnsiCode.UNDERLINE.value;
        public static final String BLINK = AnsiCode.BLINK.value;
        public static final String RAPID_BLINK = AnsiCode.RAPID_BLINK.value;
        public static final String REVERSE_VIDEO = AnsiCode.REVERSE_VIDEO.value;
        public static final String INVISIBLE_TEXT = AnsiCode.INVISIBLE_TEXT.value;
    }
}

