package core.util.io;

public interface Decoder {
    String decodeEntry(final String entry);
}
