package core.util.io;

import java.util.List;

public interface IdyotaDictionary extends DictionaryModel {

    String getWord(final int location);

    int getSize();

    void saveCompressedDictionary(final String fileName, final boolean isSorting);

    void collectEndings(final String targetFile, boolean verbose);

    List<String> compressEnding(
        final String targetFile,
        final String oldEnding,
        final String addedEnding,
        final boolean useColon);

    boolean isInDictionary(final String word);

    int getWordPosition(String word);

}
