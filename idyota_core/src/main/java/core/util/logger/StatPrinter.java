package core.util.logger;

public interface StatPrinter extends GeneralPrinter {

    void addKnown();

    void addFixed();

    void addUnfixed();

    @SuppressWarnings("SpellCheckingInspection")
    void addJota();

    void addEm();

    void addOcr();
}
