package core.util.logger;

public interface GeneralPrinter {

    void clear();

    void print();
}
