package core.util;

import core.fixer.Token;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

public interface IdyotaWordParser {

    static List<Token> parse(@NotNull List<String> words) {
        return words
                .stream()
                .distinct()
                .filter(Objects::nonNull)
                .map(word -> word.split(":"))
                .map(part -> new Token(part[0], part.length > 1 ? part[1] : "", "", ""))
                .collect(toList());
    }
}
