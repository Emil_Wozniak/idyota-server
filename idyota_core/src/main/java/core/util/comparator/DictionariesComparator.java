package core.util.comparator;

import core.util.io.IdyotaDictionary;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.util.stream.IntStream;

import static core.util.BooleanUtil.isNot;
import static org.slf4j.LoggerFactory.getLogger;

public interface DictionariesComparator {

    Logger log = getLogger(DictionariesComparator.class.getName());

    static boolean compare(@NonNull final IdyotaDictionary first,
                           @NotNull final IdyotaDictionary second) {
        return IntStream
            .range(0, second.getSize())
            .mapToObj(second::getWord)
            .filter(word -> isNot(first.isInDictionary(word)))
            .peek(log::info)
            .map(word -> true)
            .findFirst()
            .isEmpty();
    }
}
