package core.util;

import core.fixer.Token;
import core.util.io.IdyotaDictionary;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static core.data.AppConst.DICT_TO_COMPRESS;
import static core.data.AppConst.DICT_TO_OUTPUT;
import static core.util.io.LogConst.FILE_MOVE_FAIL;
import static core.util.io.LogConst.FILE_MOVE_OK;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * logger util provides method to log message for
 * one of two cases (Correct of Failure)
 */
public interface IdyotaGlobalUtil {

    Logger logger = getLogger(IdyotaGlobalUtil.class);

    /**
     * takes boolean and instantiate LogUtil
     *
     * @param value result of any boolean operation
     * @return instance of OK on true or failure in other case
     */
    @NotNull
    @Contract("_ -> new")
    static IdyotaGlobalUtil check(boolean value) {
        return value ? new Correct(true) : new Failure(false);
    }

    /**
     * takes boolean and instantiate LogUtil
     *
     * @param value result of any boolean operation
     * @return instance of OK on true or failure in other case
     */
    @NotNull
    static List<Token> checkAndReturn(boolean value, List<String> words) {
        return value
                ? new Correct(true).doReturn(words)
                : new Failure(false).doReturn(List.of());
    }

    /**
     * takes any instance of the LogUtil and perform logger info log
     * with a proper message.
     *
     * @param correct message value on correct
     * @param error   message value on failure
     */
    void thenPrint(String correct, String error);

    void thenSave(IdyotaDictionary dictionary);

    List<Token> doReturn(List<String> words);

    @Value
    @RequiredArgsConstructor
    class Correct implements IdyotaGlobalUtil {

        boolean value;

        @Override
        public void thenPrint(String correct, String error) {
            logger.info(value ? correct : error);
        }

        @Override
        public void thenSave(IdyotaDictionary dictionary) {
            saveFile(dictionary);
        }

        @Override
        public List<Token> doReturn(List<String> words) {
            return IdyotaWordParser.parse(words);
        }

        void saveFile(@NotNull IdyotaDictionary compressed) {

            logger.info("Both tests passed");
            compressed.saveCompressedDictionary(DICT_TO_COMPRESS, false);

            Path hints = Path.of(DICT_TO_COMPRESS);
            Path source = Paths.get(DICT_TO_OUTPUT);
            Try.run(() -> Files.move(source, hints))
                    .onSuccess(res -> logger.info(FILE_MOVE_OK))
                    .onFailure(err -> logger.error(FILE_MOVE_FAIL, err.getLocalizedMessage(), err.getCause()));
        }
    }

    @Value
    @RequiredArgsConstructor
    class Failure implements IdyotaGlobalUtil {

        boolean value;

        @Override
        public void thenPrint(String correct, String error) {
            logger.info(value ? correct : error);
        }

        @Override
        public void thenSave(IdyotaDictionary dictionary) {
        }

        @Override
        public List<Token> doReturn(List<String> words) {
            return IdyotaWordParser.parse(words);
        }
    }
}
