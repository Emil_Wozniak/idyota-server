package core.util;

import org.jetbrains.annotations.Contract;

public interface BooleanUtil {

    @Contract(pure = true)
    static boolean isNot(boolean value) {
        return !value;
    }
}
