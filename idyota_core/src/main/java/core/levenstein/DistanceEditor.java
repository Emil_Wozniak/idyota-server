package core.levenstein;

import core.fixer.BigramDictionary;
import core.util.factory.DictionaryFactory;
import core.util.io.IdyotaDictionary;
import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.IntStream;

import static core.util.BooleanUtil.isNot;
import static java.lang.Math.*;
import static java.util.stream.Collectors.toList;

@Value
@Slf4j
@RequiredArgsConstructor
public class DistanceEditor implements IDistance {

    String[] letters;
    EditDistanceCost cost = new EditDistanceCost();

    @Override
    @SuppressWarnings("SpellCheckingInspection")
    public String getBestSuggestion(final String previous, final String word, @NotNull final List<String> suggestions, final DictionaryFactory identifier, final BigramDictionary bigrams) {

        AtomicInteger best = new AtomicInteger(999999);
        AtomicReference<String> output = new AtomicReference<>("");

        suggestions.forEach(suggestion -> {
            int cost = getEditCost(word, suggestion, identifier);
            int bigramValue = bigrams.getMax() - bigrams.getSlowValue(previous + " " + suggestion);
            if (bigramValue != bigrams.getMax()) log.info(previous + " " + suggestion);
            cost += bigramValue;

            if (cost < best.get()) {
                best.set(cost);
                output.set(suggestion);
            }
        });
        return output.get();
    }

    @Override
    public int getEditCost(final String word, final String suggestion, @NotNull final DictionaryFactory identifier) {
        int cost = calculateDistance(word, suggestion);
        if (identifier.isCommon(suggestion)) cost -= 1;
        return cost;
    }

    @Override
    public List<String> filterSuggestions(@NotNull final List<String> words, @NotNull final IdyotaDictionary dictionary) {
        return Stream
            .ofAll(words)
            .filter(dictionary::isInDictionary)
            .toJavaList();
    }

    @Override
    public int calculateDistance(@NotNull final String firstWord, @NotNull final String secondWord) {
        int firstSize = firstWord.length() + 1;
        int secondSize = secondWord.length() + 1;
        int[][] matrix = new int[firstSize][secondSize];

        matrix[0][0] = 0;
        IntStream
            .range(1, secondSize)
            .forEach(j -> matrix[0][j] = matrix[0][j - 1] + cost.DELETION_COST);

        String securedFirstWord = " " + firstWord;
        String securedSecondWord = " " + secondWord;

        int tempCost;

        for (int i = 1; i != firstSize; ++i) {
            char firstChar = securedFirstWord.charAt(i);
            matrix[i][0] = matrix[i - 1][0] + cost.DELETION_COST;
            for (int j = 1; j != secondSize; ++j) {
                char secondChar = securedSecondWord.charAt(j);
                if (firstChar == secondChar) {
                    matrix[i][j] = matrix[i - 1][j - 1];
                } else {
                    int editCost = cost.getCost(firstChar, secondChar);
                    int matrixPoint = matrix[i][j];
                    matrix[i][j] = editCost + matrix[i - 1][j - 1];

                    if (i != 1 && j != 1
                        && firstChar == securedSecondWord.charAt(j - 1)
                        && securedFirstWord.charAt(i - 1) == secondChar) {
                        tempCost = cost.SWAP_COST + matrix[i - 2][j - 2];
                        if (isCostLessThanMatrixPoint(tempCost, matrixPoint)) matrix[i][j] = tempCost;
                    }

                    tempCost = cost.DELETION_COST + matrix[i - 1][j];
                    if (isCostLessThanMatrixPoint(tempCost, matrixPoint)) matrix[i][j] = tempCost;

                    tempCost = cost.DELETION_COST + matrix[i][j - 1];
                    if (isCostLessThanMatrixPoint(tempCost, matrixPoint)) matrix[i][j] = tempCost;
                }
            }
        }
        return matrix[firstSize - 1][secondSize - 1];
    }

    private boolean isCostLessThanMatrixPoint(int tempCost, int matrix) {
        return tempCost < matrix;
    }

    @Override
    public List<String> getDistantWords(@NotNull final String word) {
        return (word.length() > 6)
            ? getClosestWords(word, 0)
            .stream()
            .map(candidate -> getClosestWords(candidate, locateFirstDifference(word, candidate)))
            .flatMap(Collection::stream)
            .collect(toList())
            : List.of();
    }

    private int locateFirstDifference(@NotNull String word, @NotNull String candidate) {
        int minLength = min(word.length(), candidate.length());
        return IntStream
            .range(0, minLength)
            .filter(i -> word.charAt(i) != candidate.charAt(i))
            .findFirst()
            .orElse(0);
    }

    /**
     * @return array list of words within edit distance 1 to a word given as input.
     */
    @NotNull
    @Override
    public List<String> getClosestWords(@NotNull final String word, final int startFrom) {
        List<String> proposals;
        int length = word.length();

        String current, previous, post;

        proposals = Arrays
            .stream(letters)
            .map(letter -> letter + word)
            .collect(toList());

        proposals.add(word.substring(1, length));

        int start = max(2, startFrom + 1);

        for (int i = start; i <= length; i++) {
            previous = word.substring(0, i);
            post = word.substring(i);
            current = previous.substring(i - 1, i);
            previous = previous.substring(0, previous.length() - 1);
            transform(proposals, previous, current, post);
        }
        Arrays
            .stream(letters)
            .map(letter -> word + letter)
            .forEach(proposals::add);

        return proposals;
    }

    private void transform(final List<String> proposals,
                           final String previous,
                           final String current,
                           final String post) {
        deletions(proposals, previous, current, post);
        insertions(proposals, previous, current, post);
        replacements(proposals, previous, current, post);
        initialReplacements(proposals, previous, current, post);
        transpositions(proposals, previous, current, post);
    }

    private void deletions(final List<String> proposals, @NotNull final String previous, @NotNull final String current, final String post) {
        if (isNot(current.equals(previous.substring(previous.length() - 1)))) {
            proposals.add(previous + post);
        }
    }

    private void insertions(@NotNull final List<String> proposals, final String previous, final String current, final String post) {
        Arrays.stream(letters)
            .filter(letter -> isNot(letter.equals(current)))
            .map(letter -> previous + letter + current + post)
            .forEach(proposals::add);
    }

    private void replacements(@NotNull final List<String> proposals, final String previous, final String current, final String post) {
        Arrays.stream(letters)
            .filter(letter -> isNot(letter.equals(current)))
            .map(letter -> previous + letter + post)
            .forEach(proposals::add);
    }

    private void initialReplacements(@NotNull final List<String> proposals, final String previous, final String current, final String post) {
        Arrays.stream(letters)
            .filter(letter -> previous.length() == 1)
            .map(letter -> letter + current + post)
            .forEach(proposals::add);
    }

    private void transpositions(@NotNull final List<String> proposals, @NotNull final String previous, final String current, final String post) {
        String beforePostWord = previous.substring(0, previous.length() - 1);
        String fin = previous.substring(previous.length() - 1);
        proposals.add(beforePostWord + current + fin + post);
    }
}
