package core.levenstein;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings({"FieldCanBeLocal", "SpellCheckingInspection"})
public class EditDistanceCost {

    public final int DELETION_COST = 95;
    public final int SWAP_COST = 90;
    private final int MAX_CHAR = 500;
    private final int DEFAULT_SUBSTITUTION_COST = 100;
    private final int FORGOTTEN_ALT_COST = 94;
    private final int ADDED_ALT_COST = 94;
    private final int FONETIC_COST = 90;
    private final int VOWEL_MODERNIZATION_COST = 89;
    private final int[][] substitutionCost = new int[MAX_CHAR][MAX_CHAR];

    public EditDistanceCost() {
        initCost();
    }

    private void initCost() {
        for (int i = 0; i < MAX_CHAR; i++)
            for (int j = 0; j < MAX_CHAR; j++) {
                substitutionCost[i][j] = DEFAULT_SUBSTITUTION_COST;
            }

        // writer forgot about alt key

        setCost("l", "ł", FORGOTTEN_ALT_COST);
        setCost("e", "ę", FORGOTTEN_ALT_COST);
        setCost("a", "ą", FORGOTTEN_ALT_COST);
        setCost("z", "ż", FORGOTTEN_ALT_COST);
        setCost("x", "ź", FORGOTTEN_ALT_COST);
        setCost("o", "ó", FORGOTTEN_ALT_COST);
        setCost("n", "ń", FORGOTTEN_ALT_COST);

        // writer held alt key for too long

        setCost("ł", "l", ADDED_ALT_COST);
        setCost("ę", "e", ADDED_ALT_COST);
        setCost("ą", "a", ADDED_ALT_COST);
        setCost("ż", "z", ADDED_ALT_COST);
        setCost("ź", "x", ADDED_ALT_COST);
        setCost("ó", "o", ADDED_ALT_COST);
        setCost("ń", "n", ADDED_ALT_COST);

        // fonetic errors

        setCost("ó", "u", FONETIC_COST);
        setCost("u", "ó", FONETIC_COST);
        setCost("s", "z", FONETIC_COST);
        setCost("z", "s", FONETIC_COST);

        // for old texts

        setCost("j", "i", VOWEL_MODERNIZATION_COST);
        setCost("e", "y", VOWEL_MODERNIZATION_COST);
        setCost("é", "e", 10);
        setCost("é", "y", 11);
    }

    public void setCost(@NotNull String firstLetter, @NotNull String secondLetter, int value) {
        substitutionCost[firstLetter.charAt(0)][secondLetter.charAt(0)] = value;
    }

    public int getCost(char first, char second) {
        return first >= MAX_CHAR || second >= MAX_CHAR
            ? DEFAULT_SUBSTITUTION_COST
            : substitutionCost[first][second];
    }
}

