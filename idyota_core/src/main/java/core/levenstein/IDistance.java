package core.levenstein;

import core.fixer.BigramDictionary;
import core.fixer.WordIdentifier;
import core.util.factory.DictionaryFactory;
import core.util.io.IdyotaDictionary;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IDistance {

    @SuppressWarnings("SpellCheckingInspection")
    String getBestSuggestion(final String previous, final String word, @NotNull final List<String> suggestions, final DictionaryFactory identifier, final BigramDictionary bigrams);

    int getEditCost(final String word, final String suggestion, @NotNull final DictionaryFactory identifier);

    List<String> filterSuggestions(@NotNull final List<String> words, @NotNull final IdyotaDictionary dictionary);

    int calculateDistance(@NotNull final String firstWord, @NotNull final String secondWord);

    List<String> getDistantWords(@NotNull final String word);

    List<String> getClosestWords(@NotNull final String word, final int startFrom);
}
