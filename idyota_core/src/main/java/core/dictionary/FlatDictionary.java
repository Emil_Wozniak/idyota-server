package core.dictionary;

import core.util.io.*;
import core.util.model.TextScanner;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static core.data.LanguageConst.COLON;
import static core.util.BooleanUtil.isNot;
import static core.util.counter.LineCounter.*;
import static core.util.io.LogConst.DICTIONARY_COMPRESS;
import static core.util.model.TextScanner.isNotBlockWriting;
import static java.util.stream.Collectors.toList;

@Slf4j
@Value
public class FlatDictionary implements IdyotaDictionary {

    IdyotaFileWriter writer;
    IdyotaReader reader = new AllLinesReader();
    Decoder decoder = new DictShorthandDecoder();

    /* final idyota.dictionary array */
    Object[] dictionary;
    /* temporary list used to load idyota.dictionary */
    List<String> words = new ArrayList<>();
    /* temporary list used to compress idyota.dictionary */
    List<String> lines = new ArrayList<>();

    /**
     * Constructor for short dictionaries, initialized by a string.
     *
     * @param writer instance of {@link IdyotaFileWriter}
     */
    public FlatDictionary(String words, IdyotaFileWriter writer) {
        this.writer = writer;
        this.dictionary = arrayFromString(words);
        Arrays.sort(dictionary);
    }

    /**
     * Constructor for long dictionaries, initialized from file
     * which can be either compressed or not.
     *
     * @param fileName     url path with new filename
     * @param isCompressed condition describing using method
     * @param writer       instance of {@link IdyotaFileWriter}
     */
    public FlatDictionary(String fileName, boolean isCompressed, IdyotaFileWriter writer) {
        this.writer = writer;
        this.dictionary = isCompressed
                ? arrayFromCompressedFile(fileName)
                : convertFile(fileName);
        Arrays.sort(dictionary);
        log.info(fileName + " dictionary size is " + dictionary.length);
    }

    /**
     * Constructor accepting SORTED object array.
     *
     * @param array  no sorting data
     * @param writer instance of {@link IdyotaFileWriter}
     */
    public FlatDictionary(Object[] array, IdyotaFileWriter writer) {
        this.writer = writer;
        this.dictionary = array; // NO SORTING!
    }

    @Override
    public void saveCompressedDictionary(String fileName, boolean isSorting) {
        writer.writeFile(lines, fileName, isSorting);
    }

    @Override
    public int getSize() {
        return this.dictionary.length;
    }

    @Override
    public String getWord(int location) {
        return (String) this.dictionary[location];
    }

    /**
     * Create word list from compressed file.
     *
     * @param fileName complete file url
     * @return list of word from target file
     */
    @NotNull
    private Object[] arrayFromCompressedFile(final String fileName) {
        decompress(fileName);
        return words.toArray();
    }

    private void decompress(String fileName) {
        String[] uncompressed = convertFile(fileName);
        Decoder decoder = new DictShorthandDecoder();
        Arrays
                .stream(uncompressed)
                .map(decoder::decodeEntry)
                .forEach(entry -> {
                    boolean hasPrefix = entry.contains("|");
                    if (hasPrefix) {
                        processEntryWithPrefix(entry);
                    } else {
                        processEntryWithoutPrefix(entry);
                    }
                });
    }

    /**
     * TODO: handle more than one "|" in a string
     *
     * @apiNote handle more than one "|" in a string
     */
    private void processEntryWithPrefix(@NotNull final String entry) {
        String[] parts = entry.split("\\|");
        String[] prefixes = parts[0].split(","); // if no commas, returns single element
        Arrays.stream(prefixes)
                .forEach(prefix -> {
                    boolean hasEndings = parts[1].contains(":");
                    if (hasEndings) {
                        addPrefixAndEndings(prefix, parts[1]);
                    } else {
                        words.add(prefix + parts[1]);
                    }
                });
        final var contains = (parts[1]).contains(",");
        if (isNot(contains)) {
            words.add(parts[1]);
        }
    }

    private void processEntryWithoutPrefix(@NotNull String entry) {
        boolean hasEndings = entry.contains(":");
        if (hasEndings) {
            addEndings(entry);
        } else {
            words.add(entry);
        }
    }

    private void addPrefixAndEndings(final String prefix, @NotNull final String entry) {
        String[] parts = entry.split(COLON);
        unrollEndings(parts[0], parts[1]);
        unrollEndings(prefix + parts[0], parts[1]);
    }

    private void addEndings(@NotNull String entry) {
        String[] parts = entry.split(":");
        unrollEndings(parts[0], parts[1]);
    }

    private void unrollEndings(final String stem, @NotNull final String endings) {
        String[] postFixes = endings.split(",");
        Arrays.stream(postFixes)
                .map(postfix -> stem + postfix)
                .forEach(words::add);
    }

    /**
     * @param fileName file name to read
     * @return array of file lines
     * or empty array if read failed
     */
    @NotNull
    private String[] convertFile(String fileName) {
        return reader
                .linesToList(fileName)
                .toArray(new String[]{});
    }

    @Override
    public List<String> compressEnding(String targetFile, String oldEnding, String addedEnding, boolean useColon) {
        TextScanner scanner = TextScanner
                .examine()
                .oldEnding(oldEnding)
                .addedEnding(addedEnding)
                .useColon(useColon)
                .dictionary(dictionary)
                .check();

        IntStream.range(0, this.dictionary.length - 1).forEach(scanner::invoke);
        lines.addAll(scanner.getLines());

        if (isNotBlockWriting(scanner)) {
            lines.add((String) dictionary[this.dictionary.length - 1]);
        }
        writer.writeFile(lines, targetFile, true);
        log.info(DICTIONARY_COMPRESS, scanner.getCompressions().get());
        return lines;
    }

    /**
     * @param targetFile file name, with extension, without path
     * @param verbose    while true it will print statistics
     */
    @Override
    public void collectEndings(final String targetFile, boolean verbose) {
        List<String> lines = Arrays
                .stream(this.dictionary)
                .map(word -> (String) word)
                .filter(word -> word.contains(COLON))
                .map(word -> word.split(COLON))
                .map(word -> word[1])
                .filter(data -> isNot(containsUpperCase(data)))
                .collect(toList());

        Map<String, Integer> map = linesToHashMap(lines);
        Map<String, Integer> treeMap = sortByValue(map);
        printMap(treeMap, verbose);
        writer.writeFile(lines, targetFile, true);
    }

    /**
     * TODO: perhaps own binary search, because of
     * https://ai.googleblog.com/2006/06/extra-extra-read-all-about-it-nearly.html
     */
    @Override
    public boolean isInDictionary(String word) {
        return (Arrays.binarySearch(dictionary, word) > -1);
    }

    @Override
    public int getWordPosition(String word) {
        return Arrays.binarySearch(dictionary, word);
    }

    @SuppressWarnings("SameParameterValue")
    private void listToFile(@NotNull final List<String> lines, final String fileName, boolean isSorting) {
        if (isSorting) {
            Arrays.sort(lines.toArray());
        }
        writer.writeFile(lines, fileName, isSorting);
    }

    @NotNull
    @Contract(pure = true)
    private String[] arrayFromString(@NotNull String wordString) {
        String[] data = wordString.split(" ");
        int size = data.length;
        String[] output = new String[size];
        System.arraycopy(data, 0, output, 0, size);
        return output;
    }

    private boolean containsUpperCase(@NotNull String text) {
        return !text.equals(text.toLowerCase());
    }

    @Deprecated
    public void eliminateWord(String targetFile,
                              @NotNull String ending,
                              @NotNull String eliminatedEnding) {
        int length = this.dictionary.length;
        boolean blockWriting = false;
        String firstBeginning = "", firstEnding = "", secondBeginning = "", secondEnding = "";
        int compressions = 0;
        int endingLength = ending.length();
        int addendumLength = eliminatedEnding.length();

        for (int i = 0; i < length - 1; i++) {

            String firstWord = (String) dictionary[i];
            int firstLength = firstWord.length();
            String secondWord = (String) dictionary[i + 1];
            int secondLength = secondWord.length();

            // set auxiliary substrings, guarding against overflow

            if (firstLength > endingLength && secondLength > addendumLength) {

                firstBeginning = firstWord.substring(0, firstLength - endingLength);
                firstEnding = firstWord.substring(firstLength - endingLength, firstLength);
                secondBeginning = secondWord.substring(0, secondLength - addendumLength);
                secondEnding = secondWord.substring(secondLength - addendumLength, secondLength);
            }

            if (!blockWriting) {

                if (firstLength > endingLength &&
                        secondLength > addendumLength &&
                        !secondWord.contains(":") &&
                        !secondBeginning.contains(":") &&
                        firstEnding.equals(ending) &&
                        secondEnding.equals(eliminatedEnding) &&
                        firstBeginning.equals(secondBeginning)
                ) {
                    lines.add(firstBeginning + ending);
                    blockWriting = true;
                    compressions++;
                } else {
                    lines.add(firstWord);
                }

            } else {
                blockWriting = false;
            }
        }

        // last entry

        if (!blockWriting) {
            lines.add((String) dictionary[length - 1]);
        }

        writer.writeFile(lines, targetFile, true);
        log.info("Made " + compressions + " compressions");

    }

    @Deprecated(forRemoval = true)
    public void removeDuplicates() { // slow as hell
        List<String> lines = new ArrayList<>();
        int length = this.dictionary.length;
        IntStream
                .range(0, length).forEach(i -> {
            String word = (String) dictionary[i];
            log.info(String.valueOf(i));
            if (!lines.contains(word)) {
                lines.add(word);
            }
        });
        //noinspection SpellCheckingInspection
        listToFile(lines, "C:/dict/undoubled.txt", true);
    }
}

