package core.dictionary;

import core.util.io.Decoder;
import io.vavr.control.Try;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static core.data.AppConst.UNWIND_PATH;
import static java.util.stream.Collectors.toCollection;

@Slf4j
@Value
public class DictShorthandDecoder implements Decoder {

    List<String> shorthand = new LinkedList<>();
    List<String> uncompressed = new LinkedList<>();
    String SEPARATOR = " > ";

    /**
     * Constructor reads a file with unwinding rules.
     * Each line contains a rule written as something like
     * :EGO > :ch,m,e,emu,ego,ej
     * Such rule means that whenever a word processed by this class
     * contains string on the left of a separator " > ",
     * it will be changed to a string on the right of a separator.
     */
    public DictShorthandDecoder() {
        listFromFile(UNWIND_PATH)
            .stream()
            .map(line -> line.split(SEPARATOR))
            .forEach(data -> {
                shorthand.add(data[0]);
                uncompressed.add(data[1]);
            });
    }

    @SuppressWarnings("SameParameterValue")
    private List<String> listFromFile(final String fileName) {
        return Try
            .of(() -> {
                List<String> lines;
                // this is the correct way to do things: wrap fileReader in bufferedReader
                FileReader fileReader = new FileReader(fileName);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                lines = bufferedReader.lines().collect(toCollection(LinkedList::new));
                bufferedReader.close();
                return lines;
            })
            .onFailure(FileNotFoundException.class, error -> log.error("Unwinding file " + fileName + " not found"))
            .onFailure(IOException.class, error -> log.error("Error reading unwinding file " + fileName))
            .getOrElse(List.of());
    }

    @Override
    public String decodeEntry(final String entry) {
        AtomicReference<String> result = new AtomicReference<>(entry);
        shorthand
            .stream()
            .filter(entry::contains)
            .forEach(key -> {
                String replace = uncompressed.get(shorthand.indexOf(key));
                result.set(entry.replace(key, replace));
            });
        return result.get();
    }
}
