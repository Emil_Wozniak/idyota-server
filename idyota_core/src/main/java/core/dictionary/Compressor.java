package core.dictionary;

import core.fixer.Token;
import core.util.io.IdyotaDictionary;
import core.util.io.IdyotaFileWriter;
import core.util.io.TxtFileWriter;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

import static core.data.AppConst.DICT_TO_COMPRESS;
import static core.data.AppConst.DICT_TO_OUTPUT;
import static core.util.IdyotaGlobalUtil.check;
import static core.util.IdyotaGlobalUtil.checkAndReturn;
import static core.util.comparator.DictionariesComparator.compare;

public interface Compressor {

    static List<Token> endingCompression() {
        IdyotaFileWriter writer = new TxtFileWriter();

        IdyotaDictionary dictionary = new FlatDictionary(DICT_TO_COMPRESS, false, writer);
        dictionary.collectEndings("endings.txt", false);
        dictionary.compressEnding(DICT_TO_OUTPUT, ":a,ach,ami,e,i", "om", false);

        IdyotaDictionary compressed = new FlatDictionary(DICT_TO_COMPRESS, true, writer);
        IdyotaDictionary uncompressed = new FlatDictionary(DICT_TO_COMPRESS, true, writer);

        boolean condition1 = compare(compressed, uncompressed);
        boolean condition2 = compare(uncompressed, compressed);

        check(condition1).thenPrint("condition 1 ok", "condition 1 failed");
        check(condition2).thenPrint("condition 2 ok", "condition 2 failed");
        check(condition1 && condition2).thenSave(compressed);
        return List.of();
    }

    @NotNull
    static List<Token> compress(@NotNull String text) {
        var lines = Arrays.stream(text.split("\n")).toArray();
        IdyotaFileWriter writer = new TxtFileWriter();
        IdyotaDictionary dictionary = new FlatDictionary(lines, writer);
        var oldEnding = ":a,ach,ami,e,i";
        var output = dictionary.compressEnding(DICT_TO_OUTPUT, oldEnding, "om", false);
        return checkAndReturn(output.size() > 0, output);
    }
}
