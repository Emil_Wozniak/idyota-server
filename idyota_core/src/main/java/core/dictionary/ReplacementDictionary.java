package core.dictionary;

import core.util.io.AllLinesReader;
import core.util.io.IdyotaFileWriter;
import core.util.io.TxtFileWriter;
import lombok.ToString;
import lombok.Value;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Value
@Log4j2
@ToString
public class ReplacementDictionary {

    IdyotaFileWriter writer = new TxtFileWriter();

    FlatDictionary inputDict;
    FlatDictionary outputDict;

    // Temp data used by constructors, used only once
    List<String> in = new ArrayList<>();
    List<String> out = new ArrayList<>();

    /**
     * Constructor for simple replacement with optional ending.
     * if ending != "", we will store both original entry and entry
     * with the ending. It compresses -em/-emi dictionary file nicely
     * and might be useful in more situations in the future.
     *
     * @param fileName full path to file with extension
     * @param ending   symbol
     */
    public ReplacementDictionary(String fileName, String ending) {
        Object[] sortedLines = new AllLinesReader()
            .linesToList(fileName)
            .stream()
            .filter(Objects::nonNull)
            .filter(line -> !line.contains("#") && !line.equals(""))
            .sorted()
            .toArray();

        Arrays.stream(sortedLines)
            .map(line -> (String) line)
            .filter(lineString -> lineString.contains(" > "))
            .map(lineString -> lineString.split(" > "))
            .forEach(parts -> {
                in.add(parts[0]);
                out.add(parts[1]);
                if (!ending.equals("")) {
                    in.add(parts[0] + ending);
                    out.add(parts[1] + ending);
                }
            });

        inputDict = new FlatDictionary(in.toArray(), writer);
        outputDict = new FlatDictionary(out.toArray(), writer);
    }

    public ReplacementDictionary(String fileName) {

        String currentLine;
        List<String> lines = new ArrayList<String>();

        try {
            // this is the correct way to do things: wrap fileReader in bufferedReader
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((currentLine = bufferedReader.readLine()) != null) {
                if (!currentLine.contains("#") && !currentLine.equals(""))
                    lines.add(currentLine);
            }
            // close file
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            log.error("Dictionary file " + fileName + " not found");
        } catch (IOException ex) {
            log.error("Error reading dictionary file " + fileName);
        }

        Object[] sortedLines = lines.toArray();
        Arrays.sort(sortedLines);

        for (Object line : sortedLines) {
            String lineString = (String) line;

            if (lineString.contains(">")) {
                String[] parts = lineString.split(" > ");
                in.add(parts[0]);
                out.add(parts[1]);
            } else if (lineString.contains("@")) {
                // TODO: move this knowledge to a config file
                String[] parts = lineString.split(" @ ");
                String stem = parts[0].toLowerCase();

                switch (parts[1]) {
                    case "dz":
                        addEntry(stem + "dz", stem + "c");
                        break;
                    case "ja":
                        addEntry(stem + "ja", stem + "ia");
                        addEntry(stem + "ją", stem + "ią");
                        addEntry(stem + "ję", stem + "ię");
                        addEntry(stem + "je", stem + "ie");
                        addEntry(stem + "ji", stem + "ii");
                        addEntry(stem + "jo", stem + "io");
                        addEntry(stem + "jom", stem + "iom");
                        addEntry(stem + "jami", stem + "iami");
                        addEntry(stem + "jach", stem + "iach");
                        break;
                    case "jum":
                        addEntry(stem + "jum", stem + "ium");
                        addEntry(stem + "jami", stem + "iami");
                        addEntry(stem + "jom", stem + "iom");
                        addEntry(stem + "jów", stem + "iów");
                        addEntry(stem + "jach", stem + "iach");
                        addEntry(stem + "ja", stem + "ia");
                        break;
                    case "ya_ja":
                        addEntry(stem + "ya", stem + "ja");
                        addEntry(stem + "yi", stem + "ji");
                        addEntry(stem + "ye", stem + "je");
                        addEntry(stem + "yę", stem + "ję");
                        addEntry(stem + "yach", stem + "jach");
                        addEntry(stem + "yami", stem + "jami");
                        addEntry(stem + "yom", stem + "jom");
                        addEntry(stem + "yo", stem + "jo");
                        addEntry(stem + "yą", stem + "ją");
                        break;

                    case "ya_ia":
                        addEntry(stem + "ya", stem + "ia");
                        addEntry(stem + "yi", stem + "ii");
                        addEntry(stem + "ye", stem + "ie");
                        addEntry(stem + "yę", stem + "ię");
                        addEntry(stem + "yach", stem + "iach");
                        addEntry(stem + "yami", stem + "iami");
                        addEntry(stem + "yom", stem + "iom");
                        addEntry(stem + "yo", stem + "io");
                        addEntry(stem + "yą", stem + "ią");
                        break;

                    default:
                        log.error("Dictionary " + fileName + " contains unrecognized " + parts[1] + " ending");
                        break;
                }
            }
        }

        inputDict = new FlatDictionary(in.toArray(), writer);
        outputDict = new FlatDictionary(out.toArray(), writer);
    }

    private void addEntry(String first, String second) {
        in.add(first);
        out.add(second);
    }

    public String replaceUnsorted(String word) {
        int size = inputDict.getSize();
        for (int i = 0; i < size; i++) {
            if (word.equals(inputDict.getWord(i))) {
                return outputDict.getWord(i);
            }
        }

        return word;
    }

    public String replace(String word) {
        int position = inputDict.getWordPosition(word);
        return position > -1 ? outputDict.getWord(position) : word;
    }
}

