package core.tokenizer;

import core.util.finder.PunctuationFinder;
import org.jetbrains.annotations.NotNull;

import static core.data.AppConst.LINE_SEPARATOR;
import static core.data.LanguageConst.*;
import static core.util.BooleanUtil.isNot;

public class NaiveTokenizer implements IdyotaTokenizer {

    @Override
    public String[] getStringTokens(final String text) {
        final var cleared = clear(text);
        return isNot(cleared.contains(TILDE))
                ? getStringTokensIncludingDelimiters(cleared, TILDE)
                : isNot(cleared.contains(PIPE))
                ? getStringTokensIncludingDelimiters(cleared, PIPE)
                : getStringTokensUsingSpaces(cleared);
    }

    @NotNull
    private String clear(@NotNull String text) {
        return text
                .replace("\n", " " + LINE_SEPARATOR + " ")
                .replace("  ", " ")
                .replace(" ", "");
    }

    /**
     * 1. add special chars around delimiters;
     * 2. tokenize, separating at special chars;
     *
     * @param text      eliminate text
     * @param separator string that will be used to
     *                  separate special sign in text
     *                  and after all cut return value
     * @return array of string, each string
     * contains word of special sign
     */
    @NotNull
    private String[] getStringTokensIncludingDelimiters(@NotNull final String text, final String separator) {
        return PunctuationFinder
                .addSpaces(text.toLowerCase(), TOKENIZER_PUNCTUATION, separator)
                .split(separator);
    }

    /**
     * works similar to {@link #getStringTokensIncludingDelimiters}
     * but in this case separator will be whitespace.
     *
     * @param text eliminate text
     * @return array of string, each string
     * contains word of special sign
     */
    @NotNull
    private String[] getStringTokensUsingSpaces(@NotNull final String text) {
        return PunctuationFinder
                .addSpaces(text.toLowerCase())
                .split(" ");
    }
}
