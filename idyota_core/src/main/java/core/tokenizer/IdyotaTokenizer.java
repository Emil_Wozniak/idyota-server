package core.tokenizer;

/**
 * separates Tokenizer from implementation.
 */
public interface IdyotaTokenizer {
    String [] getStringTokens(String string);
}
