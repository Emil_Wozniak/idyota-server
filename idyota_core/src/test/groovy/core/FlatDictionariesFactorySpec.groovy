package core


import core.util.factory.FlatDictionariesFactory
import core.util.io.IdyotaFileWriter
import core.util.io.TxtFileWriter
import spock.lang.Shared
import spock.lang.Specification

class FlatDictionariesFactorySpec extends Specification {

    @Shared def factory = new FlatDictionariesFactory()

    def "when candidate word does not exists in dictionary names, then factory will return false"() {
        boolean isName

        given:
        IdyotaFileWriter writer = new TxtFileWriter()
        factory.create(writer)

        when:
        isName = factory.isName("foo")

        then:
        !isName
    }
}
