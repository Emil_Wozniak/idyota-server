package core.levenstein

import spock.lang.Shared
import spock.lang.Specification

import static core.data.LanguageConst.LETTERS

class DistanceEditorSpec extends Specification {

    @Shared
    def editor = new DistanceEditor(LETTERS)

    @SuppressWarnings('SpellCheckingInspection')
    def "closest words should take 2 args and return list of suggestions"() {
        when:
        def results = editor.getClosestWords("przysnęła", 0)

        then: "closest words should return non zero size list"
        results.size() > 0
    }
}
