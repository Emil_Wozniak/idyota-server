package core.tokenizer

import spock.lang.Shared
import spock.lang.Specification

import static config.Text.*
import static core.data.AppConst.LINE_SEPARATOR

class NaiveTokenizerSpec extends Specification {

    @Shared
    private IdyotaTokenizer tokenizer

    void setup() {
        tokenizer = new NaiveTokenizer()
    }

    def "getStringTokens using ~| should not return space elements"() {
        given:
        def word = "~|" + SMALL_TEXT
        when:
        def tokens = tokenizer.getStringTokens(word)
        then:
        Arrays.stream(tokens).allMatch({ !it.contains(" ") })
    }

    def "getStringTokens including delimiters should returns letters and special sign"() {
        given:
        def word = MEDIUM_TEXT
        when:
        def tokens = tokenizer.getStringTokens(word)
        then:
        Arrays.stream(tokens).anyMatch({ it.contains(LINE_SEPARATOR) })
    }

    def "getStringTokens using multiline text should return elements equals 'LINE_SEPARATOR'"() {
        given:
        def word = LONG_TEXT
        when:
        def tokens = tokenizer.getStringTokens(word)
        then:
        Arrays.stream(tokens).anyMatch({ it.contains(LINE_SEPARATOR) })
    }

}
