package core.dictionary

import spock.lang.Specification

class DictShorthandDecoderSpec extends Specification {

    def "DecodeEntry"() {
        given:
        DictShorthandDecoder decoder = new DictShorthandDecoder()

        and:
        def response = decoder.decodeEntry("foo")

        expect:
        response == "---"
    }
}
