package core.dictionary

import core.util.io.TxtFileWriter
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import java.nio.file.Files
import java.nio.file.Path

import static config.Constant.SAVE_PATH
import static core.data.LanguageConst.PUNCTUATION

class FlatDictionarySpec extends Specification {

    @Shared
    def writer = new TxtFileWriter()
    @Shared
    def dictionary = new FlatDictionary(PUNCTUATION, writer)

    @SuppressWarnings('unused')
    def cleanupSpec() {
        writer = null
        dictionary = null
    }

    @Unroll
    def "when create Punctuation FlatDictionary, first element of dictionary is '!'"() {
        when:
        String word = dictionary.getWord(0)
        then:
        word == "!"
    }

    @Unroll
    def "when create Compress FlatDictionary, first element of dictionary is 'a'"() {
        given:
        def dictionary = new FlatDictionary(SAVE_PATH, false, writer)
        when:
        String word = dictionary.getWord(0)
        then:
        word == "a"
    }

    @Unroll
    def "when create Punctuation FlatDictionary, getWord will throw Exception when index does not exists"() {
        when:
        dictionary.getWord(500)
        then:
        def e = thrown(Exception)
        e.cause == null
    }

    @Unroll
    def "when save FlatDictionary, should return true when file already exists"() {
        given:
        def path = Path.of(SAVE_PATH)
        when:
        dictionary.saveCompressedDictionary(SAVE_PATH, false)
        then:
        Files.exists(path)
    }
}
