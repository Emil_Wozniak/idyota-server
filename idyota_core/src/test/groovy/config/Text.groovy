package config

class Text {

    @SuppressWarnings('SpellCheckingInspection')
    public final static def SMALL_TEXT = "wyszedł zniknął uważasz zaczął zniknął, objął zetknął"

    @SuppressWarnings('SpellCheckingInspection')
    public final static def MEDIUM_TEXT = """
Ala ma kota i rybe młota, 
    straszniee się z Ali śmieje chołota, 
    lecz gdy ham jakiś ubliżt Ali, 
    kot rybą mlotem w mordę go wali!
    """

    @SuppressWarnings('SpellCheckingInspection')
    public final static def LONG_TEXT = """
Tago ranka, tak jak i wszystkich poprzednich dni po powrocie z dziewczyną na Samburan, Heyst wyszedł na werandę i oparł się łokciami o balustradę w wygodnej pozie gospodarza. Grzbiet górskiego łańcucha, przecinający środek wyspy, odgradzał willę od wschodów słońca, wspaniałych czy pochmurnych, gniewnych czy pogodnych. Mieszkańcy jej nie mogli wnioskować z samego rana o losie budzącego się dnia. Narzucał im się już w pełni rozwoju, jednocześnie z nagłem cofnięciem się wielkiego cienia, gdy słońce ukazywało się z za gór, gorące i suche, pożercze jak oko wroga. Ale Heyst, ongi Numer Pierwszy tej osady w okresie gdy było tam stosunkowo rojno i gwarno, lubował się tem trwaniem rannego chłodu, przyćmionym, powoli ustępującym półmrokiem, nikłem widmem ubiegłej nocy, wonnością jej rosistej, ciemnej duszy, więzionej jeszcze przez chwilę między wielką zorzą na niebie a jaskrawym blaskiem odsłoniętego morza.\n
Heystowi trudno było opanować wrodzoną skłonność do rozmyślań o naturze i skutkach swego ostatniego odstępstwa od obojętnej roli widza. Jednak resztki jego rozbitej filozofji, które go się jeszcze trzymały, nie pozwoliły mu zadać sobie świadomego pytania, jak się to wszystko skończy. Równocześnie zaś nie mógł powstrzymać wrodzonego popędu do obserwacji, rozwiniętego przez długotrwałe a rozmyślne ćwiczenia, i pozostał widzem — może trochę mniej naiwnym, ale (jak odkrył z pewnem zdziwieniem) nie o wiele bardziej przewidującym od przeciętnego typu ludzi. Jak i my wszyscy, ludzie czynu, mógł sobie tylko powiedzieć z nieco sztuczną posępnością:\n
— Zobaczymy!\n
Ten nastrój posępnego zwątpienia ogarniał go jedynie w chwilach samotności. W dniu jego było teraz niewiele chwil samotnych — i nie lubił gdy przychodziły. Tego rana nie miał czasu się martwić. Alma znalazła się przy nim na długo zanim słońce, wzniósłszy się nad grzbiet Samburanu, zmiotło chłodny cień wczesnego poranka i resztę nocnego chłodu z dachu, pod którym mieszkali już przeszło trzy miesiące. Ukazała się jak codzień rano. Usłyszał jej lekkie kroki w wielkim pokoju — gdzie rozpakował ongi rzeczy przybyłe z Londynu; pokój ten był teraz z trzech stron wyłożony książkami do połowy wysokości. Nad półkami cienkie maty na ścianach sięgały sufitu, obciągniętego białym perkalem. W mroku i chłodzie połyskiwały tylko złocone ramy portretu starego Heysta, malowanego przez słynnego artystę; portret ten wisiał samotnie na środku ściany.\n
— Czy wiesz o czem myślałem? — spytał Heyst, nie odwracając się.\n
— Nie — odrzekła. Z głosu jej przebijał zawsze cień niepokoju, jak gdyby nigdy nie była pewną dokąd rozmowa z nim zaprowadzi. Oparła się o poręcz obok niego.\n
— Nie — powtórzyła. — A o czem? — Czekała; potem rzekła, niechętnie raczej niż nieśmiało: — Czy myślałeś o mnie?\n
— Zgadywałem kiedy przyjdziesz — rzekł Heyst, wciąż jeszcze nie patrząc na dziewczynę, którą nazwał ostatecznie Leną po wielu próbach i zestawieniach pojedynczych sylab i liter.\n
Po chwili milczenia zauważyła:\n
— Nie byłam bardzo daleko od ciebie.\n
— Widać dla mnie to było za daleko.\n
— Mogłeś zawołać, jeżeliś chciał abym przyszła. Ale nie czesałam się długo.\n
— Widać dla mnie było to za długo.\n
— A więc w każdym razie myślałeś o mnie. Cieszę się z tego. Wiesz, mam takie jakieś wrażenie, że gdybyś przestał o mnie myśleć, nie byłoby mnie wcale na świecie!\n
Odwrócił się i spojrzał na nią. Mówiła często rzeczy, które go zdumiewały. Nieokreślony uśmiech znikł z jej ust pod badawczym jego wzrokiem.\n
— Co chcesz przez to powiedzieć? — zapytał. — Czy to wymówka?\n
— Jakto wymówka? skądżeby! — broniła się.\n
— Więc co to miało znaczyć? — pytał z naciskiem.\n
— To co powiedziałam — tylko to co powiedziałam. Dlaczego jesteś niesprawiedliwy?\n
— Ależ to już naprawdę wymówka!\n
Zaczerwieniła się aż do nasady włosów.\n
— Tak to wygląda, jak gdybyś chciał dowieść że jestem nieprzyjemna — szepnęła. — Więc jestem nieprzyjemna? Będę się teraz bała otworzyć usta. I wkońcu uwierzę, że jestem do niczego.\n
Spuściła zlekka głowę. Patrzył na jej gładkie, niskie czoło, cień rumieńca na policzkach i ponsowe, rozchylone wargi, między któremi błyszczały zęby.\n
— A wtedy będę rzeczywiście do niczego — dodała z przekonaniem. — Będę do niczego! Mogę być tylko tem, za co ty mnie uważasz.\n
Poruszył się nieznacznie. Położyła mu rękę na ramieniu, nie podnosząc głowy i mówiła żywo, stojąc wciąż bez ruchu:\n
— Tak już jest. I nie może być inaczej między kobietą taką jak ja i takim jak ty mężczyzną. Jest nas tu tylko dwoje i nie umiem nawet powiedzieć gdzie jesteśmy.\n
— W bardzo znanem miejscu kuli ziemskiej — wyrzekł Heyst łagodnie. — W swoim czasie ukazało się przynajmniej pięćdziesiąt tysięcy prospektów — a jeszcze prawdopodobniej sto pięćdziesiąt tysięcy. Zajmował się tem mój przyjaciel, którego zamiary bardzo były rozległe, a przekonania silnie ugruntowane. Z nas dwóch on posiadał wiarę. Ale! Z pewnością rozesłano sto pięćdziesiąt tysięcy.\n
— O czem ty mówisz? — spytała cicho.\n
— I cóżbym ci mógł zarzucić? — ciągnął Heyst. — Ze jesteś miła, dobra, pełna wdzięku — i ładna?\n
Zapadło milczenie. Wreszcie odezwała się:\n
— To dobrze że tak o mnie myślisz. Niema tu nikogo ktoby coś o nas myślał — dobrego czy złego.\n
Niezwykły dźwięk jej głosu nadawał specjalną wartość temu co mówiła. Heyst zdawał sobie sprawę, że nieokreślone wzruszenie, jakie go ogarniało przy niektórych jej intonacjach, bardziej było fizyczne niż duchowe. Za każdym razem gdy do niego mówiła, zdawała się coś mu z siebie dawać — coś niezmiernie subtelnego i nieuchwytnego, na co był nieskończenie wrażliwy i czego brakowałoby mu okropnie, gdyby jej nie stało. Patrzył jej w oczy, a ona podniosła nagie ramię, nie zakryte krótkim rękawem i trzymała je w powietrzu póki go nie zauważył i nie pośpieszył przytknąć wielkich, bronzowych wąsów do białej skóry. Potem wrócili do pokoju.\n
Wang ukazał się natychmiast przed frontem domu i, przykucnąwszy na piętach, zaczął grzebać tajemniczo koło roślin u stóp werandy. Gdy Heyst z Leną znowu się ukazali, Chińczyk ulotnił się po swojemu, jakby znikł z życia raczej niż z widoku, jakby się rozpłynął, nie poruszywszy się wcale. Zeszli ze schodów, patrząc sobie w oczy, i ruszyli żwawo przez ogołocony grunt; nie uszli jednak i dziesięciu kroków, gdy Wang zmaterializował się w pustym pokoju bez żadnego uchwytnego ruchu ani szmeru. Stał nieruchomo i błądził oczami po ścianach, jakby szukał jakichś znaków czy napisów; oglądał podłogę, jakby dopatrywał się wilczych dołów albo zgubionych pieniędzy. Przechylił zlekka głowę na ramię, spojrzawszy na profil starego Heysta, który trzymał pióro nad białym arkuszem papieru leżącym na szkarłatnem suknie; wreszcie posunął się bezgłośnie o krok naprzód i zaczął sprzątać ze stołu.\n
Choć robił to bez pośpiechu, nieomylna dokładność jego ruchów i absolutna bezdźwięczność tej krzątaniny nadawały jej charakter czarodziejskiej sztuki. Wykonawszy sztukę, Wang znikł ze sceny aby zmaterializować się niebawem przed willą. Zaczął się od niej oddalać bez widocznego ani zrozumiałego celu; ale uszedłszy jakieś dziesięć kroków, zatrzymał się, zrobił pół obrotu i osłonił ręką oczy. Słońce wzniosło się nad szary grzbiet górski Samburanu. Wielki cień poranny już zniknął, a w oddali, w zachłannym blasku, Wang zdążył dostrzec Numer Pierwszy i tę kobietę, dwie białe plamki na ciemnej smudze lasu. Po chwili znikli. Wang znikł także ze słonecznej polanki, nie wykonawszy prawie żadnego ruchu.\n
Heyst i Lena weszli w cień leśnej ścieżki, która przecinała wyspę i w pobliżu najwyższego swego punktu była zagrodzona przez ścięte drzewa. Ale nie zamierzali zajść tak daleko. Jakiś czas trzymali się ścieżki i zboczyli z niej w miejscu, gdzie las był bez podszycia, a drzewa udrapowane ljanami stały jedno opodal drugiego, w mroku płynącym z pod ich własnych konarów. Gdzieniegdzie leżały na ziemi wielkie bryzgi światła. Heyst i Lena posuwali się w milczeniu wśród wielkiej ciszy, napawając się spokojem, zupełnem odcięciem od świata, wypoczynkiem podobnym do snu bez marzeń. Wynurzyli się wśród skał u górnego krańca lasu i stanęli we wgłębieniu spadzistego zbocza, podobnem do małej platformy; odwróciwszy się, spojrzeli zwysoka na bezludne morze o barwie startej przez blask słońca, o zasnutym upalną mgłą horyzoncie, który rozpływał się w nieuchwytnem migotaniu bladego i oślepiającego bezmiaru pod ciemniejszym blaskiem nieba.\n
— W głowie mi się kręci — szepnęła Lena, zamykając oczy i kładąc rękę na ramieniu towarzysza.\n
Heyst, patrząc nieruchomo ku południowi, wykrzyknął:\n
— O, żagiel!\n
Nastała chwila milczenia.\n
— Musi być bardzo daleko — ciągnął Heyst. — Nie mogłabyś go chyba dojrzeć. To pewno jakiś statek krajowców w drodze na Molukki. Chodźmy, nie trzeba stać w słońcu.\n
Objął ją ramieniem i poprowadził nieco dalej; usiedli razem w cieniu — Lena usadowiła się na ziemi, a on położył się trochę niżej u jej nóg.\n
— Nie lubisz patrzeć tak z góry na morze? — rzekł po chwili.\n
Zaprzeczyła ruchem głowy. Ta pusta przestrzeń działała na nią jak widok okropnego jakiegoś odludzia. Ale powiedziała tylko:\n
— Dostaję zawrotu głowy.\n
— Od tego ogromu?\n
— Od tej pustki. I serce ściska mi się na ten widok — dodała pocichu, jak gdyby wyznając mu jakąś tajemnicę.\n
— Zdaje mi się niestety — rzekł Heyst — że masz prawo czuć do mnie żal za te twoje uczucia. Ale co na to poradzić?\n
Ton jego był żartobliwy, lecz oczy patrzyły poważnie w jej twarz. Zaprzeczyła z żywością.\n
— Z tobą nie czuję się wcale samotna — ani trochę. Tylko kiedy stoimy w tamtem miejscu i kiedy patrzę na wszystką tę wodę i na ten straszny blask...\n
— Więc nigdy już tu nie przyjdziemy — przerwał.\n
Milczała przez chwilę, patrząc mu w oczy, dopóki wzroku nie odwrócił.\n
— Wydaje mi się, że wszystko co tam było zapadło się w morze — rzekła.\n
— To przypomina ci historję potopu — mruknął mężczyzna wyciągnięty u jej stóp, patrząc na nie. — Czy właśnie to cię przestrasza?\n
— Bałabym się zostać sama na świecie. Mówię sama, ale myślę naturalnie o nas obojgu.\n
— O nas obojgu? — Heyst zamilkł na chwilę. — Wizja zatopionego świata — rozmyślał głośno. — Żałowałabyś go?\n
— Żałowałabym szczęśliwych ludzi, którzy na nim żyli — rzekła z prostotą.\n
Przesunął wzrok w górę po jej postaci i dotarł do twarzy, gdzie zdawał się dostrzegać zamglony błysk inteligencji, jak widzi się czasem słońce przeświecające przez chmury.\n
— A jabym uważał, że właśnie im trzebaby powinszować najgoręcej. Nieprawdaż?\n
— O tak, rozumiem co masz na myśli; ale to trwało przecież czterdzieści dni, zanim się wszystko skończyło.\n
— Widzę że pamiętasz wszystkie szczegóły.\n
Heyst odezwał się tylko aby cośkolwiek powiedzieć i nie przyglądać się jej w milczeniu. Nie patrzyła na niego.\n
— To z niedzielnej szkoły — szepnęła. — Chodziłam tam regularnie od ósmego roku życia aż do trzynastego. Mieszkaliśmy w północnej części Londynu, niedaleko Kingsland Road. Niezłe to były czasy. Ojciec dobrze zarabiał. Gospodyni domu posyłała mię wieczorem do szkoły razem ze swemi dziewczynkami. To była dobra kobieta. Mąż jej służył na poczcie; sortował listy, czy coś w tym rodzaju. Taki spokojny człowiek. Po kolacji chodził czasem na nocną służbę. Wtem jednego dnia pokłócili się i dom się rozleciał. Pamiętam jak płakałam, kiedy trzeba było nagle wszystko spakować i przenieść się na inne mieszkanie. Nie dowiedziałam się nigdy o co im poszło, chociaż...\n
— Potop — mruknął Heyst z roztargnieniem.\n
Odczuwał niezmiernie żywo odrębną indywidualność dziewczyny, jak gdyby ta chwila była pierwszą — od czasu wspólnego ich życia — kiedy mógł na nią swobodnie popatrzeć. Szczególny dźwięk jej głosu, mieniący się akcentami śmiałości i smutku, byłby mógł nadać urok najbezmyślniejszej gadaninie. Ale ona nie była gadatliwa. Była raczej małomówna i lubiła trwać bez ruchu w pozie wyprostowanej, jak wówczas gdy siadywała na koncertowej estradzie wśród koleżanek i orkiestry, skrzyżowawszy nogi i złożywszy ręce na kolanach. Obcując z nią stale i patrząc w jej siwe, nieulękłe oczy, Heyst nie mógł się oprzeć wrażeniu, że jest w niej coś niewytłumaczonego; głupota czy natchnienie, słabość czy siła — a może poprostu bezdenna pustka — coś, z czem nie zdradzała się nawet w chwilach zupełnego oddania.\n
Nie patrzyła na niego przez długą chwilę. Wreszcie, jak gdyby słowo „potop“ utkwiło jej w myśli, rzekła, spoglądając w górę na bezchmurne niebo:\n
— Czy też tu kiedy pada?\n
— Jest pora roku, kiedy pada prawie dzień w dzień — rzekł Heyst zdziwiony. — Trafiają się także i burze. Raz mieliśmy deszcz z błota.\n
— Deszcz z błota?\n
— Tamten nasz sąsiad strzelał w górę popiołem. Taki ma już sposób czyszczenia od czasu do czasu rozpalonej do czerwoności gardzieli — i wtedy właśnie nadciągnęła burza. Byliśmy w wielkich opałach, ale zwykle sąsiad nasz zachowuje się przyzwoicie — dymi sobie spokojnie, jak wówczas gdy pokazałem ci po raz pierwszy z pokładu szkunera smugę dymu na niebie. Zacności chłop i leniuch z tego wulkanu.\n
— Widziałam już raz taką dymiącą górę — rzekła, utkwiwszy oczy w smukłym pniu drzewiastej paproci, rosnącej przed nią o jakie dwanaście stóp. — To było niedługo po wyjeździe z Anglji — w kilka dni mniej więcej. Z początku tak chorowałam, że straciłam rachubę czasu. Taka dymiąca góra — nie mogę sobie przypomnieć jak się nazywa.\n
— Może Wezuwjusz? — poddał Heyst.\n
— Tak, tak, właśnie.\n
— Widziałem go już przed laty — przed wiekami — rzekł Heyst.\n
— Wtedy gdy tu jechałeś?\n
— Nie, długo zanim przyszło mi na myśl wybrać się do tej części świata. Byłem jeszcze młodym chłopcem.\n
Odwróciła się i spojrzała na niego uważnie, jak gdyby usiłując odkryć jakiś ślad chłopięctwa w dojrzałej twarzy mężczyzny o włosach przerzedzonych na wierzchu głowy i długich, gęstych wąsach. Heyst zniósł to otwarte badanie z żartobliwym uśmiechem, ukrywając głębokie wrażenie, jakie te zamglone, siwe oczy wywierały — na jego sercu czy nerwach — na zmysłach czy na duszy — czy budziły tkliwość, czy podniecenie — tego nie umiał powiedzieć.\n
— No, księżno Samburanu — rzekł wreszcie — czy znalazłem łaskę w twych oczach?\n
Zdawała się budzić ze snu i potrząsnęła głową.\n
— Myślałam o jednej rzeczy — szepnęła cichutko.\n
— Myśli, czyny — to wszystko są sidła! Jeżeli zaczniesz myśleć, przestaniesz być szczęśliwą.\n
— Nie myślałam o sobie — oświadczyła z prostotą, która zaskoczyła trochę Heysta.\n
— W ustach moralisty brzmiałoby to jak nagana — rzekł nawpół poważnie; — ale nie chcę ciebie podejrzewać o moralizowanie. Już od lat przestałem być w zgodzie z moralistami.\n
Przysłuchiwała się z uwagą.\n
— Domyślałam się że nie masz przyjaciół — rzekła. — Cieszę się że niema tu nikogo, bo nikt nie może ci wyrzucać tego coś zrobił. Miło mi pomyśleć, że nie weszłam nikomu w drogę.\n
Heyst byłby coś odpowiedział, ale nie dała mu przyjść do słowa. Nie spostrzegła że się poruszył i ciągnęła dalej:\n
— Myślałam nad tem, dlaczego tu jesteś?\n
Heyst oparł się znów na łokciu.\n
— Jeśli masz na myśli nas oboje, no to wiesz czemu tu jesteśmy.\n
Spojrzała na niego.\n
— Nie, nie o to mi chodzi. Myślę o tem — co działo się z tobą przez cały ten czas, zanim mię wtedy spotkałeś i zgadłeś odrazu, że jestem w ciężkiem położeniu, że nie mam do kogo się zwrócić. A wiesz dobrze, że to było położenie wprost rozpaczliwe.\n
Głos jej zniżył się przy ostatnich słowach, jak gdyby miała na tem skończyć; ale w postawie Heysta było coś tak wyczekującego, gdy siedział u jej stóp i patrzył na nią spokojnie, że podjęła znów, zaczerpnąwszy szybko powietrza:\n
— Tak, rozpaczliwe. Mówiłam ci że już i przedtem prześladowali mię źli mężczyźni. Czułam się wtedy nieszczęśliwa, niespokojna — i zła. Ale tamten człowiek — ach, jak ja go nienawidziłam, nienawidziłam, nienawidziłam!\n
„Tamten człowiek“ — był to Schomberg o kwitnących policzkach i wojskowym sposobie bycia, dobroczyńca białych ludzi — („przyzwoity posiłek w przyzwoitem towarzystwie“) — dojrzała ofiara spóźnionej namiętności. Dziewczyna wzdrygnęła się. Harmonja właściwa jej twarzy jakby się rozprzęgła na chwilę. Heyst przestraszył się.\n
— Dlaczego o tem myślisz? — zawołał.\n
— Bo tym razem nie miałam już wyjścia. To nie było to samo co przedtem. Było gorzej, o wiele gorzej. Z przerażenia pragnęłam już umrzeć; a jednak dopiero teraz zaczynam rozumieć, jaka mi groziła ohyda. Tak, dopiero teraz, odkąd...\n
Heyst poruszył się zlekka.\n
— Odkąd tu jesteśmy — zakończył.\n
Ciało jej odprężyło się; zaczerwienione policzki odzyskiwały stopniowo zwykłą barwę.\n
— Tak — rzekła obojętnie, lecz jednocześnie rzuciła mu ukradkiem spojrzenie pełne namiętnego podziwu. Fotem twarz jej powlekła się smutkiem, a cała postać osunęła się nieznacznie. — Ale miałeś tu wrócić w każdym razie? — zapytała.\n
— Tak. Czekałem tylko na Davidsona. Tak, miałem tu wrócić, do tych ruin — do Wanga, który może nie spodziewał się mnie już zobaczyć. Niepodobna odgadnąć, w jaki sposób ten Chińczyk rozumuje i co o człowieku myśli.\n
— Nie mów o nim. Tak mi zawsze nieprzyjemnie, kiedy go widzę. Mów mi o sobie.\n
— O sobie? Widzę że zajmuje cię jeszcze tajemnica mego przybycia tutaj; ale niema w tem nic tajemniczego. Więc przedewszystkiem — człowiek z gęsiem piórem w ręku na tym portrecie, któremu tak często się przypatrujesz — otóż ten człowiek jest odpowiedzialny za moje życie. Odpowiedzialny jest także za to czem ono jest, a raczej — czem było. W pewnem znaczeniu był to człowiek wielki. Niewiele wiem o nim. Przypuszczam że wszedł w życie jak każdy z nas: brał piękne słowa za prawdziwą, brzęczącą monetę, a wzniosłe ideały za wartościowe banknoty. Nawiasem mówiąc, był mistrzem w dziedzinie pięknych słów i wzniosłych ideałów. Później odkrył — jakże ci to wytłumaczyć? Wyobraź sobie że świat jest fabryką, a ludzie robotnikami. Otóż mój ojciec odkrył, że pensje były niewystarczające. Wypłacano je fałszywemi pieniędzmi.\n
— Rozumiem! — rzekła wolno Lena.\n
— Naprawdę zrozumiałaś?\n
Heyst, który mówił przedtem jak gdyby do siebie, spojrzał ku niej w górę z zaciekawieniem.\n
— Nie było to nowe odkrycie, ale ojciec włożył w nie całą siłę swej bezgranicznej pogardy. Potęga jej powinna była zniweczyć całą kulę ziemską. Nie wiem ile umysłów przekonał. Ale mój umysł był wówczas bardzo młody, i sądzę że młodość łatwo daje się uwieść — nawet przez negację. Ojciec mój był bardzo bezwzględny, a jednak nie pozbawiony litości. Opanował mię łatwo. Człowiek bez serca nie byłby mógł tego zrobić. Nawet dla głupców miał pewną wyrozumiałość. Umiał się oburzać, ale był za wielki aby drwić i dokuczać. To, co mówił, nie było i nie mogło być przeznaczone dla tłumu; mnie zaś pochlebiało, że znalazłem się wśród wybranych. Ludzie czytali jego książki, ale ja słyszałem jego żywe słowo, któremu nie mogłem się oprzeć. Zdawało mi się że ta dusza otwarła się przede mną, wtajemniczając mnie jednego w mistrzostwo swojej rozpaczy. Myliłem się zapewne. Jest coś z mego ojca w każdym człowieku, który żył już dosyć długo. Ale ludzie nic nie mówią. Nie mogą mówić. Nie umieją wziąć się do tego — a może nie przemówiliby nawet gdyby umieli. Człowiek jest na tej ziemi zjawiskiem przypadkowem, nie wytrzymującem ścisłego badania. Mój ojciec — ten dziwny człowiek — umarł spokojnie jak zasypiające dziecko. Wsłuchawszy się w jego słowa, nie mogłem przyłączyć się do tłumu aby wziąć udział w walce. Zacząłem wędrować po świecie jako niezależny widz — o ile niezależność taka jest możliwa.\n
Szare oczy Leny przez długi czas nie schodziły z jego twarzy. Odkryła że — zwracając się do niej — mówił właściwie do siebie. Nagle Heyst podniósł oczy — zdawało się że dopiero teraz zdaje sobie sprawę z jej obecności — opanował się i roześmiał, zupełnie ton zmieniając:\n
\n
— To wszystko nie jest odpowiedzią na pytanie, dlaczego tu się wogóle znalazłem. I doprawdy — sam nie wiem dlaczego. Poco wdzierać się w niezgłębione tajemnice, których badać właściwie nie warto. Człowiek daje się unosić prądowi. Szczęśliwców prąd niesie ku szczęściu. Nie chcę wmawiać w ciebie, że osiągnąłem powodzenie. I takbyś mi nie uwierzyła. Nie osiągnąłem powodzenia, ale z drugiej strony życie moje nie jest bankructwem — choć tak się wydaje. Fakt, że tu jestem, niczego nie dowodzi, prócz może pewnej ukrytej słabości w moim charakterze — ale nawet i to nie jest pewne.\n
Patrzył na nią nieporuszenie tak poważnemi oczami, że uznała za stosowne uśmiechnąć się do niego nieśmiało, ponieważ go nie zrozumiała. Jej uśmiech odbił się jeszcze słabiej na jego ustach.\n
— To, co powiedziałem, nic ci nie wyjaśnia — ciągnął dalej. — I niema właściwie odpowiedzi na twoje pytanie; ale że fakty mają pewną rzeczywistą wartość — opowiem ci jeden fakt. Spotkałem raz człowieka, który był w położeniu bez wyjścia. Używam tych słów, ponieważ określają dokładnie sytuację tego człowieka i ponieważ sama użyłaś ich przed chwilą. Wiesz dobrze, co to znaczy?\n
— Co też ty mówisz! — szepnęła zdziwiona. — Mężczyzna w takiej sytuacji?\n
Heyst roześmiał się, widząc jej zdumienie.\n
— Och nie! Jego położenie bez wyjścia było innego rodzaju.\n
— Wiedziałam odrazu że to nie mogło być nic takiego — zauważyła cichutko.\n
— Nie chcę cię nudzić długiem opowiadaniem. Może wyda ci się to dziwnem, ale ta historja miała związek z komorą celną. Ten człowiek byłby wolał aby go zabito na miejscu — to znaczy wolałby aby wysłano jego duszę na tamten świat — niż aby go na tym świecie obdarto z jego własności — bardzo zresztą skromnej. Przekonałem się że wierzy w tamten świat, bo gdy się znalazł w położeniu bez wyjścia, jak ci o tem mówiłem, ukląkł i zaczął się modlić. Cóż ty na to?\n
Heyst zamilkł. Patrzyła na niego poważnie.\n
— Nie wyśmiewałeś się z niego z tego powodu? — rzekła.\n
— Moje dziecko, nie jestem gburem! — zawołał i wrócił do poprzedniego tonu: — Nie miałem najmniejszej ochoty do śmiechu. Nie wyglądało to wcale na śmieszną historję. Nie, to nie było zabawne; to było raczej tragiczne; ten człowiek był taką typową ofiarą wielkiego żartu! Ale głupota jest jedynym motorem świata i dlatego koniec końców należy ją uznać za rzecz szacowną. Zresztą ten człowiek był, co się nazywa, dobrym człowiekiem. Nie mówię tego specjalnie z myślą o jego modlitwie. Nie! To był rzeczywiście człowiek uczciwy, zupełnie do tego świata nie przystosowany — zacny człowiek w położeniu bez wyjścia — widowisko zaiste dla bogów; gdyż żaden przyzwoity śmiertelnik nie chciałby czegoś podobnego oglądać. — Wydało się że jakaś myśl błysnęła Heystowi. Zwrócił twarz ku Lenie. — Przecież ty byłaś także w położeniu bez wyjścia — czy przyszła ci wtedy na myśl modlitwa?\n
Oczy jej i twarz pozostały nieruchome. Tylko z ust padły słowa:\n
— Nie jestem — jak to się mówi — porządną dziewczyną.\n
— To brzmi wymijająco — rzekł Heyst po krótkiem milczeniu. — No więc ten zacny człowiek uciekł się do modlitwy; a kiedy mi to później opowiadał, uderzyła mię śmieszność całej sytuacji. Nie, nie rozumiesz mnie — nie mówię naturalnie o samej modlitwie. I nawet wcale nie wydało mi się śmiesznem, że wcielenie Wieczności, Nieskończoności, Wszechmocy wezwano do walki ze spiskiem dwóch nędznych portugalskich metysów. Z punktu widzenia tego, który się modlił, grożące mu niebezpieczeństwo było czemś w rodzaju końca świata — albo czemś jeszcze gorszem. Nie! Co podziałało mi na wyobraźnię, to takt że ja, Aksel Heyst, najbardziej oderwane ze stworzeń więzionych na ziemskim padole, najprawdziwszy włóczęga tej ziemi, obojętny wędrowiec wśród zgiełku świata, — znalazłem się tam aby wystąpić w roli wysłańca Opatrzności. I to właśnie ja, człowiek gardzący wszystkiem i w nic nie wierzący...\n
— Udajesz — przerwała mu przymilnie czarownym swym głosem.\n
— Nie. Taki już jestem z natury, czy z wychowania, czy też z jednego i drugiego. Niedarmo jestem synem swego ojca, tego człowieka z portretu. Jestem zupełnie taki sam, tylko brak mi jego genjuszu. A nawet mniej jeszcze wart jestem niż mi się zdaje, bo i pogarda opuszcza mnie z roku na rok. Nic mnie tak nigdy nie zabawiło jak ten epizod, w którym kazano mi nagle odegrać tak niesłychaną rolę. Przez chwilę sprawiło mi to wielką przyjemność. I wiesz, wydostałem go z tej opresji.\n
— Ocaliłeś człowieka dla zabawy — czy to chcesz powiedzieć? Tylko dla zabawy?\n
— Dlaczego ten podejrzliwy ton? — upomniał ją Heyst. — Sądzę że widok tej strasznej rozpaczy był mi przykry. To, co nazywasz zabawą, nastąpiło później, gdy przyszło mi na myśl że jestem dla niego chodzącym, oddychającym, wcielonym dowodem skuteczności modlitwy. Miało to dla mnie pewien urok — a zresztą jakżebym mógł mu to wyperswadować? Żaden argument nie ostałby się wobec takiego oczywistego faktu, a przytem takby to wyglądało, jakbym chciał sobie przypisać całą zasługę. Już i tak wdzięczność jego była poprostu straszna. Zabawne położenie, prawda? Dopiero w jakiś czas potem przyszedł czas na nudę, kiedy mieszkaliśmy razem na jego statku. Nie spostrzegłem się że stwarzam dla siebie więzy. Nie wiem jak ci to dokładnie wyjaśnić. Człowiek przywiązuje się do ludzi, dla których coś zrobił. Ale czy to była przyjaźń? Nie jestem tego pewien. Wiem tylko że człowiek, który stworzył dla siebie więzy, jest zgubiony. Zaród zepsucia przeniknął do jego duszy.\n
Heyst mówił to lekko z odcieniem żartobliwości, która zaprawiała wszystkie jego wywody i zdawała się wypływać z najgłębszej istoty jego myśli. Dziewczyna z którą się spotkał, którą posiadł, do której obecności jeszcze się nie przyzwyczaił, z którą żyć jeszcze nie umiał — to ludzkie stworzenie tak bliskie, a jednak tak obce, dawało mu większe poczucie rzeczywistości, niż wszystko, z czem zetknął się w życiu.";
"""
}
