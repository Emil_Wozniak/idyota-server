@file:Suppress("SpellCheckingInspection", "PropertyName")

import org.gradle.api.JavaVersion.VERSION_11

plugins {
    java
    groovy
    kotlin("jvm")
}

group = "pl.lang"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.zalando:problem-spring-web")
}

configure<JavaPluginConvention> {
    sourceCompatibility = VERSION_11
    targetCompatibility = VERSION_11
}

tasks {
    compileTestJava {
    }
    test {
        useJUnitPlatform()
    }
}

