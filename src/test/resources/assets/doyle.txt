Artur Conan Doyle

Groźny cień

Rozdział pierwszy. Noc sygnałów

Otom jest, który piszę, ja Jock Calder z West Inch'u, w wieku lat pięćdziesięciu i w połowie dziewiętnastego stulecia jeszcze w pełni władz wszystkich, zdrów na ciele i na duszy.

Żona zaledwie raz na tydzień dopatrzeć może na skroni jakiś tam jeden siwiejący włosek i wyrywa mi je dosyć gniewnie, choć nie rozumiem, co właściwie jej na tem zależy?

Nie mam po prostu czasu myśleć o takich drobnostkach, teraz szczególniej, gdy więcej niż kiedykolwiek czuję, że jestem już człowiekiem z bezpowrotnie minionej epoki, i że życie moje płynęło jednakże w okresie, w którym sposób myślenia i działania różnił się tak od dzisiejszych, jakby ci ludzie pochodzili nie sprzed ćwierć wieku, a z jakiejś innej, dalekiej planety.

Kiedy, naprzykład, przechadzam się wśród pól i zieleniejących łanów, a spojrzę tam, — w stronę Berwick, — dostrzegam liczne, splątane sznurki białawego dymu, który mówi mi o tym dziwnym, nowym potworze, żywiącym się węglem, i który w cielsku swojem ukrywa do tysiąca ludzi, i snuje się bez końca wzdłuż granic.

W dni bardzo pogodne dojrzeć mogę bez trudu nawet czerwone błyski miedzianych kominów, skoro ów potwór kieruje się ku Corriemuir. A gdy obejmę wzrokiem sine, kłębiące się morza, dostrzegam go znowu, czasem dwa, częściej jeszcze kilkanaście żelaznych potworów i od cielsk dziwacznych nie umiem już oderwać oczu… Przejście ich w wodzie znaczy biała smuga, w powietrzu czarny ślad posępny, pod wiatr idą z większą łatwością niż łosoś, płynący w górę Tweed'y.

I wyobrażam sobie, że na podobny widok mój ojciec oniemiałby chyba z zdumienia, a więcej może jeszcze z gniewu, bo starowina miał w duszy tak głęboko zakorzeniony lęk obrażenia czemkolwiek Wszechmocnego Stwórcy, że ani słyszeć nie chciał o „niewoleniu” natury i naginaniu jej do celów ludzkich, a wszelkie inowacye graniczyły w umyśle jego nieledwie z bluźnierstwem.

Bóg stworzył zwierzę pociągowe, konia.
