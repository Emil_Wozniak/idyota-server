package pl.lang.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import pl.lang.web.rest.equalsVerifier

class TokenTest {

    @Test
    fun equalsVerifier() {
        equalsVerifier(Token::class)
        val token1 = Token()
        token1.id = 1L
        val token2 = Token()
        token2.id = token1.id
        assertThat(token1).isEqualTo(token2)
        token2.id = 2L
        assertThat(token1).isNotEqualTo(token2)
        token1.id = null
        assertThat(token1).isNotEqualTo(token2)
    }
}
