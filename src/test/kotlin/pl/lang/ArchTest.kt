package pl.lang

import com.tngtech.archunit.core.importer.ClassFileImporter
import com.tngtech.archunit.core.importer.ImportOption
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses
import org.junit.jupiter.api.Test

class ArchTest {

    @Test
    fun `services and repositories should not depend on web layer`() {

        val importedClasses = ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("pl.lang")

        noClasses()
            .that()
                .resideInAnyPackage("pl.lang.service..")
            .or()
                .resideInAnyPackage("pl.lang.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..pl.lang.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses)
    }
}
