package pl.lang.web.rest

import javax.persistence.EntityManager
import kotlin.test.assertNotNull
import org.hamcrest.Matchers.hasItem
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.Validator
import pl.lang.IdyotaApp
import pl.lang.model.Token
import pl.lang.repo.TokenRepository
import pl.lang.service.token.TokenService
import pl.lang.web.rest.errors.ExceptionTranslator
import pl.lang.web.rest.tokenizer.TokenResource

/**
 * Integration tests for the [TokenResource] REST controller.
 *
 * @see TokenResource
 */
@SpringBootTest(classes = [IdyotaApp::class])
@AutoConfigureMockMvc
@WithMockUser
class TokenResourceIT {

    @Autowired
    private lateinit var tokenRepository: TokenRepository

    @Autowired
    private lateinit var tokenService: TokenService

    @Autowired
    private lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    private lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    private lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var em: EntityManager

    private lateinit var restTokenMockMvc: MockMvc

    private lateinit var token: Token

    @BeforeEach
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val tokenResource = TokenResource(tokenService)
         this.restTokenMockMvc = MockMvcBuilders.standaloneSetup(tokenResource)
             .setCustomArgumentResolvers(pageableArgumentResolver)
             .setControllerAdvice(exceptionTranslator)
             .setConversionService(createFormattingConversionService())
             .setMessageConverters(jacksonMessageConverter)
             .setValidator(validator).build()
    }

    @BeforeEach
    fun initTest() {
        token = createEntity(em)
    }
    @Test
    @Transactional
    @Throws(Exception::class)
    fun getAllTokens() {
        // Initialize the database
        tokenRepository.saveAndFlush(token)

        // Get all the tokenList
        restTokenMockMvc.perform(get("/api/tokens?sort=id,desc"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(token.id?.toInt())))
            .andExpect(jsonPath("$.[*].word").value(hasItem(DEFAULT_WORD)))
            .andExpect(jsonPath("$.[*].replacement").value(hasItem(DEFAULT_REPLACEMENT)))
            .andExpect(jsonPath("$.[*].info").value(hasItem(DEFAULT_INFO)))
            .andExpect(jsonPath("$.[*].identifier").value(hasItem(DEFAULT_IDENTIFIER))) }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getToken() {
        // Initialize the database
        tokenRepository.saveAndFlush(token)

        val id = token.id
        assertNotNull(id)

        // Get the token
        restTokenMockMvc.perform(get("/api/tokens/{id}", id))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(token.id?.toInt()))
            .andExpect(jsonPath("$.word").value(DEFAULT_WORD))
            .andExpect(jsonPath("$.replacement").value(DEFAULT_REPLACEMENT))
            .andExpect(jsonPath("$.info").value(DEFAULT_INFO))
            .andExpect(jsonPath("$.identifier").value(DEFAULT_IDENTIFIER)) }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun getNonExistingToken() {
        // Get the token
        restTokenMockMvc.perform(get("/api/tokens/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound)
    }

    companion object {

        private const val DEFAULT_WORD = "AAAAAAAAAA"
        private const val UPDATED_WORD = "BBBBBBBBBB"

        private const val DEFAULT_REPLACEMENT = "AAAAAAAAAA"
        private const val UPDATED_REPLACEMENT = "BBBBBBBBBB"

        private const val DEFAULT_INFO = "AAAAAAAAAA"
        private const val UPDATED_INFO = "BBBBBBBBBB"

        private const val DEFAULT_IDENTIFIER = "AAAAAAAAAA"
        private const val UPDATED_IDENTIFIER = "BBBBBBBBBB"

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createEntity(em: EntityManager): Token {
            val token = Token(
                word = DEFAULT_WORD,
                replacement = DEFAULT_REPLACEMENT,
                info = DEFAULT_INFO,
                identifier = DEFAULT_IDENTIFIER
            )

            return token
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it,
         * if they test an entity which requires the current entity.
         */
        @JvmStatic
        fun createUpdatedEntity(em: EntityManager): Token {
            val token = Token(
                word = UPDATED_WORD,
                replacement = UPDATED_REPLACEMENT,
                info = UPDATED_INFO,
                identifier = UPDATED_IDENTIFIER
            )

            return token
        }
    }
}
