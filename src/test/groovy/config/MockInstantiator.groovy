package config

import groovyjarjarantlr4.v4.runtime.misc.Nullable
import org.spockframework.mock.CannotCreateMockException
import org.spockframework.runtime.GroovyRuntimeUtil

import static org.spockframework.mock.runtime.MockInstantiator.ObjenesisInstantiator
import static org.spockframework.mock.runtime.MockInstantiator.objenesisAvailable

class MockInstantiator {
    static String msg = ". To solve this problem, put Objenesis 1.2 or higher on the class path (recommended), or supply " + "constructor arguments (error.g. 'constructorArgs: [42]') that allow to construct an object of the mocked type."

    @SuppressWarnings('GroovyAccessibility')
    static def instantiate(Class<?> declaredType, Class<?> actualType, @Nullable List<Object> constructorArgs, boolean useObjenesis) {
        try {
            if (!declaredType.isInterface()
                    && constructorArgs == null
                    && useObjenesis
                    && objenesisAvailable) {
                return ObjenesisInstantiator.instantiate(actualType)
            }
            return GroovyRuntimeUtil.invokeConstructor(
                    actualType,
                    constructorArgs == null
                    ? null
                    : constructorArgs.toArray())
        } catch (Exception error) {
            String msg = constructorArgs == null
                    && useObjenesis
                    && !objenesisAvailable
                    ? msg
                    : null
            throw new CannotCreateMockException(declaredType, msg, error);
        }
    }
}
