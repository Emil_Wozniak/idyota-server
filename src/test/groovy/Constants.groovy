import static java.util.Arrays.stream

class Constants {

    static def MULTIPART = "multipart/form-data"

    static def ANY_WORD = /.*/

    static String FILE_PATH = "src/test/resources/assets/"

    @SuppressWarnings('SpellCheckingInspection')
    static def hints = """
a
a:bym,byś
abakus:,a,ach,ami,em,ie,om,owi,y,ów
abażur:,a,ach,ami,em,om,owi,u,y,ze,ów
abdyka:CJA
abdykow:ano,awszy
abdykowal:IBY
abdykować
abdykował:ABY
abdykuj;CIEŻ
abecad:eł,le
abecadł:a,ach,ami,em,o,om,u
aberra:CJA
abiturien:ci,cie
abiturient:,a,ach,ami,em,om,owi,y,ów
abnega:ci,cie,cja,cjach,cjami,cje,cji,cjo,cję,cyj
"""

    static def replacements = stream(hints.split("\n"))
            .map(word -> word.split(":"))
            .filter({ it.length > 1 })
            .map { it[1] }
            .collect()
}