package pl.lang.web.rest.tokenizer

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.data.domain.PageImpl
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.util.NestedServletException
import pl.lang.model.Token
import pl.lang.repo.TokenRepository
import pl.lang.service.impl.TokenServiceImpl
import pl.lang.service.token.ModuleProvider
import pl.lang.service.token.TokenService
import spock.lang.Specification

import static Constants.*
import static core.util.model.CoreMode.*
import static java.util.Arrays.stream
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart

class TokenResourceSpec extends Specification {

    ObjectMapper mapper = new ObjectMapper()
    ModuleProvider provider
    TokenResource resource
    MockMvc mvc
    TokenService service
    TokenRepository repository

    def uri = "/api/tokens"

    def setup() {
        provider = new ModuleProvider(null)
        repository = Stub(TokenRepository)
        service = new TokenServiceImpl(repository, provider)
        resource = new TokenResource(service)
        mvc = MockMvcBuilders
                .standaloneSetup(resource)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .alwaysDo(MockMvcResultHandlers.print())
                .build()
    }

    def "getCustomers should return list of elements"() {
        when:
        repository.findAll() >> new PageImpl<>(List.of())
        def result = mvc.perform(get(uri)).andReturn()

        then: "status code must be 200 and content type 'application/json'"
        result.response.status == 200
        result.response.contentType == APPLICATION_JSON_VALUE
        result.response.contentAsString == "[]"
    }

    def "getCustomer should return status 200 and json value"() {
        given:
        def path = uri + "/{id}"

        when: "repository #findById() returns optional value"
        repository.findById(1L) >> Optional.of(new Token(1L, "foo", "foo", "foo test", "foo"))
        def result = mvc.perform(get(path, 1)).andReturn()
        def token = mapper.readValue(result.response.contentAsString, Token.class)

        then: "status code must be 200 and content type 'application/json'"
        result.response.status == 200
        result.response.contentType == APPLICATION_JSON_VALUE

        and: "instance should be of type Token"
        token.class == Token.class
        and: "instance should have id property"
        token.id == 1l
    }

    def "getCustomer should return status 404, with no error message and no value if id variable is not correct"() {
        given:
        def path = uri + "/{id}"

        when: "repository #findById() returns optional value"
        repository.findById(1L) >> Optional.empty()
        def result = mvc.perform(get(path, 1)).andReturn()

        then: "status code must be 404 and no error message"
        result.response.status == 404
        result.response.errorMessage == null
    }

    def "when token is FIX getTokensByMode should return non empty list "() {
        given:
        def path = uri + "/mode/" + FIX.name().toLowerCase()

        when:
        def result = mvc.perform(get(path)).andReturn()
        def contents = mapper.readValue(result.response.contentAsString, Token[].class)

        then: "status code must be 200 and content type 'application/json'"
        result.response.status == 200
        result.response.contentType == APPLICATION_JSON_VALUE

        and: "each returned element should be of type Token"
        stream(contents).filter({ it.class != Token.class }).findAll().empty
    }

    def "when token type is 'COMPRESS' getTokensByMode should return list of elements"() {
        given:
        def path = "${uri}/${COMPRESS.name()}/text"
        def params = new LinkedMultiValueMap<String, String>()
        params.add("text", hints)

        when:
        def result = mvc.perform(get(path).params(params)).andReturn()
        def contents = mapper.readValue(result.response.contentAsString, Token[].class)

        then: "status code must be 200 and content type 'application/json'"
        result.response.status == 200
        result.response.contentType == APPLICATION_JSON_VALUE

        and: "none element should have empty word property"
        stream(contents)
                .filter({ it.word == "" && it.word == null })
                .findAll()
                .empty

        and: "some of return elements should have replacement value"
        stream(contents)
                .map(word -> replacements
                        .stream()
                        .anyMatch({ it == word.replacement }))
                .findAny()
    }

    def "when token type is 'DIST' getTokensByMode should return non empty list"() {
        given:
        def modeUri = uri + "/mode/" + DIST.name().toLowerCase()

        when:
        def result = mvc.perform(get(modeUri)).andReturn()
        def contents = mapper.readValue(result.response.contentAsString, Token[].class)

        then: "status code must be 200 and content type 'application/json'"
        result.response.status == 200
        result.response.contentType == APPLICATION_JSON_VALUE

        and: "each returned element should be of type Token"
        stream(contents).filter({ it.class != Token.class }).findAll().empty
    }

    @SuppressWarnings('SpellCheckingInspection')
    def "when token is LEV getTokensByMode should return non empty list "() {
        given:
        def path = uri + "/mode/" + LEV.name().toLowerCase()

        when:
        def result = mvc.perform(get(path)).andReturn()
        def contents = mapper.readValue(result.response.contentAsString, Token[].class)

        then: "status code must be 200 and content type 'application/json'"
        result.response.status == 200
        result.response.contentType == APPLICATION_JSON_VALUE

        and: "each returned element should be of type Token"
        stream(contents)
                .filter({ it.class != Token.class })
                .findAll()
                .empty

        and: "each returned element should has word of 'pqrstyz' "
        stream(contents)
                .filter({ it.word != "pqrstyz" })
                .findAll()
                .empty

        and: "each returned element should has info of 'limited to 20 elements'"
        stream(contents)
                .filter({ it.info != "limited to 20 elements" })
                .findAll()
                .empty
    }

    def "expect thrown NestedServletException when getTokensByMode contains wrong 'mode'"() {
        given:
        def path = uri + "/wrong/text"
        def params = new LinkedMultiValueMap<String, String>()
        params.add("text", hints)
        when:
        mvc.perform(get(path).params(params))
        then:
        thrown(NestedServletException)
    }

    def "when getTokensByMode contains FIX and text"() {
        given:
        def path = uri + "/" + FIX.name() + "/file"
        def file = new File(FILE_PATH + "doyle.txt")
        def inputStream = new FileInputStream(file)
        def multipartFile = new MockMultipartFile("file", file.getName(), MULTIPART, inputStream)

        when:
        def result = mvc.perform(
                multipart(path)
                        .file(multipartFile))
                .andReturn()
        def contents = mapper.readValue(result.response.contentAsString, Token[].class)

        then: "status code must be 200 and content type 'application/json'"
        result.response.status == 200
        result.response.contentType == APPLICATION_JSON_VALUE

        and: "each returned element should be of type Token"
        stream(contents)
                .filter({ it.class != Token.class })
                .findAll()
                .empty

        and: "each result element should not has empty or null word"
        stream(contents)
                .filter({ it.word != "" })
                .filter({ it.word != null })
                .findAll()
                .size() > 0

        and: "some of the results should has replacement"
        stream(contents)
                .filter({ it.replacement ==~ ANY_WORD })
                .findAll()
                .size() > 0
    }
}
