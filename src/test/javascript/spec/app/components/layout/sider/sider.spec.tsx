import React from 'react';
import {shallow} from 'enzyme';
import sinon from 'sinon';
import LoadingBar from 'react-redux-loading-bar';
import {AccountMenu, AdminMenu} from 'app/components/layout/menus';
import Brand from "app/components/layout/brand/brand";
import EntitiesMenu from "app/components/layout/menus/entities";
import LocaleMenu from "app/components/layout/menus/locale";
import AppSider from "app/components/layout/sider/app-sider";
import {Layout, Menu} from 'antd';

const {Sider} = Layout;

describe('AppSider', () => {
    let mountedWrapper;

    const localeSpy = sinon.spy();

    const devProps = {
        isAuth: true,
        isAdmin: true,
        locale: 'en',
        isSwagger: true,
        changeLocale: localeSpy,
    };
    const prodProps = {
        ...devProps,
        isInProd: true,
        isSwagger: false,
    };
    const userProps = {
        ...prodProps,
        isAdmin: false,
    };
    const guestProps = {
        ...prodProps,
        isAdmin: false,
        isAuthenticated: false,
    };

    const wrapper = (props = devProps) => {
        if (!mountedWrapper) {
            mountedWrapper = shallow(<AppSider {...props}/>);
        }
        return mountedWrapper;
    };

    beforeEach(() => {
        mountedWrapper = undefined;
    });

    // All tests will go here
    it('Renders a AppSider component in dev profile with LoadingBar, Navbar, Nav and dev ribbon.', () => {
        const component = wrapper();
        // the created snapshot must be committed to source control
        expect(component).toMatchSnapshot();
        expect(component.find(LoadingBar).length).toEqual(1);
        // noinspection DuplicatedCode
        const sider = component.find(Sider);
        expect(sider.length).toEqual(1);
        expect(sider.find(Brand).length).toEqual(1);
        const menu = component.find(Menu);
        expect(menu.length).toEqual(1);
        expect(menu.find(AdminMenu).length).toEqual(1);
        expect(menu.find(EntitiesMenu).length).toEqual(1);
        expect(menu.find(LocaleMenu).length).toEqual(1);
        expect(menu.find(AccountMenu).length).toEqual(1);
    });

    // noinspection DuplicatedCode
    it('Renders a AppSider component in prod profile with LoadingBar, Navbar, Nav.', () => {
        const component = wrapper(prodProps);
        // the created snapshot must be committed to source control
        expect(component).toMatchSnapshot();
        const sider = component.find(Sider);
        expect(sider.length).toEqual(1);
        expect(sider.find(Brand).length).toEqual(1);
        const menu = component.find(Menu);
        expect(menu.length).toEqual(1);
        expect(menu.find(AdminMenu).length).toEqual(1);
        expect(menu.find(EntitiesMenu).length).toEqual(1);
        expect(menu.find(LocaleMenu).length).toEqual(1);
        expect(menu.find(AccountMenu).length).toEqual(1);

    });

    it('Renders a AppSider component in prod profile with logged in User', () => {
        const menu = wrapper(userProps).find(Menu);
        expect(menu.find(AdminMenu).length).toEqual(0);
        expect(menu.find(EntitiesMenu).length).toEqual(1);
        const account = menu.find(AccountMenu);
        expect(account.first().props().isAuthenticated).toEqual(true);
    });

    it('Renders a AppSider component in prod profile with no logged in User', () => {
        const menu = wrapper(guestProps).find(Menu);
        expect(menu.find(AdminMenu).length).toEqual(0);
        expect(menu.find(EntitiesMenu).length).toEqual(0);
        const account = menu.find(AccountMenu);
        expect(account.length).toEqual(1);
        expect(account.first().props().isAuthenticated).toEqual(false);
    });
});
