import React from 'react';
import {shallow} from 'enzyme';
import sinon from 'sinon';
import LoadingBar from 'react-redux-loading-bar';
import AppHeader from 'app/components/layout/header/app-header';

describe('AppHeader', () => {
    let mountedWrapper;

    const localeSpy = sinon.spy();

    const devProps = {
        isAuthenticated: true,
        isAdmin: true,
        currentLocale: 'en',
        onLocaleChange: localeSpy,
        ribbonEnv: 'dev',
        isInProduction: false,
        isSwaggerEnabled: true,
        classes: null
    };
    const prodProps = {
        ...devProps,
        ribbonEnv: 'prod',
        isInProduction: true,
        isSwaggerEnabled: false,
    };
    const userProps = {
        ...prodProps,
        isAdmin: false,
    };
    const guestProps = {
        ...prodProps,
        isAdmin: false,
        isAuthenticated: false,
    };

    const wrapper = (props = devProps) => {
        if (!mountedWrapper) {
            mountedWrapper = shallow(<AppHeader {...props}/>);
        }
        return mountedWrapper;
    };

    beforeEach(() => {
        mountedWrapper = undefined;
    });

    // All tests will go here
    it('Renders a AppHeader component in dev profile with LoadingBar, Navbar, Nav and dev ribbon.', () => {
        const component = wrapper();
        // the created snapshot must be committed to source control
        expect(component).toMatchSnapshot();
        expect(component.find(LoadingBar).length).toEqual(1);
        const ribbon = component.find('.ribbon.dev');
        expect(ribbon.length).toEqual(1);
    });

    it('Renders a AppHeader component in prod profile with LoadingBar, Navbar, Nav.', () => {
        const component = wrapper(prodProps);
        // the created snapshot must be committed to source control
        expect(component).toMatchSnapshot();
        const ribbon = component.find('.ribbon.dev');
        expect(ribbon.length).toEqual(0);
    });
});
