import React from 'react';
import {shallow} from 'enzyme';
import SideForm from "app/components/idyota/sidebar/side-form";
import {AuthenticationState} from "app/components/reducers/authentication";
import IdyotaRulesRadioGroup from "app/components/idyota/sidebar/idyota-rules-radio-group";
import IdyotaFrequencyButton from "app/components/idyota/sidebar/idyota-frequency-button";
import RulesAndDictionaries from "app/components/idyota/sidebar/rules-and-dictionaries";
import IdyotaUploadFile from "app/components/idyota/sidebar/idyota-upload-file";
import ResetButton from "app/components/idyota/sidebar/reset-button";
import UserPrompt from "app/components/idyota/sidebar/user-prompt";

describe('SideForm', () => {
    let mountedWrapper;

    const devProps = {
        auth: {} as AuthenticationState
    };
    const prodProps = {
        ...devProps,
    };

    const wrapper = (props = devProps) => {
        if (!mountedWrapper) {
            mountedWrapper = shallow(<SideForm {...props}/>);
        }
        return mountedWrapper;
    };

    beforeEach(() => {
        mountedWrapper = undefined;
    });

    // All tests will go here
    it('Renders a AppHeader component in dev profile with ' +
        'IdyotaRulesRadioGroup, IdyotaFrequencyButton, RulesAndDictionaries, ' +
        'IdyotaUploadFile, ResetButton and UserPrompt.', () => {
        const component = wrapper();
        // the created snapshot must be committed to source control
        expect(component).toMatchSnapshot();
        expect(component.find(IdyotaRulesRadioGroup).length).toEqual(1);
        expect(component.find(IdyotaFrequencyButton).length).toEqual(1);
        expect(component.find(RulesAndDictionaries).length).toEqual(1);
        expect(component.find(IdyotaUploadFile).length).toEqual(1);
        expect(component.find(ResetButton).length).toEqual(1);
        expect(component.find(UserPrompt).length).toEqual(1);
    });

    it('Renders a AppHeader component in prod profile with ' +
        'IdyotaRulesRadioGroup, IdyotaFrequencyButton, RulesAndDictionaries, ' +
        'IdyotaUploadFile, ResetButton and UserPrompt.', () => {
        const component = wrapper(prodProps);
        // the created snapshot must be committed to source control
        expect(component).toMatchSnapshot();
        expect(component.find(IdyotaRulesRadioGroup).length).toEqual(1);
        expect(component.find(IdyotaFrequencyButton).length).toEqual(1);
        expect(component.find(RulesAndDictionaries).length).toEqual(1);
        expect(component.find(IdyotaUploadFile).length).toEqual(1);
        expect(component.find(ResetButton).length).toEqual(1);
        expect(component.find(UserPrompt).length).toEqual(1);
    });
});
