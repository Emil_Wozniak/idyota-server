import React from 'react';
import {shallow} from 'enzyme';
import ResultColumn from "app/components/idyota/result/result-column";
import {getTokensByMode} from "app/entities/tokens/token.reducer";
import TextArea from 'antd/lib/input/TextArea';
import TokenSelectors from 'app/components/idyota/result/selector/token-selectors';
import {Descriptions} from "app/components/idyota/result/description/descriptions";
import GridContainer from "app/components/layout/menus/grid/grid-container";

describe('ResultColumn', () => {
    let mountedWrapper;

    const devProps = {
        changes: "",
        getTokens: getTokensByMode,
        tokens: [],
        lines: [],
    };
    const prodProps = {
        ...devProps,
    };

    const wrapper = (props = devProps) => {
        if (!mountedWrapper) {
            mountedWrapper = shallow(<ResultColumn {...props}/>);
        }
        return mountedWrapper;
    };

    beforeEach(() => {
        mountedWrapper = undefined;
    });

    // All tests will go here
    it('Renders a AppHeader component in dev profile with ' +
        'TextArea, TokenSelectors and Descriptions.', () => {
        const component = wrapper();
        // the created snapshot must be committed to source control
        expect(component).toMatchSnapshot();
        expect(component.find(TextArea).length).toEqual(1);

        expect(component.find(GridContainer).length).toEqual(1);

        expect(component.find(TokenSelectors).length).toEqual(1);
        expect(component.find(Descriptions).length).toEqual(1);
    });

    it('Renders a AppHeader component in prod profile with ' +
        'TextArea, TokenSelectors and Descriptions.', () => {
        const component = wrapper(prodProps);
        // the created snapshot must be committed to source control
        expect(component).toMatchSnapshot();
        expect(component.find(TextArea).length).toEqual(1);
        expect(component.find(TokenSelectors).length).toEqual(1);
        expect(component.find(Descriptions).length).toEqual(1);
    });
});
