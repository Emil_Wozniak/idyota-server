import React from 'react';
import {shallow} from 'enzyme';
import IdyotaForm from "app/components/idyota/idyota-form";
import SideForm from "app/components/idyota/sidebar/side-form";
import ResultColumn from "app/components/idyota/result/result-column";

describe('IdyotaForm', () => {
    let mountedWrapper;

    const devProps = {
        account: {},
        entities: []
    };
    const prodProps = {
        ...devProps
    };
    const wrapper = (props = devProps) => {
        if (!mountedWrapper) {
            mountedWrapper = shallow(<IdyotaForm {...props}/>);
        }
        return mountedWrapper;
    };

    beforeEach(() => {
        mountedWrapper = undefined;
    });

    // All tests will go here
    it('Renders a IdyotaForm component in dev profile with SideForm and ResultColumn', () => {
        const component = wrapper();
        // the created snapshot must be committed to source control
        expect(component).toMatchSnapshot();
        expect(component.find(SideForm).length).toEqual(1);
        expect(component.find(ResultColumn).length).toEqual(1);
    });

    it('Renders a IdyotaForm component in prod profile with SideForm and ResultColumn', () => {
        const component = wrapper(prodProps);
        // the created snapshot must be committed to source control
        expect(component).toMatchSnapshot();
        expect(component.find(SideForm).length).toEqual(1);
        expect(component.find(ResultColumn).length).toEqual(1);
    });
});
