import { createStyles, Theme } from '@material-ui/core';

export const ROOT_DARK = '#231e24';
export const SECONDARY_COLOR = '#262065';
export const SECONDARY_COLOR_LIGHT = '#5c489c';
export const SECONDARY_COLOR_VERY_LIGHT = '#c8b2e3';

export const SUBMENU = { background: SECONDARY_COLOR_VERY_LIGHT, color: ROOT_DARK, border: 0, width: '100%' };

export const textarea = { height: '20rem', width: '100%', minHeight: '10rem' };
export const login = { margin: '1rem', width: '25ch' };
export const logCollapse = { background: '#232627' };

export const styles = (theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      flex: 1,
    },
    navigation: {
      width: 800,
      backgroundColor: 'inherit',
    },
    center: {
      alignItems: 'center',
    },
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%',
    },

    input: {
      display: 'none',
    },
    submit: {},
    menuButton: {},
    toolbar: {
      display: 'flex',
      flexDirection: 'row-reverse',
    },
    title: {
      fontSize: 'h6',
    },
    modal: {
      display: 'flex',
      padding: theme.spacing(1),
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
