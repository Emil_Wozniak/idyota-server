import React from 'react';

export const withRef = (WrappedComponent) => {
    const WithRef = ({forwardedRef, ...props}) =>
        (<WrappedComponent ref={forwardedRef} {...props}/>)
    return React.forwardRef((props, ref) => <WithRef {...props} forwardedRef={ref}/>);
};