import { closeCircleRed, outlinedGreen } from 'app/components/icons/icons';

export const isFieldOk = (field: string) => (field !== '' ? outlinedGreen : closeCircleRed);
