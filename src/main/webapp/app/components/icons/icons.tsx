import React from 'react';
import {
    ArrowDownOutlined,
    CheckOutlined,
    CloseCircleOutlined,
    DesktopOutlined,
    EllipsisOutlined,
    RedoOutlined,
    ToolFilled,
    TranslationOutlined,
    UnorderedListOutlined,
    UploadOutlined,
    UserOutlined
} from '@ant-design/icons';

type IconStyle = {
    color: string
}
export const outlinedGreen = () => <CheckOutlined style={{color: "green"}}/>;
export const Check = ({color}: IconStyle) => <CheckOutlined style={{color}}/>;
export const Arrow = ({color}: IconStyle) => <ArrowDownOutlined/>;
export const closeCircleRed = (color?: string) => <CloseCircleOutlined style={{color: color ? color : "red"}}/>
export const CloseCircle = ({color}: IconStyle) => <CloseCircleOutlined style={{color: color ? color : "red"}}/>
export const uploadOutlined = <UploadOutlined/>
export const user = <UserOutlined/>;
export const translation = <TranslationOutlined/>;
export const desktopOutlined = <DesktopOutlined/>;
export const toolFilled = <ToolFilled/>;
export const list = <UnorderedListOutlined/>
export const redo = (color?: string) => <RedoOutlined style={{color}}/>
export const Ellipsis = ({color}: IconStyle) => <EllipsisOutlined color={color}/>

/**
 *
 * @param step current step number
 * @param expected this component step value
 */
export const getExpandIcon = (step: number, expected: number) => () =>
    step < expected
        ? <Arrow color="grey"/>
        : step > expected
        ? <Check color="green"/>
        : <Arrow color="green"/>;


