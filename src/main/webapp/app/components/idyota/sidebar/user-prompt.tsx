import React from 'react';
import {Link} from 'react-router-dom';
import {Translate} from 'react-jhipster';
import {Alert} from 'reactstrap';
import {AuthenticationState} from "app/components/reducers/authentication";

const LoginAlert = ({account}) => (
    <Alert color="success">
        <Translate contentKey="home.logged.message" interpolate={{username: account.login}}>
            You are logged in as user {account.login}.
        </Translate>
    </Alert>
);
const NoAccount = () => (
    <>
        <Alert color="warning">
            <Translate contentKey="global.messages.info.authenticated.prefix">
                If you want to
            </Translate>
            <br/>
            <Link to="/login" className="alert-link">
                <Translate contentKey="global.messages.info.authenticated.link">
                    sign in
                </Translate>
            </Link>
        </Alert>
        <Alert color="waring">
            <Translate contentKey="global.messages.info.register.noaccount">
                You do not have an account yet?
            </Translate>&nbsp;
            <br/>
            <Link to="/account/register" className="alert-link">
                <Translate contentKey="global.messages.info.register.link">
                    Register a new account
                </Translate>
            </Link>
        </Alert>
    </>
);

type UserPromptProps = {
    auth: AuthenticationState
}

const UserPrompt = ({auth}: UserPromptProps) => auth.isAuthenticated
    ? <LoginAlert account={auth.account}/>
    : <NoAccount/>

export default UserPrompt;