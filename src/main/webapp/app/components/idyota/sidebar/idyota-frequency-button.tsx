import React from 'react';
import {frequency} from "app/components/idyota/sideform-labels";
import {Switch} from 'antd';
import {CheckOutlined, CloseOutlined} from '@ant-design/icons';
import SidebarCollapse from "app/components/idyota/sidebar/sidebar-collapse";

type IdyotaFrequencyButtonProps = {
    onChange: Function;
    nextStep: Function;
    step: number;
    reset: boolean;
}

const IdyotaFrequencyButton = (props: IdyotaFrequencyButtonProps) => (
    <SidebarCollapse header={frequency} currentKey={1} {...props} center>
        <Switch
            defaultChecked
            onClick={state => props.onChange(state)}
            checkedChildren={<CheckOutlined/>}
            unCheckedChildren={<CloseOutlined/>}
            key="frequency switcher"/>
    </SidebarCollapse>
);

export default IdyotaFrequencyButton;