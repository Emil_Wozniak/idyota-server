export const openCollapse = (nextStep: Function, step: number, setOpen, key: string) => () => {
    nextStep(step + 1);
    setOpen(key)
};

export const activeKey = (reset: boolean, label: string): string | number => !reset && label;

export const handleResetOpen = (reset: boolean, label: string, setLabel: Function) => {
    if (reset && label !== "") setLabel("")
};