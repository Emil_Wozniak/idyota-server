import React, {ChangeEvent, Component} from 'react';
import {connect} from 'react-redux';
import IdyotaRulesRadioGroup from "app/components/idyota/sidebar/idyota-rules-radio-group";
import UserPrompt from "app/components/idyota/sidebar/user-prompt";
import {defaultTokens, Tokens} from "app/entities/tokens/tokens";
import {transformText} from "app/entities/tokens/token.reducer";
import {AuthenticationState} from "app/components/reducers/authentication";
import IdyotaUploadFile from "app/components/idyota/sidebar/idyota-upload-file";
import IdyotaFrequencyButton from "app/components/idyota/sidebar/idyota-frequency-button";
import RulesAndDictionaries from "app/components/idyota/sidebar/rules-and-dictionaries";
import ResetButton from "app/components/idyota/sidebar/reset-button";
import {Card} from 'antd';

interface SideFormState extends Tokens {
    current: number;
    file: any;
    ruleList: string[];
    isFrequencyOn: boolean;
    reset: boolean;
}

export type SideFormForm = {
    rules: string;
}

interface SideFormProps extends StateProps, DispatchProps {
    auth: AuthenticationState;
}

class SideForm extends Component<SideFormProps, SideFormState> {
    constructor(props) {
        super(props);
        this.state = {
            current: 0,
            rules: defaultTokens.rules,
            tokens: [],
            file: null,
            ruleList: [],
            isFrequencyOn: true,
            reset: false
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextState.rules !== this.state.rules) {
            this.setState({rules: nextState.rules})
            return true;
        }
        if (this.state.reset !== nextState.reset) {
            return true;
        }
        return this.state !== nextState;
    }

    resetForm = () =>
        this.setState({
            tokens: [],
            rules: defaultTokens.rules,
            file: null,
            ruleList: [],
            current: 0,
            reset: true
        })

    handleAddRules = (list: string[]) => this.setState({ruleList: list, reset: false})

    handleFrequencyDictionary = (isOn: boolean) => this.setState({isFrequencyOn: isOn})

    onChange = (current) => this.setState({current, reset: false});

    reset = () => this.setState({reset: true})

    render() {
        const {current, reset} = this.state;
        return (
            <section id="form-sidebar" aria-label="form-sidebar">
                <Card>
                    <IdyotaRulesRadioGroup
                        reset={reset}
                        step={current}
                        nextStep={this.onChange}
                        onChange={this.handleAddRules}/>
                    <IdyotaFrequencyButton
                        reset={reset}
                        step={current}
                        nextStep={this.onChange}
                        onChange={this.handleFrequencyDictionary}/>
                    <RulesAndDictionaries
                        reset={reset}
                        step={current}
                        nextStep={this.onChange}/>
                    <IdyotaUploadFile
                        reset={reset}
                        step={current}
                        nextStep={this.onChange}
                        transformText={this.props.transformText}/>
                </Card>
                <ResetButton resetForm={this.resetForm}/>
                <UserPrompt auth={this.props.auth}/>
            </section>
        );
    }

    // noinspection JSUnusedLocalSymbols
    private handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.currentTarget;
        this.setState({[name]: value} as Pick<SideFormForm, keyof SideFormForm>);
    };
}

// noinspection JSUnusedLocalSymbols
const mapStateToProps = storeState => ({});

const mapDispatchToProps = {
    transformText
};

type StateProps = ReturnType<typeof mapStateToProps>;
// noinspection JSUnusedLocalSymbols
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SideForm);
