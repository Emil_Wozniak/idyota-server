import React from 'react';
import {ButtonGroup} from 'reactstrap';
import {Button} from 'antd';

type ResetButtonProps = {
    resetForm: Function
}

const ResetButton = ({resetForm}: ResetButtonProps) => (
    <>
        <ButtonGroup>
            <Button block onClick={() => resetForm()} type="default">
                Reset
            </Button>
        </ButtonGroup>
        <br/>
    </>
);

export default ResetButton;