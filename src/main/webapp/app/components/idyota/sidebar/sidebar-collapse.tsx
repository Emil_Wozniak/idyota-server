import React, {useState} from 'react';
import {Collapse} from 'antd';
import {activeKey, handleResetOpen, openCollapse} from "app/components/idyota/sidebar/collapse-functions";
import {getExpandIcon} from "app/components/icons/icons";

const {Panel} = Collapse;

type SidebarCollapseProps = {
    header: string;
    currentKey: number;
    nextStep: Function;
    step: number;
    reset: boolean;
    allowOpen?: boolean;
    center?: boolean;
    children: any;
}


const SidebarCollapse = ({header, currentKey, nextStep, step, reset, center, children, allowOpen}: SidebarCollapseProps) => {
    const key = `sidebar-step-${currentKey}`;
    const [label, setLabel] = useState("")
    handleResetOpen(reset, label, setLabel);
    return (
        <Collapse
            ghost
            activeKey={activeKey(reset, label)}
            expandIcon={getExpandIcon(step, currentKey)}
            onChange={allowOpen
                ? () => setLabel(key)
                : openCollapse(nextStep, step, setLabel, key)}>
            <Panel
                header={`  ${header}`}
                disabled={allowOpen ? false : step !== currentKey}
                key={key}>
                <div style={center ? {display: "flex", justifyContent: "center"} : {}}>
                    {children}
                </div>
            </Panel>
        </Collapse>
    );
};

export default SidebarCollapse;