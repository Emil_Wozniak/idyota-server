import React from 'react';
import {compress, dictionaries, prefix} from "app/components/idyota/sideform-labels";
import {Button} from 'antd';
import SidebarCollapse from "app/components/idyota/sidebar/sidebar-collapse";

type RulesAndDictionaries = {
    nextStep: Function;
    step: number;
    reset: boolean;
}

const RulesAndDictionaries = (props: RulesAndDictionaries) => (
    <SidebarCollapse header={dictionaries} currentKey={2} {...props}>
        <Button block disabled>
            {prefix}
        </Button>
        <Button block disabled>
            {compress}
        </Button>
    </SidebarCollapse>
);

export default RulesAndDictionaries;