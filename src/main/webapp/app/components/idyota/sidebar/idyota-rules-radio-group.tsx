import React, {useState} from 'react';
import {IdyotaRulesAll} from "app/entities/tokens/idyota-rules";
import {Button, Select} from 'antd';
import {all, rulesTrans} from "app/components/idyota/sideform-labels";
import SidebarCollapse from "app/components/idyota/sidebar/sidebar-collapse";

const {Option} = Select;

type RulesRadioGroupProps = {
    onChange: Function;
    nextStep: Function;
    step: number;
    reset: boolean;
}

const IdyotaRulesRadioGroup = (props: RulesRadioGroupProps) => {
    // noinspection JSUnusedLocalSymbols
    const [rules, setRules] = useState([] as string[])
    const handleSelectAllRules = () => {
        IdyotaRulesAll.forEach(rule => rules.push(rule));
        props.onChange(rules)
    }

    return (
        <SidebarCollapse header={rulesTrans} currentKey={0} {...props}>
            <Select
                onChange={(values) => props.onChange(values)}
                defaultValue={rules}
                mode="multiple"
                style={{width: '100%'}}
                placeholder="Please select">
                {IdyotaRulesAll.map((rule, i) => (
                    <Option
                        key={`idyota-rules-${rule}-${i}`}
                        type="ghost"
                        value={rule}>
                        {rule}
                    </Option>
                ))}
            </Select>
            <Button onClick={handleSelectAllRules} block>
                {all}
            </Button>
        </SidebarCollapse>
    );
};

export default IdyotaRulesRadioGroup;