import React from 'react';
import FileUploadProps from "app/components/idyota/file-upload-props";
import {uploadOutlined} from "app/components/icons/icons";
import {upload} from "app/components/idyota/sideform-labels";
import Upload from 'antd/lib/upload';
import {Button} from 'antd';
import SidebarCollapse from "app/components/idyota/sidebar/sidebar-collapse";

type IdyotaUploadFileProps = {
    transformText: Function;
    nextStep: Function;
    step: number;
    reset: boolean;
}

const IdyotaUploadFile = (props: IdyotaUploadFileProps) => (
    <SidebarCollapse header={upload} currentKey={3} {...props} allowOpen center>
        <Upload {...FileUploadProps(props.transformText)}>
            <Button block type="primary">
                {uploadOutlined}
                {upload}
            </Button>
        </Upload>
    </SidebarCollapse>
);

export default IdyotaUploadFile;