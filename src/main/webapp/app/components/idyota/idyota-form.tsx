import React from 'react';
import SideForm from "app/components/idyota/sidebar/side-form"
import ResultColumn from "app/components/idyota/result/result-column";
import {Col, Row} from 'antd';
import {connect} from 'react-redux';
import GridContainer from "app/components/layout/menus/grid/grid-container";
import {getTokens, getTokensByMode} from "app/entities/tokens/token.reducer";
import {IRootState} from "app/components/reducers";

interface DictionaryState extends StateProps, DispatchProps {
    account: any;
}

const IdyotaForm = (props: DictionaryState) => (
    <GridContainer>
        <Row gutter={[16, 16]}>
            <Col span={8}>
                <SideForm key="side-form" auth={props.auth}/>
            </Col>
            <Col span={16}>
                <ResultColumn getTokens={props.getTokensByMode} changes="" tokens={props.entities}/>
            </Col>
        </Row>
    </GridContainer>
);

const mapStateToProps = ({tokens, authentication}: IRootState) => ({
    entities: tokens.entities,
    auth: authentication
});

const mapDispatchToProps = {
    getTokens,
    getTokensByMode
};

type DispatchProps = typeof mapDispatchToProps;

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps, mapDispatchToProps)(IdyotaForm);
