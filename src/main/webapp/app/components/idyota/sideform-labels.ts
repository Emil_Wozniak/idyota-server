export const rulesTrans = 'Reguły';
export const all = 'Włącz wszystkie';
export const upload = 'Załącz plik';
export const frequency = 'Słownik frekwencyjny';
export const dictionaries = 'Słowniki i reguły';
export const prefix = 'Prefiks';
export const compress = 'Kompresja';
