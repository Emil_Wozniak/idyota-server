import { message } from 'antd';

const FileUploadProps = (transformText: Function) => ({
  accept: '.txt',
  width: '100%',
  name: 'file',
  action: file => transformText(file),
  onChange(options) {
    if (options.file.status !== 'uploading') {
      message.info('Uploading');
      // console.warn(options.file, options.fileList);
    }
    // options.customRequest =
    if (options.file.status === 'done') {
      // noinspection TypeScriptValidateJSTypes
      message.success(`${options.file.name} file uploaded successfully`);
    } else if (options.file.status === 'error') {
      options.fileList = [];
      // message.error(`${options.file.name} file upload failed.`);
    }
  },
});

export default FileUploadProps;
