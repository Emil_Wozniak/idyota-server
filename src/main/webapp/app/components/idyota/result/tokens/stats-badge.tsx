import React from 'react';
import {Badge} from 'antd';
import {PresetColorType, PresetColorTypes} from 'antd/lib/_util/colors';

const setColor = (amount: number) => {
    const index =
        (amount < 5) ? 12
            : (amount < 10) ? 11
            : (amount < 15) ? 10
                : (amount < 20) ? 9
                    : (amount < 25) ? 8
                        : (amount < 30) ? 7
                            : (amount < 35) ? 6
                                : (amount < 40) ? 5
                                    : (amount < 45) ? 4
                                        : (amount < 50) ? 3
                                            : (amount < 55) ? 2
                                                : 1
    return PresetColorTypes[index]
}

type StatsBadgeProps = {
    title: string;
    amount: number;
    status: PresetColorType;
}
const StatsBadge = ({title, amount, status = "red"}: StatsBadgeProps) => (
    <Badge count={amount} style={{background: setColor(amount)}}>
        {title}&nbsp;&nbsp;&nbsp;
    </Badge>
);

export default StatsBadge;