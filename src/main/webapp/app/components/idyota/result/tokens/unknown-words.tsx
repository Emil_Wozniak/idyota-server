import React from 'react';
import {IDescriptionsProps} from "app/components/idyota/result/description/descriptions";
import {Tag} from 'antd';
import TokenCollapse from "app/components/idyota/result/tokens/token-collapse";
import {InfoTypes} from "app/entities/tokens/InfoTypes";
import {Token} from "app/entities/tokens/tokens";

// noinspection SpellCheckingInspection
const getHeader = (tokens: Array<Token>) =>
    `Nieznane słowa: ${tokens.filter(token => token.info === InfoTypes.unknown).length}`;

const UnknownWords = ({tokens, ref}: IDescriptionsProps) => (
    <TokenCollapse ref={ref} header={getHeader(tokens)} key="unknownHeader" id={InfoTypes.unknown}>
        {tokens && tokens
            .filter(token => token.info === InfoTypes.unknown)
            .map((token, i) =>
                <Tag color="#ff0000" key={`token-unknown-${token.word}-${i}`}>
                    {token.word}
                </Tag>
            )}
    </TokenCollapse>
);

export default UnknownWords;