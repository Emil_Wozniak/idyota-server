import React from 'react';
import {redo} from "app/components/icons/icons";
import {Card, Collapse, Statistic} from 'antd';
import StatsBadge from "app/components/idyota/result/tokens/stats-badge";
import {Token} from "app/entities/tokens/tokens";
import {PresetColorType} from 'antd/lib/_util/colors';

const {Panel} = Collapse

type StatsPanelProps = {
    tokens: Array<Token>;
    title: string;
    color: string;
    status?: PresetColorType;
}

const getLength = (tokens: Array<Token>, title: string) => tokens.filter(token => token.info.includes(title)).length;

const StatsPanel = ({tokens, title, color, status}: StatsPanelProps) => {
    const header = <StatsBadge status={status} title={title} amount={tokens ? getLength(tokens, title) : 0}/>;
    return (
        <Collapse>
            <Panel key={`stats-${title}-all`} header={header}>

                {tokens && tokens
                    .filter(token => token.info.includes(title))
                    .map(token => token.info)
                    .map(info =>
                        <Card key={`stats-${title}-${info.split(title)[0]}`}>
                            <Statistic
                                title={info.split(`${title} `)[0].replace(">", "")}
                                value={info.split(`${title} `)[1].length}
                                valueStyle={{color}}
                                prefix={redo("green")}
                                suffix="replacements"/>
                        </Card>
                    )
                }
            </Panel>
        </Collapse>
    );
};

export default StatsPanel;