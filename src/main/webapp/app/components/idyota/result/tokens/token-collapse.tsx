import React, {ReactNode, RefObject} from 'react';
import {Collapse} from 'antd';

export interface TokenCollapseProps {
    header: string;
    key: string;
    id: string;
    ref: RefObject<any>;
    style?: React.CSSProperties;
    children?: ReactNode;
}

const {Panel} = Collapse;

const TokenCollapse = ({children, style, header, id, key}: TokenCollapseProps) => (
    <div id={id}>
        <Collapse>
            <Panel header={header} key={key}>
                <div style={style}>
                    {children}
                </div>
            </Panel>
        </Collapse>
    </div>
);

export default TokenCollapse;