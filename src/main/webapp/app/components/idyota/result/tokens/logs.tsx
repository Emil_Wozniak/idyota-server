import React from 'react';
import {IDescriptionsProps} from "app/components/idyota/result/description/descriptions";
import Text from "app/components/layout/text/text";
import TokenCollapse from "app/components/idyota/result/tokens/token-collapse";
import {logCollapse} from "app/components/styles/styles";

const Logs = ({tokens, ref}: IDescriptionsProps) => {
    // noinspection SpellCheckingInspection
    const header = "Logi";
    return (
        <TokenCollapse header={header} key="logHeader" id="log" style={logCollapse} ref={ref}>
            All Logs: {tokens && tokens.map(token => token.info).filter(info => info !== "" && info !== " ").length}
            {tokens && tokens
                .map(token => token.info)
                .filter(info => info !== "" && info !== " ")
                .map((info, i) =>
                    <div key={`token-unknown-${info}-${i}`}>
                        <Text text={info} size={14} color="#18F018"/>
                        <br/>
                    </div>
                )}
        </TokenCollapse>
    );
};

export default Logs;