import React from 'react';
import Text from "app/components/layout/text/text";
import TokenCollapse from "app/components/idyota/result/tokens/token-collapse";
import {IDescriptionsProps} from "app/components/idyota/result/description/descriptions";
import {Tag} from 'antd';

// noinspection SpellCheckingInspection
const header = "Poprawiony tekst";

const FixText = ({tokens, ref}: IDescriptionsProps) => (
    <TokenCollapse header={header} key="fixedHeader" id="fix" ref={ref}>
        {tokens && tokens
            .map((token, i) =>
                <Tag style={{margin: 4}} color="#009B77" key={token.replacement + "-" + i}>
                    <Text text={token.replacement} size={14}/>
                </Tag>
            )}
    </TokenCollapse>
)

export default FixText;