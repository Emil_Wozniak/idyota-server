import React from 'react';
import {IDescriptionsProps} from "app/components/idyota/result/description/descriptions";
import TokenCollapse from "app/components/idyota/result/tokens/token-collapse";
import StatsPanel from "app/components/idyota/result/tokens/stats-panel";
import {InfoTypes} from "app/entities/tokens/InfoTypes";

const Stats = ({tokens, ref}: IDescriptionsProps) => (
    <TokenCollapse header="Statystyki" key="statHeader" id="stats" ref={ref}>
        <StatsPanel tokens={tokens} title={InfoTypes.L1U} color={'#3f8600'}/>
        <StatsPanel tokens={tokens} title={InfoTypes.L2} color={'#3f8600'}/>
    </TokenCollapse>
);
export default Stats;