import React from 'react';
import DescriptionCard from "app/components/layout/menus/description-card";

// noinspection SpellCheckingInspection
const description = `
W górnym menu wybierz &quot;Wklej&quot;, potem wklej tekst do okienka, potem naciśnij
&quot;Popraw&quot;. Poniżej okienka wyświetli ci się poprawiony tekst. Przejrzyj w nim
słowa podświetlone na kolorowo i pogrubione - to są poprawki. Jeśli któraś
ci się nie podoba, kliknij na nią, a program przywróci poprzednią wersję
słowa. Wyrazy podświetlone na czerwono i pogrubione działają odwrotnie.
Po kliknięciu program zastępuje je własną sugestią. Jeśli jej nie chcesz,klikasz jeszcze raz, aby wrócić do stanu poprzedniego. Przejrzany tekst
zaznaczasz i kopiujesz. To wszystko.
`

// noinspection SpellCheckingInspection
const title = "Jak go używać?";

const HowItWorks = () => (
    <DescriptionCard id="usage" title={title} description={description}/>
);

export default HowItWorks;