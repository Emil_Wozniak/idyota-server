import React from 'react';
import DescriptionCard from "app/components/layout/menus/description-card";

// noinspection SpellCheckingInspection
const title = "Jak rozumieć dziwne oznaczenia w małych okienkach po prawej stronie?"
// noinspection SpellCheckingInspection
const description = `To nazwy używanych słowników i reguł. Możesz je kasować lub dodawać, co pozwala na konfigurację programu.`;

const SidebarInfo = () => <DescriptionCard id="sidebar_info" title={title} description={description}/>

export default SidebarInfo;