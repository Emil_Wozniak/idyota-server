import React, {RefObject, useRef} from "react";
import {Token} from "app/entities/tokens/tokens";
import AboutApp from "app/components/idyota/result/description/about-app";
import HowItWorks from "app/components/idyota/result/description/how-it-works";
import ModernText from "app/components/idyota/result/description/modern-text";
import OldestText from "app/components/idyota/result/description/oldest-text";
import SidebarInfo from "app/components/idyota/result/description/sidebar-info";
import RedoAction from "app/components/idyota/result/description/redo-action";
import SlowPerformance from "app/components/idyota/result/description/slow-performance";
import Stats from "app/components/idyota/result/tokens/stats";
import FixText from "app/components/idyota/result/tokens/fix-text";
import Logs from "app/components/idyota/result/tokens/logs";
import UnknownWords from "app/components/idyota/result/tokens/unknown-words";
import Antique from "app/components/layout/menus/grid/antique";

export interface IDescriptionsProps {
    tokens: Array<Token>;
    ref?: RefObject<any>;
}

export const Descriptions = ({tokens}: IDescriptionsProps) => (
    <article>
        {tokens.length > 0 && <FixText tokens={tokens} ref={useRef()}/>}
        {tokens.length > 0 && <Logs tokens={tokens} ref={useRef("log")}/>}
        {tokens.length > 0 && <UnknownWords tokens={tokens} ref={useRef("unknown")}/>}
        {tokens.length > 0 && <Stats tokens={tokens} ref={useRef("stats")}/>}
        <Antique id="help">
            <AboutApp/>
            <HowItWorks/>
            <ModernText/>
            <OldestText/>
            <SidebarInfo/>
            <RedoAction/>
            <SlowPerformance/>
        </Antique>
    </article>
);
