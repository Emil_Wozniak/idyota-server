import React from 'react';
import DescriptionCard from "app/components/layout/menus/description-card";

// noinspection SpellCheckingInspection
const title = "Czy Idyota może poprawiać stare wiersze?"
// noinspection SpellCheckingInspection
const description = `Tak, ale prawdopodobnie konieczne będzie cofnięcie poprawek psujących rytm lub rymy.`;

const OldestText = () => <DescriptionCard id="oldest" title={title} description={description}/>

export default OldestText;