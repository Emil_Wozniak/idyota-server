import React from 'react';
import DescriptionCard from "app/components/layout/menus/description-card";

// noinspection SpellCheckingInspection
const description = `  
Tak, ale nie ma to sensu. Poprawiając współczesny tekst dostaniesz głównie
informację o słowach, których program nie zna, oraz sugestie zmian, jeśli
słowa są podobne do tych, które program zna.
`
// noinspection SpellCheckingInspection
const title = "Czy Idyota może poprawiać współczesne teksty?"

const ModernText = () => <DescriptionCard id="modern" title={title} description={description}/>;

export default ModernText;