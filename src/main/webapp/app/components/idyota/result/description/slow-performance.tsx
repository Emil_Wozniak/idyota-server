import React from 'react';
import DescriptionCard from "app/components/layout/menus/description-card";

// noinspection SpellCheckingInspection
const title = "Program działa za wolno"
// noinspection SpellCheckingInspection
const description = "Wyłącz słownik frekwencyjny.";

const SlowPerformance = () => <DescriptionCard id="performance" title={title} description={description}/>

export default SlowPerformance;