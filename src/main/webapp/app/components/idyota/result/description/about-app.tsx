import React from 'react';
import DescriptionCard from "app/components/layout/menus/description-card";

// noinspection SpellCheckingInspection
const description = "Idyota to program do uwspółcześniania ortografii tekstów wydanych drukiem przed reformą ortografii w 1936 roku."

const AboutApp = () => (
    <DescriptionCard id="about" title="Co to jest Idyota?" description={description}/>
);

export default AboutApp;