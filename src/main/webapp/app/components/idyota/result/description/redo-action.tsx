import React from 'react';
import DescriptionCard from "app/components/layout/menus/description-card";

// noinspection SpellCheckingInspection
const title = "Skasowałem jedną z tych nazw, a jej nie pamiętam! Co robić?"
// noinspection SpellCheckingInspection
const description = `Można je przywrócić klikając na &quot;Wszystkie słowniki&quot; bądź &quot;Wszytkie reguły&quot;`;

const RedoAction = () => <DescriptionCard id="redo" title={title} description={description}/>

export default RedoAction;