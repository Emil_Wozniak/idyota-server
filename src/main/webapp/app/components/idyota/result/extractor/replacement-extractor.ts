import { InfoTypes } from 'app/entities/tokens/InfoTypes';

const isReplace = (info: string) => info.split(']  ').length > 1;

export const extractReplacements = (info: string) => {
  if (info.length > 0 && info !== InfoTypes.unknown && info !== InfoTypes.nothing && isReplace(info)) {
    switch (info.split(']  ')[1].split(' ')[0]) {
      case InfoTypes.L1U:
        return info.split(InfoTypes.L1U)[1].split(' ');
      case InfoTypes.L2:
        return info.split(InfoTypes.L2)[1].split(' ');
      default:
        return [];
    }
  } else return [];
};
