// noinspection SpellCheckingInspection
const areaDefault = 'Tekst oryginalny';

export const ResultColumnProps = lines => ({
  allowClear: true,
  defaultValue: areaDefault,
  className: 'Antique',
  autoSize: true,
  name: 'original text',
  value: lines,
  id: 'original text',
});
