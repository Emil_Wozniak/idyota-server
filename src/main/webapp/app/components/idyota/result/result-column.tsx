import React, {Component} from 'react';
import {Descriptions} from "app/components/idyota/result/description/descriptions";
import {Token} from "app/entities/tokens/tokens";
import {IRootState} from "app/components/reducers";
import {connect} from 'react-redux';
import {Input} from 'antd';
import TokenSelectors from "app/components/idyota/result/selector/token-selectors";
import {ResultColumnProps} from './result-column-props';

interface ResultColumnProps extends DispatchProps, StateProps {
    changes: string;
    getTokens: Function;
    tokens: Array<Token>;
}

type ResultColumnState = {
    text: string;
    changes: string;
}

// noinspection JSUnusedLocalSymbols
class ResultColumn extends Component<ResultColumnProps, ResultColumnState> {
    constructor(props) {
        super(props);
        this.state = {text: "", changes: ""}
    }

    private static extract(array: Array<string>, newLine = true) {
        return array.join(newLine ? "\n" : " ")
    }

    shouldComponentUpdate(nextProps, nextState, _) {
        const {lines} = this.props;
        if (lines !== nextProps.lines) {
            return true;
        }
        return this.props.tokens !== nextProps.tokens;
    }

    render() {
        const {TextArea} = Input;
        const {lines, tokens} = this.props;
        return (
            <section id="result column" aria-label="result-column">
                <TextArea {...ResultColumnProps(ResultColumn.extract(lines))}/>
                <TokenSelectors tokens={tokens} originalText={lines}/>
                <Descriptions tokens={tokens}/>
            </section>
        );
    }

    private handleChange = (event: React.FormEvent<HTMLTextAreaElement>) => {
        const {name, value} = event.currentTarget;
        this.setState({[name]: value} as Pick<ResultColumnState, keyof ResultColumnState>);
    };
}

const mapStateToProps = ({tokens}: IRootState) => ({
    lines: tokens.lines
});


const mapDispatchToProps = {};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ResultColumn);
