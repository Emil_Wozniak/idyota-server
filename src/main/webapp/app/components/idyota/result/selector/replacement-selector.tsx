import React from 'react';
import {Select} from 'antd';

const {Option} = Select

type ReplacementSelectorProps = {
    word: string;
    words: string[];
}

const ReplacementSelector = ({words, word}: ReplacementSelectorProps) => words.length > 0
    ? (
        <Select defaultValue={word} size="small">
            {words.map((replace, i) => (
                <Option
                    key={`idyota-rules-${replace}-${i}`}
                    type="ghost"
                    value={replace}>
                    {replace}
                </Option>
            ))}
        </Select>
    )
    : <> </>;

export default ReplacementSelector;