import React from 'react';
import {Badge} from 'antd';

// noinspection SpellCheckingInspection
const editor = "Edytor";

type SelectorHeaderProps = {
    size: number
}

const SelectorHeader = ({size}: SelectorHeaderProps) => size > 0
    ? <Badge status="warning">{editor}</Badge>
    : <>{editor}</>;

export default SelectorHeader;