import React from 'react';
import {Badge} from 'antd';
import {InfoTypes} from "app/entities/tokens/InfoTypes";
import {extractReplacements} from "app/components/idyota/result/extractor/replacement-extractor";
import ProcessBadge from "app/components/idyota/result/selector/process-badge";
import WarningBadge from "app/components/layout/menus/grid/warning-badge";

interface TokenSelectorProps {
    word: string;
    replacement?: string
    info: string;
}

type EditElementProps = {
    info: string;
    word: string;
}

const EditorElement = ({word, info}: EditElementProps) => {
    switch (word) {
        case InfoTypes.NEW_LINE:
            return <br/>
        case " ":
            return <>{word}</>
        default:
            switch (info) {
                case InfoTypes.unknown:
                    return <WarningBadge label={word} dot/>
                case InfoTypes.nothing:
                    return <Badge status="success" dot>{word}</Badge>
                default:
                    return info.includes(">")
                        ? <ProcessBadge word={word} words={extractReplacements(info)} dot/>
                        : <>{word}&nbsp;</>
            }
    }
};

const TokenSelector = ({word, info}: TokenSelectorProps) => (
    <>
        <EditorElement info={info} word={word} />
        &nbsp;
    </>
);

export default TokenSelector;