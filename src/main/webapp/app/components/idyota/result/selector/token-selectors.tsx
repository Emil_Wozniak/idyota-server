import React from 'react';
import {Button} from 'antd';
import TokenSelector from "app/components/idyota/result/selector/token-selector";
import SelectorPanel from "app/components/idyota/result/selector/selector-panel";
import SelectorHeader from "app/components/idyota/result/selector/selector-header";
import {showLegend} from "app/components/idyota/result/selector/legend";
import {matchInfo, wordsOf} from "app/components/idyota/result/selector/selector-functions";
import {Token} from "app/entities/tokens/tokens";
import Antique from "app/components/layout/menus/grid/antique";

// noinspection SpellCheckingInspection
const legend = "Pokaż Legendę";

interface TextSelectorProps {
    tokens: Array<Token>;
    originalText: Array<string>;
}

const TokenSelectors = ({tokens, originalText}: TextSelectorProps) => {
    const header = <SelectorHeader size={tokens.length}/>;
    return (
        <SelectorPanel key={`selector-text`} header={header} onChange={showLegend()}>
            <Button onClick={showLegend()}>{legend}</Button>
            <br/>
            <Antique style={{paddingTop: "2rem"}} id="selector-words">
                {wordsOf(originalText)
                    .map((word, i) =>
                        <TokenSelector
                            word={word}
                            info={matchInfo(tokens, word)}
                            key={`selector-${word}-${i}`}/>
                    )}
            </Antique>
        </SelectorPanel>
    );
};

export default TokenSelectors;