import React from 'react';
import {Collapse} from 'antd';

const {Panel} = Collapse;

type SelectorPanelProps = {
    header: string | JSX.Element;
    key?: string;
    onChange?: any
    children?: JSX.Element | JSX.Element[];
    id?: string;
}

const SelectorPanel = ({header, key, onChange, children}: SelectorPanelProps) => (
    <Collapse onChange={onChange} aria-label={`selector-panel-${key}`}>
        <Panel header={header} key={key}>
            {children}
        </Panel>
    </Collapse>
);

export default SelectorPanel;