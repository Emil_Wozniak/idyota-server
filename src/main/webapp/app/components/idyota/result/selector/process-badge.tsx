import React from 'react';
import ReplacementSelector from "app/components/idyota/result/selector/replacement-selector";
import {Badge} from 'antd';

type ProcessBadgeProps = {
    word: string;
    words: string[];
    dot?: boolean;
}

const ProcessBadge = ({word, words, dot = false}: ProcessBadgeProps) => (
    <Badge status="processing" dot={dot}>
        <ReplacementSelector word={word} words={words}/>
    </Badge>
);

export default ProcessBadge;