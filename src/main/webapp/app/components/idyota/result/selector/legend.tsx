import {Badge, notification, Tag} from 'antd';
import React from 'react';

export const showLegend = () => () => {
    // noinspection SpellCheckingInspection
    notification.open({
        message: 'Legenda',
        description:
            <>
                <Tag>
                    <Badge status="warning">Nieznane</Badge>
                </Tag>
                <Tag>
                    <Badge status="success">Brak akcji</Badge>
                </Tag>
                <Tag>
                    <Badge status="processing">Możliwe zmiany</Badge>
                </Tag>
            </>,
    });
};