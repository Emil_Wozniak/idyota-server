import { Token } from 'app/entities/tokens/tokens';
import { InfoTypes } from 'app/entities/tokens/InfoTypes';

export const matchInfo = (tokens: Array<Token>, word: string) => {
  const matchToken = tokens.filter(token => token.word === word);
  return matchToken.length > 0 ? matchToken[0].info : '';
};

export const wordsOf = (originals: Array<string>) =>
  originals && originals.length > 0
    ? originals
        .map(line => (line.includes(' ') ? `${line} ${InfoTypes.NEW_LINE} ` : ` ${InfoTypes.NEW_LINE} `))
        .join('')
        .split(' ')
    : [];
