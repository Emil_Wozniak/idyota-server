import React from 'react';

interface MediumTextProps {
    text: string;
    color?: string;
    size?: number | string
}

const Text = ({text, size = 12, color = "black"}: MediumTextProps) => (
    <span style={{fontSize: size, color}} className="Antique">
        {text}
    </span>
);

export default Text;