import React from 'react';
import SiderItem from "app/components/layout/sider/sider-item";

// noinspection SpellCheckingInspection
const IdyotaElements = () => (
    <>
        <SiderItem name="Wklej" to="#paste">
            Wklej
        </SiderItem>
        <SiderItem name="Popraw" to="#fix">
            Popraw
        </SiderItem>
        <SiderItem name="Statystyki" to="#stats">
            Statystyki
        </SiderItem>
        <SiderItem name="Log" to="#log">
            Logi
        </SiderItem>
        <SiderItem name="Nieznane" to="#unknown">
            Nieznane
        </SiderItem>
        <SiderItem name="Alpha vr." to="#dev">
            Alpha
        </SiderItem>
        <SiderItem name="Pomoc" to="#help">
            Pomoc
        </SiderItem>
        <SiderItem name="Co to jest Idyota" to="#about">
            Co to jest Idyota
        </SiderItem>
        <SiderItem name="how_to_use" to="#usage">
            Jak go używać
        </SiderItem>
        <SiderItem name="modern" to="#modern">
            Współczesne teksty
        </SiderItem>
        <SiderItem name="oldest" to="#oldest">
            Starsze teksty
        </SiderItem>
        <SiderItem name="sidebar_info" to="#sidebar_info">
            Formularz z prawej strony
        </SiderItem>
        <SiderItem name="redo" to="#redo">
            Cofanie zmian
        </SiderItem>
        <SiderItem name="performance" to="#performance">
            Wolne działanie
        </SiderItem>
    </>
)

export default IdyotaElements;
