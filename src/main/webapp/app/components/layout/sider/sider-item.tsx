import "app/app.scss"
import React from 'react';
import Menu from 'antd/lib/menu';
import {Button} from 'antd';
import {SUBMENU} from "app/components/styles/styles";

interface SiderItemProps {
    name: string;
    icon?: any;
    to?: string;
    children: any;
}

const focusOn = (to: string) => () => window.location.hash = to;

const SiderItem = ({name, children, to, icon}: SiderItemProps) => (
    <Menu.Item className="slider-submenu" key={`${name}`} onItemHover={() => null}>
        <Button aria-label="brand icon" key={name + "-btn"} style={SUBMENU} onClick={focusOn(to)}>
            {to && icon && <div style={{marginRight: "1rem"}}>{icon}</div>}
            {to && children}
            {!to && children}
        </Button>
    </Menu.Item>
);

export default SiderItem;
