import "app/app.scss"
import React, {useState} from 'react';
import SubMenu from 'antd/lib/menu/SubMenu';
import {Layout, Menu} from 'antd';
import {Storage, translate} from 'react-jhipster';
import {SECONDARY_COLOR} from "app/components/styles/styles";
import Brand from "app/components/layout/brand/brand";
import LocaleMenu from "app/components/layout/menus/locale";
import EntitiesMenu from "app/components/layout/menus/entities";
import AccountMenu from "app/components/layout/menus/account";
import AdminMenu from "app/components/layout/menus/admin";
import {SiderTheme} from 'antd/lib/layout/Sider';
import {BrightnessHigh} from "@material-ui/icons"
import IdyotaElements from "app/components/layout/sider/idyota-elements";
import ErrorBoundary from "app/components/error/error-boundary";
import {desktopOutlined, list, toolFilled, translation, user} from "app/components/icons/icons";

// noinspection SpellCheckingInspection
const {Sider} = Layout;

export interface ISliderProps {
    isAuth: boolean;
    isAdmin: boolean;
    isSwagger: boolean;
    locale: string;
    changeLocale: Function;
}

const AppSider = (props: ISliderProps) => {
    const themes: Array<SiderTheme> = ["light", "dark"]
    const [collapsed, setCollapse] = useState(true);
    const [currentTheme, setTheme] = useState(themes[0])
    const onCollapse = (event) => {
        setCollapse(event)
    };
    const localeChange = (event) => {
        const language = event.target.innerText;
        const langKey = language === "English" ? "en" : "pl";
        Storage.session.set('locale', langKey);
        props.changeLocale(langKey);
    };

    const changeTheme = () => setTheme(themes[currentTheme === "light" ? 1 : 0])
    const menu = {backgroundColor: SECONDARY_COLOR};

    const entities = translate("global.menu.entities.main");
    const title = translate('global.menu.account.main');
    const theme = translate("global.menu.theme");
    const admin = translate("global.menu.admin.main");
    const elements = translate("global.menu.elements");

    return (
        <ErrorBoundary>
            <Sider
                reverseArrow
                collapsible
                collapsed={collapsed}
                onCollapse={onCollapse}
                theme={currentTheme}
                className="slider-color">
                <Brand resize={collapsed}/>
                <Menu defaultSelectedKeys={['1']} mode="inline" style={menu}>
                    <SubMenu className="slider-menu" icon={translation} title={props.locale}>
                        <LocaleMenu onClick={localeChange}/>
                    </SubMenu>
                    <SubMenu className="slider-menu" icon={list} title={elements}>
                        <IdyotaElements/>
                    </SubMenu>
                    <SubMenu className="slider-menu" icon={user} title={title}>
                        <AccountMenu isAuthenticated={props.isAuth}/>
                    </SubMenu>
                    {props.isAuth &&
                    <SubMenu className="slider-menu" icon={desktopOutlined} title={entities}>
                        <EntitiesMenu/>
                    </SubMenu>
                    }
                    {props.isAuth && props.isAdmin &&
                    <SubMenu className="slider-menu" icon={toolFilled} title={admin}>
                        <AdminMenu showSwagger={props.isSwagger}/>
                    </SubMenu>
                    }
                    <Menu.Item onClick={() => changeTheme()} className="slider-menu" title={theme}>
                        <BrightnessHigh className="slider-button"/>
                    </Menu.Item>
                </Menu>
            </Sider>
        </ErrorBoundary>
    );
};

export default AppSider;