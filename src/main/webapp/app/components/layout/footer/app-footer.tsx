import './footer.scss';
import React from 'react';
import {Translate} from 'react-jhipster';
import {Layout} from "antd";

const {Footer} = Layout;

const AppFooter = () => (
    <Footer>
        <Translate contentKey="footer">Your footer</Translate>
    </Footer>
);

export default AppFooter;
