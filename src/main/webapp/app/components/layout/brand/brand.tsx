import React from 'react';
import Logo from "-!svg-react-loader!app/assets/Anonymous_emblem.svg";

const Brand = ({resize}) => {
    const size = resize ? "5rem" : "12.5rem"
    return (
        <Logo className="brand-color" style={{height: size, width: size}}/>);
};

export default Brand;

