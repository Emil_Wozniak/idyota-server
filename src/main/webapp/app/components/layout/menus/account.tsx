import React from 'react';
import {Translate} from 'react-jhipster';
import SiderItem from "app/components/layout/sider/sider-item";

const AccountMenuAuthenticated = () => (
    <>
        <SiderItem name="wrench" to="/account/settings">
            <Translate contentKey="global.menu.account.settings">Settings</Translate>
        </SiderItem>
        <SiderItem name="lock" to="/account/password">
            <Translate contentKey="global.menu.account.password">Password</Translate>
        </SiderItem>
        <SiderItem name="sign-out-alt" to="/logout">
            <Translate contentKey="global.menu.account.logout">Sign out</Translate>
        </SiderItem>
    </>
);

const AccountMenuItems = () => (
    <>
        <SiderItem name="login" to="/login">
            <Translate contentKey="global.menu.account.login">Sign in</Translate>
        </SiderItem>
        <SiderItem name="login" to="/account/register">
            <Translate contentKey="global.menu.account.register">Register</Translate>
        </SiderItem>
    </>
);

export const AccountMenu = ({isAuthenticated = false}) =>
    isAuthenticated
        ? <AccountMenuAuthenticated/>
        : <AccountMenuItems/>


export default AccountMenu;
