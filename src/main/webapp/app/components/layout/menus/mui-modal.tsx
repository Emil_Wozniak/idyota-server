import React, {useState} from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import {Button} from '@material-ui/core';

function getModalStyle() {
  const top = 50;
  const left = 50;
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      width: "80vw",
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
  }),
);

type MuiModalProps = {
  id: string;
  fixed?: boolean;
  isOpen: boolean;
  toggle: Function;
  autoFocus?: boolean
  children: any;
}

const MuiModal = ({id, children, fixed = false, isOpen = true, toggle, autoFocus= true}: MuiModalProps) => {
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const [open, setOpen] = useState(isOpen);

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <div id={id}>
      {fixed
        ? null
        : (
          <Button type="button" onClick={handleOpen}>
            Open Modal
          </Button>
        )
      }
      <Modal
        disableAutoFocus={autoFocus}
        open={open}
        onClose={() => toggle()}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div style={modalStyle} className={classes.paper}>
          {children}
        </div>
      </Modal>
    </div>
  );
}

export default MuiModal;
