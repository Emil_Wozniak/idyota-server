import React from 'react';
import {Translate} from 'react-jhipster';
import SiderItem from '../sider/sider-item';

const EntitiesMenu = () => (
    <>
        <SiderItem to="/token" name="token">
            <Translate contentKey="global.menu.entities.token"/>
        </SiderItem>
    </>
);

export default EntitiesMenu;