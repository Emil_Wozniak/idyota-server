import React from 'react';
import {StyledMenuItem} from "app/components/layout/menus/mui-nam-menu";
import { ListItemIcon, Link, ListItemText } from '@material-ui/core';

/**
 *
 * @param icon https://material-ui.com/components/material-icons/
 * @param href http address
 * @param label name to display
 */
const MuiNavItem = ({icon, href, label}) => (
    <StyledMenuItem>
      <ListItemIcon>
        {icon}
      </ListItemIcon>
      <Link href={href}>
        <ListItemText primary={label}/>
      </Link>
    </StyledMenuItem>
  );

export default MuiNavItem;
