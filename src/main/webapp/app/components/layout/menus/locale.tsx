import React from 'react';
import "app/app.scss"
import {languages, locales} from 'app/config/translation';
import {Button, Menu} from 'antd';
import {SUBMENU} from "app/components/styles/styles";

interface LocaleProps {
    onClick: Function
}

const LocaleMenu = ({onClick}: LocaleProps) => (
    <>
        {locales.map((locale, i) => (
            <Menu.Item className="slider-submenu"
                       key={`${locale}-${i}`}
                       onClick={() => null}
                       onItemHover={() => null}>
                <Button aria-label="brand icon"
                        key={i}
                        style={SUBMENU}
                        onClick={(e) => onClick(e)}>
                    {languages[locale].name}
                </Button>
            </Menu.Item>
        ))
        }
    </>
);

export default LocaleMenu;

