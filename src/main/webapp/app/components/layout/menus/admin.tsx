import React from 'react';
import {Translate} from 'react-jhipster';
import SliderItem from "app/components/layout/sider/sider-item";

const AdminSliderItems = () => (
    <>
        <SliderItem name="user" to="/admin/user-management">
            <Translate contentKey="global.menu.admin.userManagement">User management</Translate>
        </SliderItem>
        <SliderItem name="tachometer" to="/admin/metrics">
            <Translate contentKey="global.menu.admin.metrics">Metrics</Translate>
        </SliderItem>
        <SliderItem name="heart" to="/admin/health">
            <Translate contentKey="global.menu.admin.health">Health</Translate>
        </SliderItem>
        <SliderItem name="list" to="/admin/configuration">
            <Translate contentKey="global.menu.admin.configuration">Configuration</Translate>
        </SliderItem>
        <SliderItem name="bell" to="/admin/audits">
            <Translate contentKey="global.menu.admin.audits">Audits</Translate>
        </SliderItem>
        <SliderItem name="tasks" to="/admin/logs">
            <Translate contentKey="global.menu.admin.logs">Logs</Translate>
        </SliderItem>
    </>
);

const SwaggerItem = () => (
    <SliderItem name="book" to="/admin/docs">
        <Translate contentKey="global.menu.admin.apidocs">API</Translate>
    </SliderItem>
);

export const AdminMenu = ({showSwagger}) => (
    <>
        <AdminSliderItems/>
        {showSwagger && <SwaggerItem/>}
    </>
);

export default AdminMenu;
