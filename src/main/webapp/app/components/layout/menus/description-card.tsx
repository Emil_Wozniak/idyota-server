import React from 'react';
import { Card } from 'antd';
import Title from 'antd/lib/typography/Title';

const DescriptionCard = ({title, description, id}) => (
    <div ref={React.createRef()} id={id}>
        <Card id="">
            <Title level={5}>
                {title}
            </Title>
            <div>{description}</div>
        </Card>
    </div>

    );

export default DescriptionCard;