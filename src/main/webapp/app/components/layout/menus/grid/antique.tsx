import React from 'react';

type AntiqueProps = {
    style?: React.CSSProperties;
    id?: string;
    children: any;
}
const Antique = ({style = {}, children, id = ""}: AntiqueProps) => (
    <div className="Antique" style={style} id={id}>
        {children}
    </div>
);

export default Antique;