export type BadgeProps = {
  label: string;
  dot?: boolean;
};
