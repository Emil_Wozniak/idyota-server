import React from 'react';
import {Badge} from 'antd';
import {BadgeProps} from "app/components/layout/menus/grid/badge-props";

const WarningBadge = ({label, dot = false}: BadgeProps) => (
    <Badge status="warning" dot={dot}>{label}</Badge>
);

export default WarningBadge;