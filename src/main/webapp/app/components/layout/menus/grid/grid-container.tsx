import React from 'react';

type MainContainerProps = {
    children: any;
}
const GridContainer = ({children}: MainContainerProps) => (
    <div className="container top-2">
        {children}
    </div>
);

export default GridContainer;