import './header.scss';
import React from 'react';
import LoadingBar from 'react-redux-loading-bar';
import {Layout} from 'antd';
import DevRibbon from "app/components/layout/header/dev-ribbon";
import ErrorBoundary from "app/components/error/error-boundary";
import {Link} from 'react-router-dom';

const {Header} = Layout;

export interface IHeaderProps {
    ribbonEnv: string;
    isInProduction: boolean;
}

const AppHeader = ({ribbonEnv, isInProduction}: IHeaderProps) => (
    <ErrorBoundary>
        <Header className="header-color Antique">
            <LoadingBar className="loading-bar"/>
            <Link to="/">
                <h1 style={{color: "white"}}>Idyota</h1>
            </Link>
            <DevRibbon isInProduction={isInProduction} ribbonEnv={ribbonEnv}/>
        </Header>
    </ErrorBoundary>
);

export default AppHeader;
