import React from 'react';
import {Translate} from 'react-jhipster';

const DevRibbon = ({isInProduction, ribbonEnv}) =>
    isInProduction === false && (
        <div className="ribbon dev">
            <a href="">
                <Translate contentKey={`global.ribbon.${ribbonEnv}`}/>
            </a>
        </div>
    )

export default DevRibbon;
