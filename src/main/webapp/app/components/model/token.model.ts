export interface IToken {
  id?: number;
  word?: string;
  replacement?: string;
  info?: string;
  identifier?: string;
}

export const defaultValue: Readonly<IToken> = {};
