import React, {ChangeEvent, Component} from 'react';
import {translate} from 'react-jhipster';
import {Checkbox, FormControlLabel, TextField} from '@material-ui/core';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import {Alert, Col, Modal, Row} from 'antd';
import {Link} from 'react-router-dom';

export interface ILoginModalProps {
    showModal: boolean;
    loginError: boolean;
    handleLogin: Function;
    handleClose: Function;
}

type LoginModalState = {
    password: string;
    username: string;
}

class LoginModal extends Component<ILoginModalProps, LoginModalState> {
    constructor(props) {
        super(props)
        this.state = {username: "", password: ""}
    }

    handleSubmit = () => {
        const {handleLogin} = this.props;
        handleLogin(this.state.username, this.state.password, false);
    };

    handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.currentTarget;
        this.setState({[name]: value} as Pick<LoginModalState, keyof LoginModalState>);
    };

    render() {
        const {loginError, handleClose} = this.props;
        const registration = translate("global.messages.info.register.link");
        const register = <Link to={"/account/register"}>{registration}</Link>;
        const translation = translate("login.password.forgot");
        const changePassword = <Link type="button" to={"/account/reset/request"}>{translation}</Link>;
        const control = <Checkbox value="allowExtraEmails" color="primary"/>;
        const authError = translate("login.messages.error.authentication");
        return (
            <Modal
                title={translate("login.title")}
                visible={this.props.showModal}
                onCancel={() => handleClose()}
                onOk={() => this.handleSubmit()}
                okButtonProps={{htmlType: "submit", form: "login-form"}}
                okText={translate("login.form.button")}
                cancelText={translate("entity.action.cancel")}>
                <form onSubmit={this.handleSubmit} noValidate autoComplete="off">
                    <Row>
                        <Col span={24}>
                            {loginError && <Alert showIcon type="error" message={authError}/>}
                        </Col>
                        <Col span={24}>
                            <TextField
                                style={{width: "45%"}}
                                id="outlined-login-input"
                                name="username"
                                type="text"
                                variant="outlined"
                                label={translate('global.form.username.label')}
                                placeholder={translate('global.form.username.placeholder')}
                                onChange={this.handleChange}
                                autoFocus
                                required
                                error={this.state.username === ""}
                                helperText={this.state.username === "" ? "Incorrect entry." : ""}
                            />
                            <TextField
                                style={{width: "45%"}}
                                id="outlined-password-input"
                                name="password"
                                type="password"
                                autoComplete="current-password"
                                variant="outlined"
                                label={translate('login.form.password')}
                                placeholder={translate('login.form.password.placeholder')}
                                required
                                onChange={this.handleChange}
                                error={this.state.username === ""}
                                helperText={this.state.username === "" ? "Incorrect entry." : ""}
                            />
                        </Col>
                        <Col>
                            <FormControlLabel control={control} label="Remember me"/>
                        </Col>
                    </Row>
                    <div className="mt-1">&nbsp;</div>
                    <Alert message={changePassword} showIcon type="warning"/>
                    <Alert message={register} type="warning" showIcon icon={<PersonAddIcon/>}/>
                </form>
            </Modal>
        );
    }
}

export default LoginModal;
