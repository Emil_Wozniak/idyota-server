import { translate } from 'react-jhipster';

export const firstNameRules = {
  required: { value: true, errorMessage: translate('settings.messages.validate.firstname.required') },
  minLength: { value: 1, errorMessage: translate('settings.messages.validate.firstname.minlength') },
  maxLength: { value: 50, errorMessage: translate('settings.messages.validate.firstname.maxlength') },
};
export const lastNameRules = {
  required: { value: true, errorMessage: translate('settings.messages.validate.lastname.required') },
  minLength: { value: 1, errorMessage: translate('settings.messages.validate.lastname.minlength') },
  maxLength: { value: 50, errorMessage: translate('settings.messages.validate.lastname.maxlength') },
};
export const emailRules = {
  required: { value: true, errorMessage: translate('global.messages.validate.email.required') },
  minLength: { value: 5, errorMessage: translate('global.messages.validate.email.minlength') },
  maxLength: { value: 254, errorMessage: translate('global.messages.validate.email.maxlength') },
};
