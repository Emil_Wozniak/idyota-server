import React, {ChangeEvent} from 'react';
import {connect} from 'react-redux';
import {Translate, translate} from 'react-jhipster';
import {IRootState} from 'app/components/reducers';
import {getSession} from 'app/components/reducers/authentication';
import {reset, saveAccountSettings} from './settings.reducer';
import {Button, Col, Input, Row} from 'antd';
import Title from 'antd/lib/typography/Title';
import {isFieldOk} from "app/components/icons/icon-functions";
// import {emailTrans, firstNameTrans, lastNameTrans} from "app/components/translations/translations";

export interface IUserSettingsProps extends StateProps, DispatchProps {
}

interface SettingsState {
    firstName: string;
    lastName: string;
    email: string;
    langKey: string;
}

export class SettingsPage extends React.Component<IUserSettingsProps, SettingsState> {
    constructor(props) {
        super(props);
        this.state = {firstName: "", lastName: "", email: "", langKey: "pl"}
    }

    componentDidMount() {
        this.props.getSession();
        const {firstName, lastName, email, langKey} = this.props.account
        this.setState({firstName, lastName, email, langKey})
        return () => {
            this.props.reset();
        };
    }

    handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.currentTarget;
        this.setState({[name]: value} as Pick<SettingsState, keyof SettingsState>);
    };

    handleValidSubmit = (event) => {
        const account = {
            ...this.props.account,
            ...this.state
        };

        this.props.saveAccountSettings(account);
        event.persist();
    };

    render() {
        const firstNameTrans = translate('settings.form.firstname.placeholder');
        const lastNameTrans = translate('settings.form.lastname.placeholder');
        const emailTrans = 'email';
        const disabled = this.state.firstName === "" || this.state.lastName === "" || this.state.email === "";
        return (
            <div className="container top-2">
                <Row gutter={[48, 48]} align="middle" justify="center">
                    <Col span={12}>
                        <Title level={2} id="settings-title">
                            <Translate contentKey="settings.title" interpolate={{username: this.props.account.login}}>
                                User settings for {this.props.account.login}
                            </Translate>
                        </Title>
                        <form onSubmit={this.handleValidSubmit} noValidate autoComplete="off" id="settings-form">
                            <Input
                                prefix={isFieldOk(this.state.firstName)}
                                suffix={firstNameTrans}
                                value={this.state.firstName}
                                name="firstName"
                                placeholder={firstNameTrans}
                                onChange={this.handleChange}
                                autoFocus
                                required
                            />
                            <Input
                                prefix={isFieldOk(this.state.lastName)}
                                suffix={lastNameTrans}
                                value={this.state.lastName}
                                name="lastName"
                                placeholder={lastNameTrans}
                                onChange={this.handleChange}
                                autoFocus
                                required
                            />
                            <Input
                                prefix={isFieldOk(this.state.email)}
                                suffix={emailTrans}
                                value={this.state.email}
                                name="email"
                                placeholder={emailTrans}
                                onChange={this.handleChange}
                                autoFocus
                                required
                            />
                            <Button color="primary" type="primary" htmlType="submit" block disabled={disabled}>
                                {translate("settings.form.button")}
                            </Button>
                        </form>
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = ({authentication}: IRootState) => ({
    account: authentication.account,
    isAuthenticated: authentication.isAuthenticated,
});

const mapDispatchToProps = {getSession, saveAccountSettings, reset};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);
