import { translate } from 'react-jhipster';

// noinspection SpellCheckingInspection
export const confirmPass = {
  required: {
    value: true,
    errorMessage: translate('global.messages.validate.confirmpassword.required'),
  },
  minLength: {
    value: 4,
    errorMessage: translate('global.messages.validate.confirmpassword.minlength'),
  },
  maxLength: {
    value: 50,
    errorMessage: translate('global.messages.validate.confirmpassword.maxlength'),
  },
  match: {
    value: 'newPassword',
    errorMessage: translate('global.messages.error.dontmatch'),
  },
};
export const newPassword = {
  required: { value: true, errorMessage: translate('global.messages.validate.newpassword.required') },
  minLength: { value: 4, errorMessage: translate('global.messages.validate.newpassword.minlength') },
  maxLength: { value: 50, errorMessage: translate('global.messages.validate.newpassword.maxlength') },
};
export const current = {
  required: { value: true, errorMessage: translate('global.messages.validate.newpassword.required') },
};
