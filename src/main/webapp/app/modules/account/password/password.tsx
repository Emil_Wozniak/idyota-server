import React, {useEffect, useState} from 'react';
import {Translate, translate} from 'react-jhipster';
import {connect} from 'react-redux';
import {AvField, AvForm} from 'availity-reactstrap-validation';

import {IRootState} from 'app/components/reducers';
import {getSession} from 'app/components/reducers/authentication';
import PasswordStrengthBar from 'app/components/layout/password/password-strength-bar';
import {reset, savePassword} from './password.reducer';
import {Button, Col, Row} from 'antd';
import Title from 'antd/lib/typography/Title';
import {confirmPass, current, newPassword} from "app/modules/account/password/password-rules";
import GridContainer from "app/components/layout/menus/grid/grid-container";

export interface IUserPasswordProps extends StateProps, DispatchProps {
}

export const PasswordPage = (props: IUserPasswordProps) => {
    const [password, setPassword] = useState('');
    useEffect(() => {
        props.reset();
        props.getSession();
        return () => {
            props.reset();
        };
    }, []);

    const handleValidSubmit = (event, values) =>
        props.savePassword(values.currentPassword, values.newPassword);

    const updatePassword = event => setPassword(event.target.value);

    // noinspection SpellCheckingInspection
    return (
        <GridContainer>
            <Row justify="center">
                <Col span={16}>
                    <Title level={2}>
                        <Translate contentKey="password.title" interpolate={{username: props.account.login}}>
                            Password for {props.account.login}
                        </Translate>
                    </Title>
                    <AvForm id="password-form" onValidSubmit={handleValidSubmit}>
                        <AvField
                            className="ant-input"
                            name="currentPassword"
                            label={translate('global.form.currentpassword.label')}
                            placeholder={translate('global.form.currentpassword.placeholder')}
                            type="password"
                            validate={current}
                        />
                        <AvField
                            className="ant-input"
                            name="newPassword"
                            label={translate('global.form.newpassword.label')}
                            placeholder={translate('global.form.newpassword.placeholder')}
                            type="password"
                            validate={newPassword}
                            onChange={updatePassword}
                        />
                        <PasswordStrengthBar password={password}/>
                        <AvField
                            className="ant-input"
                            name="confirmPassword"
                            label={translate('global.form.confirmpassword.label')}
                            placeholder={translate('global.form.confirmpassword.placeholder')}
                            type="password"
                            validate={confirmPass}
                        />
                        <Button type="primary" color="success" htmlType="submit">
                            <Translate contentKey="password.form.button">
                                Save
                            </Translate>
                        </Button>
                    </AvForm>
                </Col>
            </Row>
        </GridContainer>
    );
};

const mapStateToProps = ({authentication}: IRootState) => ({
    account: authentication.account,
    isAuthenticated: authentication.isAuthenticated,
});

const mapDispatchToProps = {getSession, savePassword, reset};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PasswordPage);
