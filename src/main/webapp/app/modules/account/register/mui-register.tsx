import React, {ChangeEvent} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {WithStyles, withStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {styles} from "app/components/styles/styles";
import Copyright from "app/components/layout/copyright/copyright";
import {Alert, AlertTitle} from '@material-ui/lab';

type MuiRegisterFieldsState = {
  firstName: string;
  lastName: string;
  email: string;
  firstPassword: string;
  secondPassword: string;
}

/**
 * interface extends register state model and adds validation fields for it.
 */
interface MuiRegisterState extends MuiRegisterFieldsState {
  validPassword: boolean;
  validEmail: boolean;
}

interface MuiRegisterProps extends WithStyles<typeof styles> {
  name: string
}

const validateEmail = email => {
  const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return emailRegex.test(email);
};

export class MuiRegister extends React.Component<MuiRegisterProps, MuiRegisterState> {
  constructor(props) {
    super(props)
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      firstPassword: "",
      secondPassword: "",
      validPassword: true,
      validEmail: true
    }
  }

  handleRegister = (values) => {
    this.setState({validPassword: (values.firstPassword === values.secondPassword)})
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.handleRegister(this.state)
  };

  handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const {name, value} = event.currentTarget;
    if (name === "email") {
      const check = validateEmail(value);
      this.setState({validEmail: check})
    }
    this.setState({[name]: value} as Pick<MuiRegisterFieldsState, keyof MuiRegisterFieldsState>);
  };

  render() {
    const {classes} = this.props;
    const {validPassword, validEmail} = this.state;
    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline/>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon/>
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <br/>
          <form className={classes.form} noValidate onSubmit={this.handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  aria-label="register first name"
                  onChange={this.handleChange}
                  autoComplete="fname"
                  name="firstName"
                  variant="outlined"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  aria-label="register last name"
                  onChange={this.handleChange}
                  autoComplete="lname"
                  variant="outlined"
                  id="lastName"
                  name="lastName"
                  label="Last Name"
                  fullWidth
                  required
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  aria-label="register email"
                  onChange={this.handleChange}
                  variant="outlined"
                  required
                  fullWidth
                  error={!validEmail}
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  aria-label="register password"
                  onChange={this.handleChange}
                  variant="outlined"
                  name="firstPassword"
                  label="Password"
                  type="password"
                  id="password"
                  fullWidth
                  autoComplete="current-password"
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  aria-label="password confirm"
                  autoComplete="current-password"
                  onChange={this.handleChange}
                  variant="outlined"
                  error={!validPassword}
                  id="secondPassword"
                  name="secondPassword"
                  label="Confirm password"
                  type="password"
                  fullWidth
                  required
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}
                    hidden={validPassword}
                    className={classes.center}>
                <Alert severity="error">
                  <AlertTitle>Error</AlertTitle>
                  {!validPassword && <strong>Password not match!</strong>}
                </Alert>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}
                    hidden={validEmail}
                    className={classes.center}>

                <Alert severity="error">
                  {!validEmail && <strong>Wrong email pattern</strong>}
                  <br/>
                </Alert>
              </Grid>
            </Grid>
            <br/>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}>
              Sign Up
            </Button>
            <Grid container justify="flex-end">
              <Grid item>
                <Link href="#" variant="body2">
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={5}>
          <Copyright/>
        </Box>
      </Container>
    );
  }
}

export default withStyles(styles)(MuiRegister);
