import React from 'react';
import {Translate} from 'react-jhipster';

const DefaultAccountWarn = () => (
    <>
        <Translate contentKey="global.messages.info.authenticated.prefix">If you want
            to </Translate>
        <a className="alert-link">
            <Translate contentKey="global.messages.info.authenticated.link">sign in</Translate>
        </a>
        <Translate contentKey="global.messages.info.authenticated.suffix">
            , you can try the default accounts:
            <br/>- Administrator (login=&quot;admin&quot; and password=&quot;admin&quot;)
            <br/>- User (login=&quot;user&quot; and password=&quot;user&quot;).
        </Translate>
    </>
);

export default DefaultAccountWarn;