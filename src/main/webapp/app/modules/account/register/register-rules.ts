import { translate } from 'react-jhipster';

export const emailRules = {
  required: {
    value: true,
    errorMessage: translate('global.messages.validate.email.required'),
  },
  minLength: {
    value: 5,
    errorMessage: translate('global.messages.validate.email.minlength'),
  },
  maxLength: {
    value: 254,
    errorMessage: translate('global.messages.validate.email.maxlength'),
  },
};
export const usernameRules = {
  required: {
    value: true,
    errorMessage: translate('register.messages.validate.login.required'),
  },
  pattern: {
    value: '^[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$|^[_.@A-Za-z0-9-]+$',
    errorMessage: translate('register.messages.validate.login.pattern'),
  },
  minLength: {
    value: 1,
    errorMessage: translate('register.messages.validate.login.minlength'),
  },
  maxLength: {
    value: 50,
    errorMessage: translate('register.messages.validate.login.maxlength'),
  },
};

export const firstPasswordRules = {
  required: {
    value: true,
    errorMessage: translate('global.messages.validate.newpassword.required'),
  },
  minLength: {
    value: 4,
    errorMessage: translate('global.messages.validate.newpassword.minlength'),
  },
  maxLength: {
    value: 50,
    errorMessage: translate('global.messages.validate.newpassword.maxlength'),
  },
};
export const secondPasswordRules = {
  required: {
    value: true,
    errorMessage: translate('global.messages.validate.confirmpassword.required'),
  },
  minLength: {
    value: 4,
    errorMessage: translate('global.messages.validate.confirmpassword.minlength'),
  },
  maxLength: {
    value: 50,
    errorMessage: translate('global.messages.validate.confirmpassword.maxlength'),
  },
  match: {
    value: 'firstPassword',
    errorMessage: translate('global.messages.error.dontmatch'),
  },
};
