import React, {useEffect, useState} from 'react';
import {Translate, translate} from 'react-jhipster';
import {connect} from 'react-redux';
import {AvField, AvForm} from 'availity-reactstrap-validation';

import PasswordStrengthBar from 'app/components/layout/password/password-strength-bar';
import {IRootState} from 'app/components/reducers';
import {handleRegister, reset} from './register.reducer';
import {Alert, Button, Col, Row} from 'antd';
import {
    emailRules,
    firstPasswordRules,
    secondPasswordRules,
    usernameRules
} from "app/modules/account/register/register-rules";
import DefaultAccountWarn from "app/modules/account/register/default-account-warn";

export interface IRegisterProps extends StateProps, DispatchProps {
}

export const RegisterPage = (props: IRegisterProps) => {
    const [password, setPassword] = useState('');

    useEffect(
        () => () => {
            props.reset();
        },
        []
    );

    const handleValidSubmit = (event, values) => {
        props.handleRegister(values.username, values.email, values.firstPassword, props.currentLocale);
        event.preventDefault();
    };

    const updatePassword = event => setPassword(event.target.value);

    return (
        <div>
            <Row className="justify-content-center">
                <Col md="8">
                    <h1 id="register-title">
                        <Translate contentKey="register.title">Registration</Translate>
                    </h1>
                </Col>
            </Row>
            <Row className="justify-content-center">
                <Col md="8">
                    <AvForm id="register-form" onValidSubmit={handleValidSubmit}>
                        <AvField
                            name="username"
                            label={translate('global.form.username.label')}
                            placeholder={translate('global.form.username.placeholder')}
                            validate={usernameRules}
                        />
                        <AvField
                            name="email"
                            label={translate('global.form.email.label')}
                            placeholder={translate('global.form.email.placeholder')}
                            type="email"
                            validate={emailRules}
                        />
                        <AvField
                            name="firstPassword"
                            label={translate('global.form.newpassword.label')}
                            placeholder={translate('global.form.newpassword.placeholder')}
                            type="password"
                            onChange={updatePassword}
                            validate={firstPasswordRules}
                        />
                        <PasswordStrengthBar password={password}/>
                        <AvField
                            name="secondPassword"
                            label={translate('global.form.confirmpassword.label')}
                            placeholder={translate('global.form.confirmpassword.placeholder')}
                            type="password"
                            validate={secondPasswordRules}
                        />
                        <Button id="register-submit" color="primary" htmlType="submit">
                            <Translate contentKey="register.form.button">Register</Translate>
                        </Button>
                    </AvForm>
                    <p>&nbsp;</p>
                    <Alert type="warning" message={DefaultAccountWarn}/>
                </Col>
            </Row>
        </div>
    );
};

const mapStateToProps = ({locale}: IRootState) => ({
    currentLocale: locale.currentLocale,
});

const mapDispatchToProps = {handleRegister, reset};
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);
