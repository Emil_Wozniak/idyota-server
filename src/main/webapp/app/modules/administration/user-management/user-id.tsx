import React from 'react';
import { Space, Button } from 'antd';
import {IUser} from "app/components/model/user.model";

type UserIdProps = {
    userId: any[];
    match: any;
    users: IUser[]
}

const UserId = ({userId, match, users}: UserIdProps) => {
    return (
        <Space key={`user-id-${userId}`}>
        <Button href={`${match.url}/${users.find(user => user.id === userId).login}`} color="link">
            {userId}
        </Button>
    </Space>
    );
};

export default UserId;