import React from 'react';
import { Button } from 'antd';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const UserDelete = ({login, match, account}) => (
        <Button color="danger" disabled={account.login === login}>
            <Link to={`${match.url}/${login}/delete`}>
                <FontAwesomeIcon icon="trash"/>{' '}
            </Link>
        </Button>
    );

export default UserDelete;