import React from 'react';
import { Button } from 'antd';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const UserEdit = ({login, match}) => (
    <Button color="primary">
        <Link to={`${match.url}/${login}/edit`}>
            <FontAwesomeIcon icon="pencil-alt" />{' '}
        </Link>
    </Button>
);

export default UserEdit;