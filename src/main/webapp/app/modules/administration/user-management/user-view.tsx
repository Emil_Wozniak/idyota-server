import React from 'react';
import {Button, Space} from 'antd';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

const UserView = ({login, match}) => (
    <Space key={`view-${login}`}>
        <Button color="info">
            <Link to={`${match.url}/${login}`}>
                <FontAwesomeIcon icon="eye"/>{' '}
            </Link>
        </Button>
    </Space>
);

export default UserView;