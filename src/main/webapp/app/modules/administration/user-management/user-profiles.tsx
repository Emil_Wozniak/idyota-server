import React from 'react';
import {Tag} from 'antd';

const UserProfiles = (userProfiles) => (
    <>
        {userProfiles.map((profile, i) => {
            const color = profile === "ROLE_ADMIN" ? "red" : "blue"
            const name = profile.replace("ROLE_", "")
            return <Tag style={{color}} key={`${profile}-${i}`}>{name}</Tag>;
        })}
    </>
);

export default UserProfiles;