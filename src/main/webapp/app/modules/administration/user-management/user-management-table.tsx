import React from 'react';
import {IUser} from "app/components/model/user.model";
import {Table} from 'antd';
import Column from 'antd/lib/table/Column';
import {translate} from 'react-jhipster';
import UserProfiles from "app/modules/administration/user-management/user-profiles";
import UserId from "app/modules/administration/user-management/user-id";
import UserView from "app/modules/administration/user-management/user-view";
import UserEdit from "app/modules/administration/user-management/user-edit";
import UserDelete from "app/modules/administration/user-management/user-delete";
import ColumnGroup from 'antd/lib/table/ColumnGroup';

type UserManagementColumnProps = {
    users: ReadonlyArray<IUser>
    match: any;
    account: any;
}

const UserManagementTable = ({users, match, account}: UserManagementColumnProps) => {
    const loginTrans = translate("userManagement.login");
    const id = translate("global.field.id");
    const email = translate("userManagement.email");
    const lang = translate("userManagement.langKey");
    const profiles = translate("userManagement.profiles");
    const created = translate("userManagement.createdDate");
    const modifiedBy = translate("userManagement.lastModifiedBy");
    const modifiedOn = translate("userManagement.lastModifiedDate");
    const view = translate("entity.action.view");
    const edit = translate("entity.action.edit");
    const remove = translate("entity.action.delete");
    return (
        <Table dataSource={users.concat()} bordered showHeader>
            <Column title={id} dataIndex="id" key="id" render={(ids) =>
                <UserId users={users.concat()} match={match} userId={ids}/>}/>
            <Column title={loginTrans} dataIndex="login" key="login"/>
            <Column title={email} dataIndex="email" key="email"/>
            <Column title={lang} dataIndex="langKey" key="langKey"/>
            <Column title={profiles} dataIndex="authorities" key="authorities" render={UserProfiles}/>
            <Column title={created} dataIndex="createdDate" key="createdDate"/>
            <Column title={modifiedBy} dataIndex="lastModifiedBy" key="lastModifiedBy" colSpan={1}/>
            <Column title={modifiedOn} dataIndex="lastModifiedDate" key="lastModifiedDate" colSpan={1}/>
            <ColumnGroup fixed="right" title="OPS">
                <Column title={view} dataIndex="login" key="view" colSpan={1} render={(login) => (
                    <UserView match={match} login={login}/>
                )}/>
                <Column title={edit} dataIndex="login" key="edit" colSpan={1} render={(login) => (
                    <UserEdit match={match} login={login}/>
                )}/>
                <Column title={remove} dataIndex="login" key="edit" colSpan={1} render={(login) => (
                    <UserDelete match={match} login={login} account={account}/>
                )}/>
            </ColumnGroup>

        </Table>
    );
};

export default UserManagementTable;