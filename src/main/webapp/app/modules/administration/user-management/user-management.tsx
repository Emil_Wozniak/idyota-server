import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {getSortState, Translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {ITEMS_PER_PAGE} from 'app/components/util/pagination.constants';
import {overridePaginationStateWithQueryParams} from 'app/components/util/entity-utils';
import {getUsers, updateUser} from './user-management.reducer';
import {IRootState} from 'app/components/reducers';
import UserManagementTable from "app/modules/administration/user-management/user-management-table";

export interface IUserManagementProps extends StateProps, DispatchProps, RouteComponentProps<{}> {
}

export const UserManagement = (props: IUserManagementProps) => {
    const [pagination, setPagination] = useState(
        overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE), props.location.search)
    );

    useEffect(() => {
        props.getUsers(pagination.activePage - 1, pagination.itemsPerPage, `${pagination.sort},${pagination.order}`);
        const endURL = `?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`;
        if (props.location.search !== endURL) {
            props.history.push(`${props.location.pathname}${endURL}`);
        }
    }, [pagination.activePage, pagination.order, pagination.sort]);

    useEffect(() => {
        const params = new URLSearchParams(props.location.search);
        const page = params.get('page');
        const sort = params.get('sort');
        if (page && sort) {
            const sortSplit = sort.split(',');
            setPagination({
                ...pagination,
                activePage: +page,
                sort: sortSplit[0],
                order: sortSplit[1],
            });
        }
    }, [props.location.search]);

    const sort = p => () =>
        setPagination({
            ...pagination,
            order: pagination.order === 'asc' ? 'desc' : 'asc',
            sort: p,
        });

    const handlePagination = currentPage =>
        setPagination({
            ...pagination,
            activePage: currentPage,
        });

    const toggleActive = user => () =>
        props.updateUser({
            ...user,
            activated: !user.activated,
        });

    const {users, account, match, totalItems} = props;
    return (
        <div>
            <h2 id="user-management-page-heading">
                <Translate contentKey="userManagement.home.title">Users</Translate>
                <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity">
                    <FontAwesomeIcon icon="plus"/>
                    <Translate contentKey="userManagement.home.createLabel">
                        Create a new user
                    </Translate>
                </Link>
            </h2>
            <UserManagementTable users={users} match={match} account={account}/>
        </div>
    );
};

const mapStateToProps = (storeState: IRootState) => ({
    users: storeState.userManagement.users,
    totalItems: storeState.userManagement.totalItems,
    account: storeState.authentication.account,
});

const mapDispatchToProps = {getUsers, updateUser};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(UserManagement);
