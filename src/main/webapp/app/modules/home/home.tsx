import './home.scss';
import React from 'react';
import {connect} from 'react-redux';
import IdyotaForm from "app/components/idyota/idyota-form";
import {Col} from 'antd';

export const Home = (props: StateProps) => (
    <div style={{minHeight: "50vh", maxHeight: "83vh"}}>
        <Col span={24}>
            <IdyotaForm account={props.account}/>
        </Col>
    </div>
);

const mapStateToProps = storeState => ({
    account: storeState.authentication.account,
    isAuthenticated: storeState.authentication.isAuthenticated,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(Home);
