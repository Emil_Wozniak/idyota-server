import 'react-toastify/dist/ReactToastify.css';
import './app.scss';
import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import {ToastContainer} from 'react-toastify';
import {hot} from 'react-hot-loader';
import {IRootState} from 'app/components/reducers';
import {getSession} from 'app/components/reducers/authentication';
import {getProfile} from 'app/components/reducers/application-profile';
import {setLocale} from 'app/components/reducers/locale';
import AppHeader from 'app/components/layout/header/app-header';
import AppFooter from 'app/components/layout/footer/app-footer';
import {hasAnyAuthority} from 'app/components/auth/private-route';
import {AUTHORITIES} from 'app/config/constants';
import AppRoutes from 'app/routes';
import {styles} from "app/components/styles/styles";
import {WithStyles} from '@material-ui/styles';
import {withStyles} from '@material-ui/core';
import {Layout} from 'antd';
import AppSider from "app/components/layout/sider/app-sider";
import {topLeftToast} from "app/config/toast-props";
import {siderProps} from "app/config/sider-props";
import AppLayout from "app/components/layout/menus/grid/app-layout";

const baseHref = document.querySelector('base').getAttribute('href').replace(/\/$/, '');

export interface IAppProps extends StateProps, DispatchProps, WithStyles<typeof styles> {
}

export const App = (props: IAppProps) => {
    useEffect(() => {
        props.getSession();
        props.getProfile();
    }, []);
    const {isAuth, isAdmin, locale, isSwagger, isProd, ribbonEnv} = props;
    return (
        <Router basename={baseHref}>
            <AppLayout size={10}>
                <ToastContainer {...topLeftToast}/>
                <Layout className="site-layout">
                    <AppHeader ribbonEnv={ribbonEnv} isInProduction={isProd}/>
                    <AppLayout size={8}>
                        <AppRoutes/>
                    </AppLayout>
                    <AppFooter/>
                </Layout>
                <AppSider {...siderProps(isAuth, isAdmin, isSwagger, locale, setLocale)}/>
            </AppLayout>
        </Router>
    );
};

const mapStateToProps = ({authentication, applicationProfile, locale}: IRootState) => ({
    locale: locale.currentLocale,
    isAuth: authentication.isAuthenticated,
    isAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN]),
    ribbonEnv: applicationProfile.ribbonEnv,
    isProd: applicationProfile.inProduction,
    isSwagger: applicationProfile.isSwaggerEnabled,
});

const mapDispatchToProps = {setLocale, getSession, getProfile};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)
(hot(module)((withStyles(styles)(App))));
