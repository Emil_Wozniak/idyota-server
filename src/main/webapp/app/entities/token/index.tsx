import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/components/error/error-boundary-route';

import Token from './token';
import TokenDetail from './token-detail';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={TokenDetail} />
      <ErrorBoundaryRoute path={match.url} component={Token} />
    </Switch>
  </>
);

export default Routes;
