import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/components/reducers';
import { getEntity } from './token.reducer';
import { IToken } from 'app/components/model/token.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ITokenDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const TokenDetail = (props: ITokenDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { tokenEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="idyotaApp.token.detail.title">Token</Translate> [<b>{tokenEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="word">
              <Translate contentKey="idyotaApp.token.word">Word</Translate>
            </span>
          </dt>
          <dd>{tokenEntity.word}</dd>
          <dt>
            <span id="replacement">
              <Translate contentKey="idyotaApp.token.replacement">Replacement</Translate>
            </span>
          </dt>
          <dd>{tokenEntity.replacement}</dd>
          <dt>
            <span id="info">
              <Translate contentKey="idyotaApp.token.info">Info</Translate>
            </span>
          </dt>
          <dd>{tokenEntity.info}</dd>
          <dt>
            <span id="identifier">
              <Translate contentKey="idyotaApp.token.identifier">Identifier</Translate>
            </span>
          </dt>
          <dd>{tokenEntity.identifier}</dd>
        </dl>
        <Button tag={Link} to="/token" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/token/${tokenEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ token }: IRootState) => ({
  tokenEntity: token.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(TokenDetail);
