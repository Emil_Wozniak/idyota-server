import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, loadMoreDataWhenScrolled, parseHeaderForLinks } from 'react-jhipster';
import { FAILURE, REQUEST, SUCCESS } from 'app/components/reducers/action-type.util';

import { defaultValue, IToken } from 'app/components/model/token.model';

export const ACTION_TYPES = {
  FETCH_TOKEN_LIST: 'token/FETCH_TOKEN_LIST',
  FETCH_TOKEN: 'token/FETCH_TOKEN',
  RESET: 'token/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IToken>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type TokenState = Readonly<typeof initialState>;

// Reducer

export default (state: TokenState = initialState, action): TokenState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_TOKEN_LIST):
    case REQUEST(ACTION_TYPES.FETCH_TOKEN):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_TOKEN_LIST):
    case FAILURE(ACTION_TYPES.FETCH_TOKEN):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_TOKEN_LIST): {
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_TOKEN):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/tokens';

// Actions

export const getEntities: ICrudGetAllAction<IToken> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_TOKEN_LIST,
    payload: axios.get<IToken>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IToken> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_TOKEN,
    payload: axios.get<IToken>(requestUrl),
  };
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
