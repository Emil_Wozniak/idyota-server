import React, {useEffect, useState} from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import {connect} from 'react-redux';
import {Link, RouteComponentProps} from 'react-router-dom';
import {Button, Table} from 'reactstrap';
import {getSortState, Translate} from 'react-jhipster';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import {IRootState} from 'app/components/reducers';
import {getEntities, reset} from './token.reducer';
import {ITEMS_PER_PAGE} from 'app/components/util/pagination.constants';
import {overridePaginationStateWithQueryParams} from 'app/components/util/entity-utils';

export interface ITokenProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {
}

export const Token = (props: ITokenProps) => {
  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE), props.location.search)
  );
  const [sorting, setSorting] = useState(false);

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  const resetAll = () => {
    props.reset();
    setPaginationState({
      ...paginationState,
      activePage: 1,
    });
    props.getEntities();
  };

  useEffect(() => {
    resetAll();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      resetAll();
    }
  }, [props.updateSuccess]);

  useEffect(() => {
    getAllEntities();
  }, [paginationState.activePage]);

  const handleLoadMore = () => {
    if ((window as any).pageYOffset > 0) {
      setPaginationState({
        ...paginationState,
        activePage: paginationState.activePage + 1,
      });
    }
  };

  useEffect(() => {
    if (sorting) {
      getAllEntities();
      setSorting(false);
    }
  }, [sorting]);

  const sort = p => () => {
    props.reset();
    setPaginationState({
      ...paginationState,
      activePage: 1,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });
    setSorting(true);
  };

  const {tokenList, match, loading} = props;
  return (
    <div>
      <h2 id="token-heading">
        <Translate contentKey="idyotaApp.token.home.title">Tokens</Translate>
      </h2>
      <div className="table-responsive">
        <InfiniteScroll
          pageStart={paginationState.activePage}
          loadMore={handleLoadMore}
          hasMore={paginationState.activePage - 1 < props.links.next}
          loader={<div className="loader">Loading ...</div>}
          threshold={0}
          initialLoad={false}
        >
          {tokenList && tokenList.length > 0 ? (
            <Table responsive>
              <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort"/>
                </th>
                <th className="hand" onClick={sort('word')}>
                  <Translate contentKey="idyotaApp.token.word">Word</Translate> <FontAwesomeIcon icon="sort"/>
                </th>
                <th className="hand" onClick={sort('replacement')}>
                  <Translate contentKey="idyotaApp.token.replacement">Replacement</Translate> <FontAwesomeIcon
                  icon="sort"/>
                </th>
                <th className="hand" onClick={sort('info')}>
                  <Translate contentKey="idyotaApp.token.info">Info</Translate> <FontAwesomeIcon icon="sort"/>
                </th>
                <th className="hand" onClick={sort('identifier')}>
                  <Translate contentKey="idyotaApp.token.identifier">Identifier</Translate> <FontAwesomeIcon
                  icon="sort"/>
                </th>
                <th/>
              </tr>
              </thead>
              <tbody>
              {tokenList.map((token, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${token.id}`} color="link" size="sm">
                      {token.id}
                    </Button>
                  </td>
                  <td>{token.word}</td>
                  <td>{token.replacement}</td>
                  <td>{token.info}</td>
                  <td>{token.identifier}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${token.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye"/>{' '}
                        <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
              </tbody>
            </Table>
          ) : (
            !loading && (
              <div className="alert alert-warning">
                <Translate contentKey="idyotaApp.token.home.notFound">No Tokens found</Translate>
              </div>
            )
          )}
        </InfiniteScroll>
      </div>
    </div>
  );
};

const mapStateToProps = ({token}: IRootState) => ({
  tokenList: token.entities,
  loading: token.loading,
  totalItems: token.totalItems,
  links: token.links,
  entity: token.entity,
  updateSuccess: token.updateSuccess,
});

const mapDispatchToProps = {
  getEntities,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Token);
