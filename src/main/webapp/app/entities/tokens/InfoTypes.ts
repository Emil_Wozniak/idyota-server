export enum InfoTypes {
  unknown = 'unknown',
  L1U = 'L1U',
  L2 = 'L2',
  NEW_LINE = '#line#',
  nothing = '[did nothing]',
}
