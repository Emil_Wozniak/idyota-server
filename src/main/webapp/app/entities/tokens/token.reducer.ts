import axios from 'axios';

import { FAILURE, REQUEST, SUCCESS } from 'app/components/reducers/action-type.util';

export const ACTION_TYPES = {
  FETCH_TOKENS: 'api/FETCH_TOKENS',
};

export let fileLines = [] as Array<string>;

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [],
  lines: [] as Array<string>,
  totalItems: 0,
};

export type TokensState = Readonly<typeof initialState>;

// Reducer

export default (state: TokensState = initialState, action): TokensState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_TOKENS):
      return {
        ...state,
        errorMessage: null,
        loading: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_TOKENS):
      return {
        ...state,
        loading: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_TOKENS):
      return {
        ...state,
        lines: fileLines,
        loading: false,
        entities: action.payload.data,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/tokens';

// Actions
export const getTokens = (words: string) => {
  if (words === '' || words === undefined) {
    // noinspection SpellCheckingInspection
    alert('Szukane słowa muszą mieć wartość');
  }
  const route = `${apiUrl}/${words}`;
  return {
    type: ACTION_TYPES.FETCH_TOKENS,
    payload: axios.get(route),
  };
};

export const getTokensByMode = (words: string, mode: 'fix' | 'compress') => {
  if (words === '' || words === undefined) {
    // noinspection SpellCheckingInspection
    alert('Szukane słowa muszą mieć wartość');
  }
  const route = `api/tokens/mode/${mode.toString()}`;
  return {
    type: ACTION_TYPES.FETCH_TOKENS,
    payload: axios.get(route),
  };
};

const getLines = (file: File) => {
  fileLines = [];
  file.text().then(lines => lines.split('\n').forEach(line => fileLines.push(line)));
};

export const transformText = (text: File, data?: string[]) => {
  getLines(text);
  const route = `api/tokens/FIX/file`;
  const form = new FormData();
  form.append('file', text);
  const config = { headers: { 'content-type': 'multipart/form-data' } };
  return {
    type: ACTION_TYPES.FETCH_TOKENS,
    payload: axios.post(route, form, config),
  };
};
