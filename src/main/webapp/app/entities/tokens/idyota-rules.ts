// noinspection JSUnusedGlobalSymbols
export enum IdyotaRules {
  // noinspection SpellCheckingInspection
  OCR = '[ocr]',
  EM = '[em]',
  EM_RULE = '[emRule]',
  E = '[e]',
  MIE = '[mię]',
  JOTA = '[jota]',
  SEPARATE = '[podział]',
  JOIN = '[łączenie]',
  SOUND = '[dźwięczność]',
  DZ = '[dz]',
  NUM = '[num]',
  BLP = '[blp]',
  DLM = '[dlm]',
  TYPO = '[typo]',
  AUTO = '[auto]',
}
export const keys = Object.keys(IdyotaRules); // ["A", "B"]
export const IdyotaRulesValues = keys.map(k => IdyotaRules[k as any]).join('');

export const IdyotaRulesAll: Array<string> = keys.map(k => IdyotaRules[k as any]);
