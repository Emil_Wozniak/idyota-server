import { IdyotaRulesValues } from 'app/entities/tokens/idyota-rules';

export interface Token {
  word: string;
  replacement: string;
  info: string;
}

export type Tokens = {
  rules: string;
  tokens: Array<Token>;
};

// noinspection SpellCheckingInspection
export const defaultTokens: Tokens = {
  rules: IdyotaRulesValues,
  tokens: [],
};
