import React from 'react';
import {Switch} from 'react-router-dom';
import Loadable from 'react-loadable';
import Login from 'app/modules/login/login';
import Register from 'app/modules/account/register/register';
import Activate from 'app/modules/account/activate/activate';
import PasswordResetInit from 'app/modules/account/password-reset/init/password-reset-init';
import PasswordResetFinish from 'app/modules/account/password-reset/finish/password-reset-finish';
import Logout from 'app/modules/login/logout';
import Home from 'app/modules/home/home';
import Entities from 'app/entities';
import PrivateRoute from 'app/components/auth/private-route';
import ErrorBoundaryRoute from 'app/components/error/error-boundary-route';
import PageNotFound from 'app/components/error/page-not-found';
import {AUTHORITIES} from 'app/config/constants';
import MuiRegister from "app/modules/account/register/mui-register";
import ErrorBoundary from "app/components/error/error-boundary";
import {Layout} from "antd";

const Account = Loadable({
    loader: () => import(/* webpackChunkName: "account" */ 'app/modules/account'),
    loading: () => <div>loading ...</div>,
});

const Admin = Loadable({
    loader: () => import(/* webpackChunkName: "administration" */ 'app/modules/administration'),
    loading: () => <div>loading ...</div>,
});

const {Content} = Layout;

const Routes = () => {
    const authorities = [AUTHORITIES.ADMIN, AUTHORITIES.USER];
    const admin = [AUTHORITIES.ADMIN];
    const user = [AUTHORITIES.USER];

    return (
        <ErrorBoundary>
            <Content>
                <Switch>
                    <ErrorBoundaryRoute path="/login" component={Login}/>
                    <ErrorBoundaryRoute path="/logout" component={Logout}/>
                    <ErrorBoundaryRoute path="/account/register" component={Register}/>
                    <ErrorBoundaryRoute path="/account/signup" component={MuiRegister}/>
                    <ErrorBoundaryRoute path="/account/activate/:key?" component={Activate}/>
                    <ErrorBoundaryRoute path="/account/reset/request" component={PasswordResetInit}/>
                    <ErrorBoundaryRoute path="/account/reset/finish/:key?" component={PasswordResetFinish}/>
                    <PrivateRoute path="/admin" component={Admin} hasAnyAuthorities={admin}/>
                    <PrivateRoute path="/account" component={Account} hasAnyAuthorities={authorities}/>
                    <ErrorBoundaryRoute path="/" exact component={Home}/>
                    <PrivateRoute path="/" component={Entities} hasAnyAuthorities={user}/>
                    <ErrorBoundaryRoute component={PageNotFound}/>
                </Switch>
            </Content>
        </ErrorBoundary>
    );
};

export default Routes;
