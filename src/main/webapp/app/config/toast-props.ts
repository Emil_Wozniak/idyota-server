import { toast } from 'react-toastify';

export const topLeftToast = {
  position: toast.POSITION.TOP_LEFT,
  className: 'toastify-container',
  toastClassName: 'toastify-toast',
};
