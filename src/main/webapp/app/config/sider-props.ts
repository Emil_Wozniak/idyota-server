export const siderProps = (isAuth: boolean, isAdmin: boolean, isSwagger: boolean, locale: string, setLocale) => ({
  isAuth,
  isAdmin,
  locale,
  isSwagger,
  changeLocale: setLocale,
});
