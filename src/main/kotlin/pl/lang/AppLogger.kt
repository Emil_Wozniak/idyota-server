package pl.lang

import io.vavr.control.Try
import java.net.InetAddress
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment

interface AppLogger {
    companion object {
        val log: Logger = LoggerFactory.getLogger(IdyotaApp::class.java)
        @JvmStatic
        fun registerStart(env: Environment) {
            val protocol =
                    if (env.getProperty("server.ssl.key-store") != null) "https"
                    else "http"
            val serverPort = env.getProperty("server.port")
            val contextPath = env.getProperty("server.servlet.context-path") ?: "/"
            val hostAddress = Try
                    .of { InetAddress.getLocalHost().hostAddress }
                    .onFailure { log.warn("The host name could not be determined, using `localhost` as fallback") }
                    .getOrElse { "localhost" }

            log.info(
                    """
                        
                ----------------------------------------------------------
                Application '${env.getProperty("spring.application.name")}' is running! Access URLs:
                Local:      $protocol://localhost:$serverPort$contextPath
                External:   $protocol://$hostAddress:$serverPort$contextPath
                Profile(s): ${env.activeProfiles.joinToString(",")}
                ----------------------------------------------------------
                """.trimIndent()
            )
        }
    }
}
