package pl.lang.annotations

import kotlin.annotation.AnnotationRetention.*
import kotlin.annotation.AnnotationTarget.*
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Target(TYPE, AnnotationTarget.CLASS)
@Retention(RUNTIME)
@RestController
@RequestMapping("/api")
annotation class RestApi
