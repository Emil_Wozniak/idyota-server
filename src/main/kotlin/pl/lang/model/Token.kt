package pl.lang.model

import java.io.Serializable
import javax.persistence.*
import javax.persistence.GenerationType.SEQUENCE
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy.*

/**
 * A Token.
 */
@Entity
@Table(name = "token")
@Cache(usage = READ_WRITE)
data class Token(
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,
    @Column(name = "word")
    var word: String? = null,

    @Column(name = "replacement")
    var replacement: String? = null,

    @Column(name = "info")
    var info: String? = null,

    @Column(name = "identifier")
    var identifier: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Token) return false

        return id != null && other.id != null && id == other.id
    }

    override fun hashCode() = 31

    override fun toString() = "Token{" +
        "id=$id" +
        ", word='$word'" +
        ", replacement='$replacement'" +
        ", info='$info'" +
        ", identifier='$identifier'" +
        "}"

    companion object {
        private const val serialVersionUID = 1L
    }
}
