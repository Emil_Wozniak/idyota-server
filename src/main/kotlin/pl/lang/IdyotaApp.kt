package pl.lang

import io.github.jhipster.config.DefaultProfileUtil.*
import io.github.jhipster.config.JHipsterConstants.*
import javax.annotation.PostConstruct
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.core.env.Environment
import pl.lang.AppLogger.Companion.registerStart
import pl.lang.config.ApplicationProperties
import pl.lang.config.CLOUD_DEV_ERROR
import pl.lang.config.DEV_PROD_ERROR

@Suppress("SpellCheckingInspection")
@SpringBootApplication
@EnableConfigurationProperties(LiquibaseProperties::class, ApplicationProperties::class)
class IdyotaApp(private val env: Environment) {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Initializes idyota.
     *
     * Spring profiles can be configured with a program argument --spring.profiles.active=your-active-profile
     *
     * You can find more information on how profiles work with JHipster on [https://www.jhipster.tech/profiles/]("https://www.jhipster.tech/profiles/").
     */
    @Suppress("LocalVariableName")
    @PostConstruct
    fun initApplication() {
        val profiles = env.activeProfiles
        val DEV_AND_PROD = profiles.contains(SPRING_PROFILE_DEVELOPMENT) && profiles.contains(SPRING_PROFILE_PRODUCTION)
        val CLOUD_AND_DEV = profiles.contains(SPRING_PROFILE_DEVELOPMENT) && profiles.contains(SPRING_PROFILE_CLOUD)
        if (DEV_AND_PROD) {
            log.error(DEV_PROD_ERROR)
        }
        if (CLOUD_AND_DEV) {
            log.error(CLOUD_DEV_ERROR)
        }
    }

    companion object {
        /**
         * Main method, used to run the application.
         *
         * @param args the command line arguments.
         */
        @JvmStatic
        fun main(args: Array<String>) {
            val env = runApplication<IdyotaApp>(*args) { addDefaultProfile(this) }.environment
            registerStart(env)
        }
    }
}
