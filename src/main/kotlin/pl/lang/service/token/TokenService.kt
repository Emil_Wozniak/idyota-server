package pl.lang.service.token
import java.util.Optional
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import pl.lang.model.Token

/**
 * Service Interface for managing [Token].
 */
interface TokenService {

    /**
     * Save a token.
     *
     * @param token the entity to save.
     * @return the persisted entity.
     */
    fun save(token: Token): Token

    /**
     * Get all the tokens.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    fun findAll(pageable: Pageable?): Page<Token>?

    /**
     * Get the "id" token.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    fun findOne(id: Long): Optional<Token>

    /**
     * Delete the "id" token.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long)

    fun tokenize(mode: String, examine: String? = null, lines: List<String>? = null): List<core.fixer.Token>
}
