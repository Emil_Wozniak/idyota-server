package pl.lang.service.token

import core.IdyotaModule
import core.fixer.Token
import org.springframework.boot.info.BuildProperties
import org.springframework.stereotype.Component

/**
 * Facade between modules and Rest endpoints.
 *
 * The buildProperties is required for Idyota module only to display
 * application version on instantiate, it can be omitted.
 * @param buildProperties Spring properties
 */
@Component
class ModuleProvider(buildProperties: BuildProperties?) {

    private val idyotaModule = IdyotaModule(buildProperties?.version)

    /**
     * transfers request to Idyota_core module.
     */
    fun getTokens(mode: String, examine: String?, lines: List<String>?): List<Token> =
        idyotaModule.tokenize(mode, examine, lines)
}
