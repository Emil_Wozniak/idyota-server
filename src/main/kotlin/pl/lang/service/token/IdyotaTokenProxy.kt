package pl.lang.service.token

import pl.lang.model.Token

interface IdyotaTokenProxy {

    companion object {
        fun toResponse(entity: core.fixer.Token): Token =
            Token(
                id = null,
                word = entity.word,
                replacement = entity.replacement,
                info = entity.info,
                identifier = entity.id
            )
    }
}
