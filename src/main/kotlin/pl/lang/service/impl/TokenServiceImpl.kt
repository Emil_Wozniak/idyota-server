package pl.lang.service.impl

import java.util.*
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.lang.model.Token
import pl.lang.repo.TokenRepository
import pl.lang.service.token.ModuleProvider
import pl.lang.service.token.TokenService

/**
 * Service Implementation for managing [Token].
 */
@Service
@Transactional
class TokenServiceImpl(
    private val tokenRepository: TokenRepository,
    private val moduleProvider: ModuleProvider
) : TokenService {

    private val log = LoggerFactory.getLogger(javaClass)

    override fun save(token: Token): Token {
        log.debug("Request to save Token : $token")
        return tokenRepository.save(token)
    }

    @Transactional(readOnly = true)
    override fun findAll(pageable: Pageable?): Page<Token>? {
        val isPage = pageable ?: PageRequest.of(0, 20)
        log.debug("Request to get all Tokens")
        return tokenRepository.findAll(isPage)
    }

    @Transactional(readOnly = true)
    override fun findOne(id: Long): Optional<Token> {
        log.debug("Request to get Token : $id")
        return tokenRepository.findById(id)
    }

    override fun delete(id: Long) {
        log.debug("Request to delete Token : $id")
        tokenRepository.deleteById(id)
    }

    override fun tokenize(mode: String, examine: String?, lines: List<String>?):
        List<core.fixer.Token> = moduleProvider.getTokens(mode, examine, lines)
}
