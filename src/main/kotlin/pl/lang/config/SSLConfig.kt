package pl.lang.config

import javax.net.ssl.SSLContext
import org.apache.http.client.HttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.ssl.SSLContextBuilder
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.util.ResourceUtils.getFile
import org.springframework.web.client.RestTemplate

// @Configuration
class SSLConfig {
    private val allPassword = "3sIttQfk0CVM"

//    @Bean
    @Throws(Exception::class)
    fun restTemplate(builder: RestTemplateBuilder): RestTemplate? {
        val keystore = "classpath:keystore/idyota.jks"
        val truststore = "classpath:keystore/idyota.jks"
        val sslContext: SSLContext = SSLContextBuilder
            .create()
            .loadKeyMaterial(getFile(keystore), allPassword.toCharArray(), allPassword.toCharArray())
            .loadTrustMaterial(getFile(truststore), allPassword.toCharArray())
            .build()
        val client: HttpClient = HttpClients
            .custom()
            .setSSLContext(sslContext)
            .build()
        return builder
            .requestFactory { HttpComponentsClientHttpRequestFactory(client) }
            .build()
    }
}
