package pl.lang.config.constants

const val CSP: String =
    """
default-src 'self' https://api.emailjs.com/api/v1.0/email/send https://fonts.googleapis.com ;
frame-src 'self' data: ;
script-src 'self' 'unsafe-inline' 'unsafe-eval' https://api.emailjs.com/api/v1.0/email/send https://storage.googleapis.com ;
style-src 'self'  https://fonts.googleapis.com 'unsafe-inline' ;
connect-src: 'self' 'unsafe-inline' 'unsafe-eval' https://api.emailjs.com/api/v1.0/email/send ;
img-src 'self' data: blob: ;
font-src 'self' https://fonts.gstatic.com data: ;
"""
