@file:Suppress("SpellCheckingInspection")

package pl.lang.config

// Regex for acceptable logins
const val LOGIN_REGEX: String = "^(?>[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)|(?>[_.@A-Za-z0-9-]+)$"
const val SYSTEM_ACCOUNT: String = "system"
const val ANONYMOUS_USER: String = "anonymoususer"
const val DEFAULT_LANGUAGE: String = "pl"

const val DEV_PROD_ERROR = "You have misconfigured your application! It should not run with both the 'dev' and 'prod' profiles at the same time."
const val CLOUD_DEV_ERROR = "You have misconfigured your application! It should not run with both the 'dev' and 'cloud' profiles at the same time."
