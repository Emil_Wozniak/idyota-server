package pl.lang.web.rest.tokenizer

import core.util.model.CoreMode.FIX
import core.util.model.CoreMode.isNotPossibleMode
import io.github.jhipster.web.util.PaginationUtil.generatePaginationHttpHeaders
import io.github.jhipster.web.util.ResponseUtil.wrapOrNotFound
import javax.validation.Valid
import org.slf4j.LoggerFactory.getLogger
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest
import pl.lang.model.Token
import pl.lang.service.token.IdyotaTokenProxy
import pl.lang.service.token.TokenService
import pl.lang.web.rest.errors.TokenModeException

/**
 * REST controller for managing [pl.lang.model.Token].
 */
@RestController
@RequestMapping("/api")
class TokenResource(private val tokenService: TokenService) {

    private val log = getLogger(javaClass)

    /**
     * `GET  /tokens` : get all the tokens.
     *
     * @param pageable the pagination information.
     * @return the [ResponseEntity] with status `200 (OK)` and the list of tokens in body.
     */
    @Suppress("UNCHECKED_CAST")
    @GetMapping("/tokens")
    fun getAllTokens(pageable: Pageable?): ResponseEntity<List<Token>> {
        log.debug("REST request to get a page of Tokens")
        val page = tokenService.findAll(pageable)
        val safe = mutableListOf(listOf(Token()))
        val content = page ?: PageImpl<List<Token>>(safe, PageRequest.of(0, 20), 0L)
        val headers = generatePaginationHttpHeaders(fromCurrentRequest(), content as Page<Token>)
        return ok().headers(headers).body(page?.content)
    }

    /**
     * `GET  /tokens/:id` : get the "id" token.
     *
     * @param id the id of the token to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the token,
     * or with status `404 (Not Found)`.
     */
    @GetMapping("/tokens/{id}")
    fun getToken(@PathVariable id: Long): ResponseEntity<Token> {
        log.debug("REST request to get Token : $id")
        val token = tokenService.findOne(id)
        return wrapOrNotFound(token)
    }

    /**
     * `GET  /tokens/:mode/text` : tokenize "text" word using "mode"
     *
     * @see core.IdyotaModule
     * @param mode the mode of the core.IdyotaModule
     * @return the [ResponseEntity] with status `200 (OK)` and with body the token,
     * or with status `404 (Not Found)`.
     */
    @PostMapping("/tokens/{mode}/text", produces = [APPLICATION_JSON_VALUE])
    fun tokenize(@RequestBody text: String, @PathVariable mode: String): Set<Token> {
        log.debug("REST request to get Tokens text by mode {}", mode)
        if (isNotPossibleMode(mode)) throw TokenModeException()
        return tokenService
            .tokenize(mode, text)
            .map { IdyotaTokenProxy.toResponse(it) }
            .toSet()
    }

    /**
     * `GET  /tokens/FIX/file` : tokenize "file" using FIX mode
     *
     * @see core.IdyotaModule
     * @param file send multipart file
     * @return the [ResponseEntity] with status `200 (OK)` and with body the token,
     * or with status `404 (Not Found)`.
     */
    @PostMapping("/tokens/FIX/file", produces = [APPLICATION_JSON_VALUE])
    fun tokenizeFile(@Valid @RequestPart("file") file: MultipartFile): Set<Token> {
        log.debug("REST request to get Tokens text by mode {}", file.name)
        val lines = String(file.bytes).split("\n").toList().filter { it != "" }.toList()
        return tokenService
            .tokenize(FIX.name, lines = lines)
            .map { IdyotaTokenProxy.toResponse(it) }
            .toSet()
    }

    /**
     * `GET  /tokens/:mode` : tokenize in mode =:mode
     *
     * @see core.IdyotaModule
     * @param mode the mode of the core.IdyotaModule
     * @return the [ResponseEntity] with status `200 (OK)` and with body the token,
     * or with status `404 (Not Found)`.
     */
    @GetMapping("/tokens/mode/{mode}")
    fun getTokensByMode(@PathVariable mode: String): Set<Token> {
        log.debug("REST request to get Token : $mode")
        return tokenService
            .tokenize(mode, null)
            .map { IdyotaTokenProxy.toResponse(it) }
            .toSet()
    }
}
