package pl.lang.web.rest.errors

class TokenModeException : RuntimeException("Wrong token name") {
    companion object {
        private const val serialVersionUID = 1L
    }
}
