package pl.lang.web.rest.errors

import java.net.URI

class WrongTokenName : BadRequestAlertException(URI.create(""), "Incorrect mode name", "", "modeName") {

    companion object {
        private const val serialVersionUID = 1L
    }
}
