package pl.lang.repo

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pl.lang.model.Token

/**
 * Spring Data  repository for the [Token] entity.
 */
@Suppress("unused")
@Repository
interface TokenRepository : JpaRepository<Token, Long>
