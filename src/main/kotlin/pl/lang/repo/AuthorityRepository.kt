package pl.lang.repo

import org.springframework.data.jpa.repository.JpaRepository
import pl.lang.model.Authority

/**
 * Spring Data JPA repository for the [Authority] entity.
 */

interface AuthorityRepository : JpaRepository<Authority, String>
