# HTTP endpoints

Application is using multiple type of mode to build desired response,
I wrote this article to let you know all correct endpoints for this function

## Modes

Below is the all the possible endpoints

### FIX
This mode has 2 endpoints both are used to get suggestions how to fix the text words.

1. Frontend priory endpoint is this:
 - it accepts MultipartFile;
 - it converts the file to the list of string;
 - returns value is [Token](./src/main/kotlin/pl/lang/model/Token.kt)  

```http request
POST /api/tokens/FIX/file HTTP/1.1
Host: localhost:8080
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

----WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="file"; filename="doyle.txt"
Content-Type: text/plain

(data)
----WebKitFormBoundary7MA4YWxkTrZu0gW
```

```http request
GET /api/tokens/mode/fix HTTP/1.1
Host: localhost:8080
```
 
### LEV 
This endpoint is used to run levenstein function 

```http request
GET /api/tokens/mode/lev HTTP/1.1
Host: localhost:8080
```

### COMPRESS 
```http request
GET /api/tokens/mode/compress HTTP/1.1
Host: localhost:8080
```

### DIST
This endpoint creates random distance ending for the given word, default: `przysnęła`

```http request
GET /api/tokens/mode/dist HTTP/1.1
Host: localhost:8080
```

### EXAMPLES

I do not recommend using this endpoint because it generates threadlock!!

```http request
GET /api/tokens/mode/examples
Host: localhost:8080
```
